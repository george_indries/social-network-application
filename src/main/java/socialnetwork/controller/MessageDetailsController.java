package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import socialnetwork.domain.MessageDTO;
import socialnetwork.domain.User;
import socialnetwork.service.SuperService;


public class MessageDetailsController {

    private SuperService superService;
    private Stage messagesDetailsWindowStage;
    private Long userID;
    private MessageDTO messageDTO;

    private ObservableList<User> usersListModel = FXCollections.observableArrayList();

    @FXML
    private TableView<User> usersTableView;
    @FXML
    private TableColumn<User, String> tableColumnFirstName;
    @FXML
    private TableColumn<User, String> tableColumnLastName;
    @FXML
    private TextArea messageTextArea;

    public void setService(SuperService service, Stage stage, Long userID, MessageDTO messageDTO) {
        this.superService = service;
        this.messagesDetailsWindowStage = stage;
        this.userID = userID;
        this.messageDTO = messageDTO;
        initModel();
        loadMessageText();
    }

    @FXML
    public void initialize() {

        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        usersTableView.setItems(usersListModel);
    }

    @FXML
    public void initModel() {
        usersListModel.setAll(messageDTO.getToUsersList());
    }

    private void loadMessageText() {
        String text = messageDTO.getMessage().getMessage() + "\n";
        if(messageDTO.getReplyMessage() != null)
            text += "Replied to: \n" + messageDTO.getReplyMessage().getMessage();
        messageTextArea.setWrapText(true);
        messageTextArea.setText(text);
        messageTextArea.setDisable(true);
    }

}
