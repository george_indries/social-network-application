package socialnetwork.controller;

import socialnetwork.domain.Notification;
import socialnetwork.domain.User;
import socialnetwork.service.SuperService;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.TimerTask;

public class Notifier extends TimerTask {

    private SuperService superService;

    public void setSuperService(SuperService superService) {
        this.superService = superService;
    }

    @Override
    public void run() {

        int currentPage = 0;
        int totalNumberOfPages = (int) (Math.ceil(superService.getNotificationService().getTotalNumberOfNotifications() * 1.0 / 5));

        while(currentPage < totalNumberOfPages) {

            List<Notification> notificationsOnCurrentPage = superService.getNotificationService().getNotificationsOnPage(currentPage);
            for(Notification notification: notificationsOnCurrentPage) {
                Long attendeeID = notification.getAttendeeID();
                User attendee = superService.getNotificationService().getUserByID(attendeeID);
                if(attendee.isNotifiable()) {
                    LocalDateTime lastDate = notification.getLastDate();
                    LocalDateTime eventDate = notification.getEvent().getDate();
                    LocalDateTime nowDate = LocalDateTime.now();
                    LocalDateTime tempDate = LocalDateTime.from(nowDate);
                    long hours = tempDate.until(eventDate, ChronoUnit.HOURS);
                    LocalDateTime tempDateLast = LocalDateTime.from(lastDate);
                    long hoursFromLast = tempDateLast.until(nowDate, ChronoUnit.HOURS);
                    Notification updatedNotification = new Notification();
                    updatedNotification.setId(notification.getId());
                    updatedNotification.setEvent(notification.getEvent());
                    updatedNotification.setAttendeeID(attendeeID);
                    updatedNotification.setToShow(true);
                    if(attendeeID == 5 && notification.getEvent().getId() == 4) {
                        System.out.println(hours);
                        System.out.println(hoursFromLast);
                    }
                    if(hours == 72 && hoursFromLast >= 24) {
                        String description = "Three days until " + notification.getEvent().getName() + " event!\n" +
                                "Do not forget to check your notifications!\n";
                        updatedNotification.setDescription(description);
                        updatedNotification.setLastDate(LocalDateTime.now());
                        superService.getNotificationService().updateNotification(updatedNotification);
                    } else if(hours == 24 && hoursFromLast >= 24) {
                        String description = "Just a day until " + notification.getEvent().getName() + " event!\n" +
                                "Do not forget to check your notifications!\n";
                        updatedNotification.setDescription(description);
                        updatedNotification.setLastDate(LocalDateTime.now());
                        superService.getNotificationService().updateNotification(updatedNotification);
                    }

                }

            }

            currentPage++;
        }

    }
}
