package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.Region;
import socialnetwork.domain.Attendee;
import socialnetwork.domain.Event;
import socialnetwork.service.UserPage;

public class PeopleRespondedPageController {

    private UserPage userPage;
    private Event selectedEvent;
    private ObservableList<Attendee> attendeesListModel = FXCollections.observableArrayList();

    @FXML
    private ListView<Attendee> listViewAttendees;

    public void setUserPage(UserPage userPage, Event selectedEvent) {
        this.userPage = userPage;
        this.selectedEvent = selectedEvent;
        initModel();
    }

    public void initialize() {
        listViewAttendees.setCellFactory(lst -> new ListCell<>() {
            @Override
            protected void updateItem(Attendee item, boolean empty) {
                super.updateItem(item, empty);
                if(!empty) {
                    setPrefHeight(40);
                    setText(item.getName());
                } else {
                    setPrefHeight(Region.USE_COMPUTED_SIZE);
                    setText(null);
                }
            }
        });
        listViewAttendees.setItems(attendeesListModel);
    }

    public void initModel() {
        System.out.println(selectedEvent.getAttendees());
        attendeesListModel.addAll(selectedEvent.getAttendees());
    }

}
