package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import socialnetwork.domain.User;
import socialnetwork.domain.UserAndFriendshipRequestDTO;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.UserPage;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


public class FriendsPageController implements Observer<FriendshipChangeEvent> {

    private UserPage userPage;
    private BorderPane mainPane;
    private final ObservableList<User> friendsListModel = FXCollections.observableArrayList();
    private final ObservableList<UserAndFriendshipRequestDTO> friendshipRequestsListModel = FXCollections.observableArrayList();
    private static final int ROWS_PER_PAGE = 5;
    private boolean sent = true;

    @FXML
    private TableView<User> friendsTableView;
    @FXML
    private TableColumn<User, String> friendNameTableColumn;
    @FXML
    private TableColumn<User, User> buttonTableColumn;
    @FXML
    private TextField textFieldSearchFriend;
    @FXML
    private Pagination paginationFriendsTableView;

    @FXML
    private TableView<UserAndFriendshipRequestDTO> tableViewFriendshipRequests;
    @FXML
    private TableColumn<UserAndFriendshipRequestDTO, String> tableColumnName;
    @FXML
    private TableColumn<UserAndFriendshipRequestDTO, String> tableColumnStatus;
    @FXML
    private TableColumn<UserAndFriendshipRequestDTO, String> tableColumnDate;
    @FXML
    private TableColumn<UserAndFriendshipRequestDTO, UserAndFriendshipRequestDTO> tableColumnButtonManage;
    @FXML
    private Pagination paginationFriendshipRequestsTableView;
    @FXML
    private MenuButton buttonShowFriendshipRequests;
    @FXML
    private TableColumn<User, User> buttonTableColumnViewProfile;


    public void setUserPage(UserPage userPage, BorderPane mainPane) {
        this.userPage = userPage;
        this.mainPane = mainPane;
        this.userPage.getSuperService().getFriendshipService().addObserver(this);
        initModel();
    }

    @FXML
    public void initialize() {

        Callback<TableColumn<User, User>, TableCell<User, User>> cellFactory = new Callback<>() {
            @Override
            public TableCell<User, User> call(TableColumn<User, User> param) {

                return new TableCell<>() {
                    final Button unfriendButton = new Button();

                    {
                        unfriendButton.setOnAction(event -> {
                            User selectedItem = getTableView().getSelectionModel().getSelectedItem();
                            deleteFriend(selectedItem);
                        });
                        unfriendButton.setStyle("-fx-background-color: #1a8cff;-fx-border-width: 0;-fx-text-alignment: center; -fx-text-color: #ffffff;" +
                                "-fx-border-radius: 10 10 10 10;-fx-background-radius: 10 10 10 10;");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/delete-friend.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(20);
                        imageView.setFitWidth(20);
                        unfriendButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(User item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if(empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(unfriendButton);
                        }
                    }
                };

            }
        };


        Callback<TableColumn<User, User>, TableCell<User, User>> cellFactoryViewProfile = new Callback<>() {
            @Override
            public TableCell<User, User> call(TableColumn<User, User> param) {

                return new TableCell<>() {
                    final Button viewProfile = new Button();

                    {
                        viewProfile.setOnAction(event -> {
                            User selectedItem = getTableView().getSelectionModel().getSelectedItem();
                            handleViewUserProfile(selectedItem);
                        });
                        viewProfile.setStyle("-fx-background-color: #1a8cff;-fx-border-width: 0;-fx-text-alignment: center; -fx-text-color: #ffffff;" +
                                "-fx-border-radius: 10 10 10 10;-fx-background-radius: 10 10 10 10;");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/eye.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(20);
                        imageView.setFitWidth(20);
                        viewProfile.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(User item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if(empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(viewProfile);
                        }
                    }
                };

            }
        };


        Callback<TableColumn<UserAndFriendshipRequestDTO, UserAndFriendshipRequestDTO>, TableCell<UserAndFriendshipRequestDTO, UserAndFriendshipRequestDTO>> cellFactoryView = new Callback<>() {
            @Override
            public TableCell<UserAndFriendshipRequestDTO, UserAndFriendshipRequestDTO> call(TableColumn<UserAndFriendshipRequestDTO, UserAndFriendshipRequestDTO> param) {

                return new TableCell<>() {
                    final Button viewProfile = new Button();
                    {
                        viewProfile.setOnAction(event -> {
                            UserAndFriendshipRequestDTO selectedItem = getTableView().getSelectionModel().getSelectedItem();
                            if(selectedItem != null) {
                                if(sent)
                                    handleViewUserProfile(selectedItem.getToUser());
                                else
                                    handleViewUserProfile(selectedItem.getFromUser());
                            } else {
                                AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "Please select the user whose profile you want to view!\n");
                            }
                        });
                        viewProfile.setStyle("-fx-background-color: #1a8cff;-fx-border-width: 0;-fx-text-alignment: center; -fx-text-color: #ffffff;" +
                                "-fx-border-radius: 10 10 10 10;-fx-background-radius: 10 10 10 10;");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/eye.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(20);
                        imageView.setFitWidth(20);
                        viewProfile.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(UserAndFriendshipRequestDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if(empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(viewProfile);
                        }
                    }
                };

            }
        };

        friendNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        buttonTableColumn.setCellFactory(cellFactory);
        friendsTableView.setItems(friendsListModel);

        tableColumnName.setCellValueFactory(new PropertyValueFactory<>("toName"));
        tableColumnStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        tableColumnButtonManage.setCellFactory(cellFactoryView);
        buttonTableColumnViewProfile.setCellFactory(cellFactoryViewProfile);
        tableViewFriendshipRequests.setItems(friendshipRequestsListModel);
    }

    public void initModel() {
        initModelFriends();
        initModelFriendshipRequestsSent();
    }

    public void initModelFriends() {
        int totalPagesFriends = (int) (Math.ceil(userPage.getNumberOfFriends() * 1.0 / ROWS_PER_PAGE));
        paginationFriendsTableView.setPageCount(totalPagesFriends);
        paginationFriendsTableView.setCurrentPageIndex(0);
        paginationFriendsTableView.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeFriendsTableView(newValue.intValue())
        );
        changeFriendsTableView(0);
    }

    public void initModelFriendshipRequestsSent() {
        buttonShowFriendshipRequests.setText("Sent");
        sent = true;
        tableColumnName.setCellValueFactory(new PropertyValueFactory<>("toName"));
        int totalPagesFriendshipRequests = (int) (Math.ceil(userPage.getNumberOfFriendshipsSent() * 1.0 / ROWS_PER_PAGE));
        paginationFriendshipRequestsTableView.setPageCount(totalPagesFriendshipRequests);
        paginationFriendshipRequestsTableView.setCurrentPageIndex(0);
        paginationFriendshipRequestsTableView.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeFriendshipRequestsTableViewSent(newValue.intValue())
        );
        changeFriendshipRequestsTableViewSent(0);
    }

    public void initModelFriendshipRequestsReceived() {
        buttonShowFriendshipRequests.setText("Received");
        sent = false;
        tableColumnName.setCellValueFactory(new PropertyValueFactory<>("fromName"));
        int totalPagesFriendshipRequests = (int) (Math.ceil(userPage.getNumberOfFriendshipsReceived() * 1.0 / ROWS_PER_PAGE));
        paginationFriendshipRequestsTableView.setPageCount(totalPagesFriendshipRequests);
        paginationFriendshipRequestsTableView.setCurrentPageIndex(0);
        paginationFriendshipRequestsTableView.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeFriendshipRequestsTableViewReceived(newValue.intValue())
        );
        changeFriendshipRequestsTableViewReceived(0);
    }



    private void changeFriendsTableView(int pageNumber) {
        friendsListModel.setAll(userPage.getSuperService().getFriendshipService().getNextFriendsUser(userPage.getUserID(), pageNumber));
    }

    private void changeFriendshipRequestsTableViewSent(int pageNumber) {
        friendshipRequestsListModel.setAll(userPage.getNextPageFriendshipRequestsSent(pageNumber));
    }

    private void changeFriendshipRequestsTableViewReceived(int pageNumber) {
        friendshipRequestsListModel.setAll(userPage.getNextPageFriendshipRequestsReceived(pageNumber));
    }


    private void deleteFriend(User selectedItem) {

        if(selectedItem == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "You didn't select any friend!");
        } else {
            try {
                userPage.getSuperService().getFriendshipService().deleteFriendship(selectedItem.getId(), userPage.getUserID());
            } catch (ValidationException ex) {
                AlertMessage.showErrorMessage(null, ex.getMessage());
                System.out.println(ex.getMessage());
            }
        }

    }

    public void handleSearchFriend(KeyEvent event) {

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<List<User>> future = executorService.submit(() -> {
            String text = textFieldSearchFriend.getText();
            String[] parts = text.split(" ");
            List<User> result;
            if(parts.length > 1) {
                result = userPage.getSuperService().getFriendshipService().searchFriends2(userPage.getUserID(), parts[0], parts[1]);
            } else if(parts.length == 1) {
                result = userPage.getSuperService().getFriendshipService().searchFriends2(userPage.getUserID(), parts[0], "");
            } else {
                result = new ArrayList<>();
            }
            return result;
        });
        executorService.shutdown();

        try {
            List<User> result = future.get();
            friendsListModel.setAll(result);
        } catch (InterruptedException | ExecutionException ex) {
            ex.printStackTrace();
        }

    }

    public void handleViewUserProfile(User selectedUser) {
        if (selectedUser == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "Please select the user whose profile you want to view!\n");
        } else {
            try{
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/ProfilePageView.fxml".replace("%20", " ")));
                AnchorPane profilePage = loader.load();
                profilePage.getStylesheets().add("/stylesheets/styles.css".replace("%20", " "));
                mainPane.setCenter(profilePage);

                ProfilePageController profilePageController = loader.getController();
                profilePageController.setUserPage(userPage, selectedUser, mainPane);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    public void handleShowSentFriendshipRequests(ActionEvent actionEvent) {
        initModelFriendshipRequestsSent();
    }

    public void handleShowReceivedFriendshipRequests(ActionEvent actionEvent) {
        initModelFriendshipRequestsReceived();
    }

    @Override
    public void update(FriendshipChangeEvent friendshipChangeEvent) {
        initModel();
    }
}
