package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.Event;
import socialnetwork.domain.Notification;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.UserPage;

import java.io.IOException;
import java.time.LocalDateTime;

public class EventDetailsController {

    private UserPage userPage;
    private BorderPane mainPane;
    private Event selectedEvent;

    @FXML
    private TextArea textAreaDescription;
    @FXML
    private Button buttonShowPeopleResponded;
    @FXML
    private Button buttonJoinEvent;
    @FXML
    private Label labelDate;
    @FXML
    private Label labelLocation;
    @FXML
    private Label labelHost;
    @FXML
    private Label labelEventName;
    @FXML
    private AnchorPane anchorPaneEventDetails;

    public void setUserPage(UserPage userPage, BorderPane mainPane, Event selectedEvent) {
        this.userPage = userPage;
        this.mainPane = mainPane;
        this.selectedEvent = selectedEvent;
        initModel();
    }

    public void initModel() {
        textAreaDescription.setText(selectedEvent.getDescription());
        textAreaDescription.setWrapText(true);
        textAreaDescription.setEditable(false);
        buttonShowPeopleResponded.setText(selectedEvent.getAttendees().size() + " People Responded");
        buttonShowPeopleResponded.hoverProperty().addListener((obs, oldItem, newItem) -> {
            if(newItem) {
                buttonShowPeopleResponded.setStyle("-fx-background-color: #cce6ff;");
            } else {
                buttonShowPeopleResponded.setStyle("-fx-background-color: #ffffff;");
            }
        });
        buttonJoinEvent.hoverProperty().addListener((obs, oldItem, newItem) -> {
            if(newItem) {
                buttonJoinEvent.setStyle("-fx-background-color: #AAC9FF;-fx-text-fill: #1a8cff");
            } else {
                buttonJoinEvent.setStyle("-fx-background-color: #1a8cff;-fx-text-fill: #ffffff");
            }
        });
        anchorPaneEventDetails.getChildren().forEach(x -> {
            x.hoverProperty().addListener((obs, oldItem, newItem) -> {
                if(newItem) {
                    if(x instanceof Label)
                        x.setStyle("-fx-background-color: #cce6ff;");
                } else {
                    if(x instanceof Label)
                        x.setStyle("-fx-background-color: #ffffff;");
                }
            });
        });
        labelEventName.setText(selectedEvent.getName());
        labelDate.setText(selectedEvent.getDateString());
        labelLocation.setText(selectedEvent.getLocation());
        labelHost.setText(selectedEvent.getHostName());
    }

    public void handleJoinEvent(ActionEvent actionEvent) {

        try {
            System.out.println(userPage.getUserID());
            userPage.getSuperService().getEventService().addAttendeeToEvent(userPage.getUserID(), selectedEvent.getId());
            LocalDateTime dateTime = LocalDateTime.now();
            String description = "Congratulations!\nYou've been successfully registered for " + selectedEvent.getName() +
                    " event. Now you have to check your notifications in order to not forget about the event!\n" +
                    "The event will take place on " + selectedEvent.getDateString() + " at " +
                    selectedEvent.getLocation();
            Notification notification = new Notification();
            notification.setDescription(description);
            notification.setLastDate(dateTime);
            notification.setEvent(selectedEvent);
            notification.setAttendeeID(userPage.getUserID());
            userPage.getSuperService().getNotificationService().addNotification(selectedEvent, userPage.getUserID(), dateTime, description);
            handleShowNotification(notification);
        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }

    }

    public void handleShowPeopleResponded(ActionEvent actionEvent) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/PeopleRespondedPageView.fxml".replace("%20", " ")));
            AnchorPane root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("People Responded");
            stage.getIcons().add(new Image("/images/people.png"));
            stage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(root);
            stage.setScene(scene);

            PeopleRespondedPageController peopleRespondedPageController = loader.getController();
            peopleRespondedPageController.setUserPage(userPage, selectedEvent);

            stage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public void handleShowNotification(Notification notification) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/NotificationDetailsPageView.fxml".replace("%20", " ")));
            AnchorPane root = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Successfully registered");
            stage.getIcons().add(new Image("/images/notification.png"));
            stage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(root);
            stage.setScene(scene);

            NotificationDetailsPageController notificationDetailsPageController = loader.getController();
            notificationDetailsPageController.setUserPage(notification);

            stage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
