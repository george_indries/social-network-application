package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import socialnetwork.domain.User;
import socialnetwork.domain.UsersRelation;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.UserPage;

import java.io.IOException;


public class ProfilePageController {

    private UserPage userPage;
    private User user;
    private final ObservableList<User> mutualFriendsListModel = FXCollections.observableArrayList();
    private BorderPane mainPane;
    private static final int ROWS_PER_PAGE = 5;

    @FXML
    private TableView<User> tableViewMutualFriends;
    @FXML
    private TableColumn<User, String> tableColumnName;
    @FXML
    private MenuButton buttonDropdown;
    @FXML
    private Label labelUserProfileName;
    @FXML
    private Label labelFriendshipInfo;
    @FXML
    private ImageView imageViewFriendship;
    @FXML
    private Label labelText;
    @FXML
    private Pagination paginationTableViewFriends;
    @FXML
    private TableColumn<User, User> tableColumnView;
    @FXML
    private TitledPane titledPaneFriendship;

    public void setUserPage(UserPage userPage, User user, BorderPane mainPane) {
        this.userPage = userPage;
        this.user = user;
        this.mainPane = mainPane;
        init();
        initModel();
    }

    @FXML
    public void initialize() {
        tableColumnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        Callback<TableColumn<User, User>, TableCell<User, User>> cellFactoryView = new Callback<>() {
            @Override
            public TableCell<User, User> call(TableColumn<User, User> param) {

                return new TableCell<>() {
                    final Button viewProfile = new Button();
                    {
                        viewProfile.setOnAction(event -> {
                            User selectedItem = getTableView().getSelectionModel().getSelectedItem();
                            if(selectedItem != null) {
                                handleViewUserProfile(selectedItem);
                            } else {
                                AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "Please select the user whose profile you want to view!\n");
                            }
                        });
                        viewProfile.setStyle("-fx-background-color: #1a8cff;-fx-border-width: 0;-fx-text-alignment: center; -fx-text-color: #ffffff;" +
                                "-fx-border-radius: 10 10 10 10;-fx-background-radius: 10 10 10 10;");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/eye.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(20);
                        imageView.setFitWidth(20);
                        viewProfile.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(User item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if(empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(viewProfile);
                        }
                    }
                };

            }
        };
        tableColumnView.setCellFactory(cellFactoryView);
        tableViewMutualFriends.setItems(mutualFriendsListModel);
    }

    public void handleViewUserProfile(User selectedUser) {
        if (selectedUser == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "Please select the user whose profile you want to view!\n");
        } else {
            try{
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/ProfilePageView.fxml".replace("%20", " ")));
                AnchorPane profilePage = loader.load();
                profilePage.getStylesheets().add("/stylesheets/styles.css".replace("%20", " "));
                mainPane.setCenter(profilePage);

                ProfilePageController profilePageController = loader.getController();
                profilePageController.setUserPage(userPage, selectedUser, mainPane);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void init() {
        mutualFriendsListModel.setAll();
        labelUserProfileName.setText(user.getName());
        if(!userPage.getSuperService().getFriendshipRequestService().getRelationTwoUsers(userPage.getUserID(), user.getId()).equals(UsersRelation.FRIENDS)) {
            labelText.setText("You are not friends");
            labelFriendshipInfo.setText("");
        } else {
            labelText.setText("You became friends on");
            labelFriendshipInfo.setText(
                    userPage.getSuperService().getFriendshipService().getFriendshipTwoUsers(userPage.getUserID(), user.getId()).getDateString());
        }
        updateDropdownButton();
        titledPaneFriendship.setExpanded(false);
    }

    private void initModel() {
        int totalPagesFriends = (int) (Math.ceil(userPage.getSuperService().getFriendshipService().getNumberOfFriendsUser(user.getId()) * 1.0 / ROWS_PER_PAGE));
        paginationTableViewFriends.setPageCount(totalPagesFriends);
        paginationTableViewFriends.setCurrentPageIndex(0);
        paginationTableViewFriends.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewFriends(newValue.intValue())
        );
        changeTableViewFriends(0);
    }

    public void changeTableViewFriends(int pageNumber) {
        mutualFriendsListModel.setAll(userPage.getSuperService().getFriendshipService().getNextFriendsUser(user.getId(), pageNumber));
    }

    private void updateDropdownButton() {
        UsersRelation relation = userPage.getSuperService().getFriendshipRequestService().getRelationTwoUsers(userPage.getUserID(), user.getId());
        switch (relation) {
            case PENDING_REQUEST_FROM_TO:
                buttonDropdown.getItems().forEach(x -> {
                    if(x.getId().equals("buttonAddFriend") || x.getId().equals("buttonUnfriend") || x.getId().equals("buttonAcceptFriendRequest") || x.getId().equals("buttonRejectFriendRequest")) {
                        x.setDisable(true);
                    }
                });
                break;
            case PENDING_REQUEST_TO_FROM:
                buttonDropdown.getItems().forEach(x -> {
                    if(x.getId().equals("buttonAddFriend") || x.getId().equals("buttonUnfriend") || x.getId().equals("buttonCancelFriendRequest")) {
                        x.setDisable(true);
                    }
                });
                break;
            case FRIENDS:
                buttonDropdown.getItems().forEach(x -> {
                    if(x.getId().equals("buttonAddFriend") || x.getId().equals("buttonAcceptFriendRequest") || x.getId().equals("buttonCancelFriendRequest") || x.getId().equals("buttonRejectFriendRequest")) {
                        x.setDisable(true);
                    }
                });
                break;
            case NOT_FRIENDS:
                buttonDropdown.getItems().forEach(x -> {
                    if(x.getId().equals("buttonUnfriend") || x.getId().equals("buttonAcceptFriendRequest") || x.getId().equals("buttonCancelFriendRequest") || x.getId().equals("buttonRejectFriendRequest")) {
                        x.setDisable(true);
                    }
                });
                break;
        }
    }


    public void handleAddFriend(ActionEvent actionEvent) {
        try {
            userPage.getSuperService().getFriendshipRequestService().addFriendshipRequest(userPage.getUserID(), user.getId());
            updateDropdownButton();
            AlertMessage.showMessage(null, Alert.AlertType.INFORMATION, "Info", "Your friend request has been sent!");
        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }
    }

    public void handleAcceptFriendRequest(ActionEvent actionEvent) {
        try {
            userPage.getSuperService().getFriendshipRequestService().acceptFriendship(userPage.getUserID(), user.getId() ,true);
            updateDropdownButton();
            AlertMessage.showMessage(null, Alert.AlertType.INFORMATION, "Info", "Awesome!\nNow you are friend with " + user.getName() + "!");
        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }
        initModel();
    }

    public void handleCancelFriendRequest(ActionEvent actionEvent) {
        try {
            userPage.getSuperService().getFriendshipRequestService().deleteFriendshipRequest(userPage.getUserID(), user.getId());
            updateDropdownButton();
            AlertMessage.showMessage(null, Alert.AlertType.INFORMATION, "Info", "You canceled your friend request for " + user.getName() + "!");
        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }
    }

    public void handleUnfriend(ActionEvent actionEvent) {
        try {
            userPage.getSuperService().getFriendshipService().deleteFriendship(userPage.getUserID(), user.getId());
            updateDropdownButton();
            AlertMessage.showMessage(null, Alert.AlertType.INFORMATION, "Info", "You deleted " + user.getName() + " from your friends list!");
        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }
        initModel();
    }

    public void handleRejectFriendRequest(ActionEvent actionEvent) {
        try {
            userPage.getSuperService().getFriendshipRequestService().acceptFriendship(userPage.getUserID(), user.getId(), false);
            updateDropdownButton();
            AlertMessage.showMessage(null, Alert.AlertType.INFORMATION, "Info", "You rejected " + user.getName() + "'s friend request!");
        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }
    }
}
