package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.UserPage;

import java.io.IOException;

public class UpdateProfilePageController {

    private UserPage userPage;
    private BorderPane mainPane;
    @FXML
    private TextField textFieldFirstName;
    @FXML
    private TextField textFieldLastName;
    @FXML
    private CheckBox checkBoxNotifiable;
    @FXML
    private AnchorPane anchorPaneUpdateProfile;

    public void setUserPage(UserPage userPage, BorderPane mainPane) {
        this.userPage = userPage;
        this.mainPane = mainPane;
        initComponents();
    }

    void initComponents() {
        textFieldFirstName.setText(userPage.getUserFirstName());
        textFieldLastName.setText(userPage.getUserLastName());
        checkBoxNotifiable.setSelected(userPage.getLoggedInUser().isNotifiable());
        change(true);
        anchorPaneUpdateProfile.getChildren().forEach(x -> {
            x.hoverProperty().addListener((obs, oldItem, newItem) -> {
                if(newItem) {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #AAC9FF;-fx-text-fill: #1a8cff");
                } else {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #1a8cff;-fx-text-fill: #ffffff");
                }
            });
        });
    }

    private void change(boolean disabled) {
        textFieldLastName.setDisable(disabled);
        textFieldFirstName.setDisable(disabled);
        checkBoxNotifiable.setDisable(disabled);
    }

    public void handleBackToProfile(ActionEvent actionEvent) {

        try {
            FXMLLoader homeLoader = new FXMLLoader();
            homeLoader.setLocation(getClass().getResource("/views/HomePageView.fxml".replace("%20", " ")));
            mainPane.setCenter(homeLoader.load());

            HomePageController homePageController = homeLoader.getController();
            homePageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleSaveChanges(ActionEvent actionEvent) {

        String lastName = textFieldLastName.getText();
        String firstName = textFieldFirstName.getText();
        boolean notifiable = checkBoxNotifiable.isSelected();

        try {
            userPage.getSuperService().getUserService().updateUser(userPage.getUserID(), firstName, lastName, userPage.getUserName(), userPage.getLoggedInUser().getUserPassword(), notifiable, null);
        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }

        change(true);

    }

    public void handleEdit(ActionEvent actionEvent) {
        change(false);
    }
}
