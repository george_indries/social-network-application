package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import socialnetwork.domain.FriendAndDateDTO;
import socialnetwork.service.SuperService;

import java.io.File;
import java.time.LocalDate;
import java.util.List;


public class ReportsController {

    private SuperService superService;
    private Long userID;
    private DirectoryChooser directoryChooser;
    private File selectedFile;
    private static final int ROWS_PER_PAGE = 5;
    private String firstString = "";
    private String secondString = "";

    private ObservableList<FriendAndDateDTO> friendsListModel = FXCollections.observableArrayList();

    @FXML
    private DatePicker datePickerActivitiesFrom;
    @FXML
    private DatePicker datePickerActivitiesTo;
    @FXML
    private TextField textFieldSearchFriend;
    @FXML
    private TextField textFieldDirectory;

    @FXML
    private TableView<FriendAndDateDTO> friendsTableView;
    @FXML
    private TableColumn<FriendAndDateDTO, String> tableColumnFriendFirstName;
    @FXML
    private TableColumn<FriendAndDateDTO, String> tableColumnFriendLastName;
    @FXML
    private Pagination paginationFriendsTableView;

    @FXML
    private DatePicker datePickerMessagesFrom;
    @FXML
    private DatePicker datePickerMessagesTo;
    @FXML
    private TextField textFieldDirectory2;
    @FXML
    private TextArea textAreaActivities;
    @FXML
    private TextArea textAreaMessages;
    @FXML
    private ProgressIndicator progressIndicationActivities;
    @FXML
    private ProgressIndicator progressIndicationMessages;
    @FXML
    private AnchorPane anchorPaneActivities;
    @FXML
    private AnchorPane anchorPaneMessages;


    public void setService(SuperService service, Long userID) {
        this.superService = service;
        this.userID = userID;
        initModel();
    }

    @FXML
    public void initialize() {
        tableColumnFriendFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnFriendLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        friendsTableView.setItems(friendsListModel);
    }

    private void initModel() {
        initFriendsModel();
        progressIndicationActivities.setVisible(false);
        progressIndicationMessages.setVisible(false);
        setButtonsStyles(anchorPaneActivities);
        setButtonsStyles(anchorPaneMessages);
    }

    private void setButtonsStyles(AnchorPane anchorPane) {
        anchorPane.getChildren().forEach(x -> {
            x.hoverProperty().addListener((obs, oldItem, newItem) -> {
                if(newItem) {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #AAC9FF;-fx-text-fill: #1a8cff");
                } else {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #1a8cff;-fx-text-fill: #ffffff");
                }
            });
        });
    }

    private void initFriendsModel() {
        int totalPagesFriends = (int) (Math.ceil(superService.getFriendshipService().getNumberOfFriendsUser(userID) * 1.0 / ROWS_PER_PAGE));
        paginationFriendsTableView.setPageCount(totalPagesFriends);
        paginationFriendsTableView.setCurrentPageIndex(0);
        paginationFriendsTableView.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewFriends(newValue.intValue())
        );
        paginationFriendsTableView.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewFriendsWhenSearching(newValue.intValue())
        );
        changeTableViewFriends(0);
    }

    private void changeTableViewFriends(int page) {
        friendsListModel.setAll(superService.getFriendshipService().getNextFriendAndDateDTOsUser(userID, page));
    }

    private void changeTableViewFriendsWhenSearching(int page) {
        friendsListModel.setAll(superService.getFriendshipService().getNextFriendAndDateDTOsUserSearchByName(userID, page, firstString, secondString));
    }

    @FXML
    public void handleBrowse() {
        directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File("C:/Users/George Indres/Desktop/MyDoc/An2/Sem1/MAP/ReportsSocialNetwork"));
        directoryChooser.setTitle("Choose a directory");
        textFieldDirectory.setText(directoryChooser.getInitialDirectory().getAbsolutePath());
        textFieldDirectory2.setText(directoryChooser.getInitialDirectory().getAbsolutePath());
        selectedFile = directoryChooser.showDialog(null);
    }

    @FXML
    public void handleViewActivities() {

        LocalDate fromDate = datePickerActivitiesFrom.getValue();
        LocalDate toDate = datePickerActivitiesTo.getValue();
        if(fromDate == null || toDate == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No specified period of time", "Please select both the starting date and the ending date!");
        } else {
            Task<String> taskForPreview = new Task<>() {
                @Override
                protected String call() {
                    List<String> list = superService.getReportActivitiesPreview(userID, fromDate, toDate);
                    return list
                            .stream()
                            .reduce("", String::concat);
                }
            };
            taskForPreview.setOnScheduled(event -> {
                progressIndicationActivities.setVisible(true);
                progressIndicationActivities.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
            });
            taskForPreview.setOnSucceeded(event -> {
                textAreaActivities.setText(taskForPreview.getValue());
                progressIndicationActivities.setProgress(0);
                progressIndicationActivities.setVisible(false);
            });
            Thread thread = new Thread(taskForPreview);
            thread.setDaemon(true);
            thread.start();
        }

    }


    @FXML
    public void handleSaveActivitiesToPDF() {

        LocalDate fromDate = datePickerActivitiesFrom.getValue();
        LocalDate toDate = datePickerActivitiesTo.getValue();
        if(fromDate == null || toDate == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No specified period of time", "Please select both the starting date and the ending date!");
        } else {
            Task<Void> taskForSave = new Task<>() {
                @Override
                protected Void call() throws Exception {
                    superService.writeReportActivitiesToPDF(selectedFile.getAbsolutePath(), userID, fromDate, toDate);
                    return null;
                }
            };
            taskForSave.setOnScheduled(event -> {
                progressIndicationActivities.setVisible(true);
                progressIndicationActivities.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
            });
            taskForSave.setOnSucceeded(event -> {
                progressIndicationActivities.setProgress(0);
                progressIndicationActivities.setVisible(false);
            });
            Thread thread = new Thread(taskForSave);
            thread.setDaemon(true);
            thread.start();
        }

    }


    @FXML
    public void handleSearchFriend() {

        String text = textFieldSearchFriend.getText();
        String[] parts = text.split(" ");
        if(parts.length > 1) {
            this.firstString = parts[0];
            this.secondString = parts[1];
            changeTableViewFriendsWhenSearching(0);
        } else if(parts.length == 1) {
            this.firstString = parts[0];
            this.secondString = "";
            changeTableViewFriendsWhenSearching(0);
        } else {
            changeTableViewFriends(0);
        }
    }

    @FXML
    public void handleViewMessages() {

        LocalDate fromDate = datePickerMessagesFrom.getValue();
        LocalDate toDate = datePickerMessagesTo.getValue();
        FriendAndDateDTO selectedFriend = friendsTableView.getSelectionModel().getSelectedItem();
        if(selectedFriend == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "You didn't select any friend!");
        } else if(fromDate == null || toDate == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No specified period of time", "Please select both the starting date and the ending date!");
        } else {
            Task<String> taskForPreview = new Task<>() {
                @Override
                protected String call() {
                    List<String> list = superService.getReportMessagesPreview(userID, selectedFriend.getId(), fromDate, toDate);
                    return list
                            .stream()
                            .reduce("", String::concat);
                }
            };
            taskForPreview.setOnScheduled(event -> {
                progressIndicationMessages.setVisible(true);
                progressIndicationMessages.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
            });
            taskForPreview.setOnSucceeded(event -> {
                textAreaMessages.setText(taskForPreview.getValue());
                progressIndicationMessages.setProgress(0);
                progressIndicationMessages.setVisible(false);
            });
            Thread thread = new Thread(taskForPreview);
            thread.setDaemon(true);
            thread.start();
        }

    }

    @FXML
    public void handleSaveMessagesToPDF() {

        LocalDate fromDate = datePickerMessagesFrom.getValue();
        LocalDate toDate = datePickerMessagesTo.getValue();
        FriendAndDateDTO selectedFriend = friendsTableView.getSelectionModel().getSelectedItem();
        if(fromDate == null || toDate == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No specified period of time", "Please select both the starting date and the ending date!");
        } else if(selectedFriend == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "You didn't select any friend!");
        } else {
            Task<Void> taskForSave = new Task<>() {
                @Override
                protected Void call() throws Exception {
                    superService.writeReportMessagesToPDF(selectedFile.getAbsolutePath(), userID, selectedFriend.getId(), fromDate, toDate);
                    return null;
                }
            };
            taskForSave.setOnScheduled(event -> {
                progressIndicationMessages.setVisible(true);
                progressIndicationMessages.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
            });
            taskForSave.setOnSucceeded(event -> {
                progressIndicationMessages.setProgress(0);
                progressIndicationMessages.setVisible(false);
            });
            Thread thread = new Thread(taskForSave);
            thread.setDaemon(true);
            thread.start();

        }

    }

}
