package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import socialnetwork.domain.Event;
import socialnetwork.service.UserPage;

import java.io.IOException;

public class EventsPageController {

    private UserPage userPage;
    private BorderPane mainPane;
    private ObservableList<Event> eventsListModel = FXCollections.observableArrayList();
    private static final int ROWS_PER_PAGE = 5;

    @FXML
    private TableView<Event> tableViewEvents;
    @FXML
    private TableColumn<Event, String> tableColumnName;
    @FXML
    private TableColumn<Event, String> tableColumnHost;
    @FXML
    private TableColumn<Event, String> tableColumnDate;
    @FXML
    private Pagination paginationTableViewEvents;
    @FXML
    private MenuButton buttonFilter;
    @FXML
    private Button buttonCreateEvent;

    public void setUserPage(UserPage userPage, BorderPane mainPane) {
        this.userPage = userPage;
        this.mainPane = mainPane;
        initModel();
    }

    @FXML
    public void initialize() {
        tableColumnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableColumnHost.setCellValueFactory(new PropertyValueFactory<>("hostName"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<>("dateString"));
        tableViewEvents.setItems(eventsListModel);
    }


    public void initModel() {
        tableViewEvents.setRowFactory(tV -> {
            TableRow<Event> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if(event.getClickCount() == 2 && !row.isEmpty()) {
                    handleViewEventDetails(row.getItem());
                }
            });
            return row;
        });
        initModelAllEvents();
        buttonCreateEvent.hoverProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue) {
                buttonCreateEvent.setStyle("-fx-background-color: #cce6ff;");
            } else {
                buttonCreateEvent.setStyle("-fx-background-color: #AAC9FF;");
            }
        });
    }

    public void initModelAllEvents() {
        int totalPagesEvents = (int) (Math.ceil(userPage.getTotalNumberOfEvents() * 1.0 / ROWS_PER_PAGE));
        paginationTableViewEvents.setPageCount(totalPagesEvents);
        paginationTableViewEvents.setCurrentPageIndex(0);
        paginationTableViewEvents.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewAllEvents(newValue.intValue())
        );
        changeTableViewAllEvents(0);
        buttonFilter.setText("All Events");
    }

    public void initModelJoinedEvents() {
        int totalPagesEvents = (int) (Math.ceil(userPage.getTotalNumberOfJoinedEvents() * 1.0 / ROWS_PER_PAGE));
        paginationTableViewEvents.setPageCount(totalPagesEvents);
        paginationTableViewEvents.setCurrentPageIndex(0);
        paginationTableViewEvents.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewJoinedEvents(newValue.intValue())
        );
        changeTableViewJoinedEvents(0);
        buttonFilter.setText("Going");
    }

    public void initModelHostedEvents() {
        int totalPagesEvents = (int) (Math.ceil(userPage.getTotalNumberOfHostedEvents() * 1.0 / ROWS_PER_PAGE));
        paginationTableViewEvents.setPageCount(totalPagesEvents);
        paginationTableViewEvents.setCurrentPageIndex(0);
        paginationTableViewEvents.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewHostedEvents(newValue.intValue())
        );
        changeTableViewHostedEvents(0);
        buttonFilter.setText("Hosted by me");
    }

    public void changeTableViewAllEvents(int pageNumber) {
        eventsListModel.setAll(userPage.getAllEvents(pageNumber));
    }

    public void changeTableViewHostedEvents(int pageNumber) {
        eventsListModel.setAll(userPage.getAllHostedEvents(pageNumber));
    }

    public void changeTableViewJoinedEvents(int pageNumber) {
        eventsListModel.setAll(userPage.getAllJoinedEvents(pageNumber));
    }

    public void handleViewEventDetails(Event selectedEvent) {

        try {
            FXMLLoader messagesLoader = new FXMLLoader();
            messagesLoader.setLocation(getClass().getResource("/views/EventDetailsPageView.fxml".replace("%20", " ")));
            mainPane.setCenter(messagesLoader.load());

            EventDetailsController eventDetailsController = messagesLoader.getController();
            eventDetailsController.setUserPage(userPage, mainPane, selectedEvent);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleCreateEvent(ActionEvent actionEvent) {

        try {
            FXMLLoader messagesLoader = new FXMLLoader();
            messagesLoader.setLocation(getClass().getResource("/views/CreateEventPageView.fxml".replace("%20", " ")));
            mainPane.setCenter(messagesLoader.load());

            CreateEventPageController createEventPageController = messagesLoader.getController();
            createEventPageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleShowAllEvents(ActionEvent actionEvent) {
        initModelAllEvents();
    }

    public void handleShowHostedEvents(ActionEvent actionEvent) {
        initModelHostedEvents();
    }

    public void handleShowJoinedEvents(ActionEvent actionEvent) {
        initModelJoinedEvents();
    }
}
