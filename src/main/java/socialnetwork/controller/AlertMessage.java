package socialnetwork.controller;

import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class AlertMessage {

    public static void showMessage(Stage owner, Alert.AlertType alertType, String header, String text) {

        Alert message = new Alert(alertType);
        message.setHeaderText(header);
        message.setContentText(text);
        message.initOwner(owner);
        message.show();
    }

    public static void showErrorMessage(Stage owner, String text) {

        Alert message = new Alert(Alert.AlertType.ERROR);
        message.initOwner(owner);
        message.setTitle("Error message!");
        message.setContentText(text);
        message.showAndWait();

    }

}
