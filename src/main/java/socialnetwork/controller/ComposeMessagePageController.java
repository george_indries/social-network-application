package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.UserPage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ComposeMessagePageController {

    private UserPage userPage;
    private BorderPane mainPane;
    private ObservableList<User> contactsListModel = FXCollections.observableArrayList();
    private ObservableList<User> selectedContactsListModel = FXCollections.observableArrayList();
    private static final int ROWS_PER_PAGE = 5;
    private String firstString = "";
    private String secondString = "";

    @FXML
    private TableView<User> tableViewContacts;
    @FXML
    private TableColumn<User, String> tableColumnContact;
    @FXML
    private TextField textFieldFrom;
    @FXML
    private ListView<User> listViewSelectedContacts;
    @FXML
    private TextField textFieldSearchContacts;
    @FXML
    private TextArea textAreaMessageText;
    @FXML
    private Pagination paginationTableViewContacts;
    @FXML
    private AnchorPane anchorPaneComposeMessage;


    public void setUserPage(UserPage userPage, BorderPane mainPane) {
        this.userPage = userPage;
        this.mainPane = mainPane;
        initModel();
    }

    public void initialize() {
        tableColumnContact.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableViewContacts.setItems(contactsListModel);
        listViewSelectedContacts.setItems(selectedContactsListModel);
    }

    public void initModel() {
        textFieldFrom.setText(userPage.getUserName());
        int totalPagesContacts = (int) (Math.ceil(userPage.getNumberOfUsersSearch(firstString, secondString) * 1.0 / ROWS_PER_PAGE));
        paginationTableViewContacts.setPageCount(totalPagesContacts);
        paginationTableViewContacts.setCurrentPageIndex(0);
        paginationTableViewContacts.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewContacts(newValue.intValue())
        );
        anchorPaneComposeMessage.getChildren().forEach(x -> {
            x.hoverProperty().addListener((obs, oldItem, newItem) -> {
                if(newItem) {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #AAC9FF;-fx-text-fill: #1a8cff");
                } else {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #1a8cff;-fx-text-fill: #ffffff");
                }
            });
        });
    }

    public void changeTableViewContacts(int page) {
        contactsListModel.setAll(userPage.searchUsers(firstString, secondString, page));
    }

    public void handleBackToMessagesPage(ActionEvent actionEvent) {

        try {
            FXMLLoader messagesLoader = new FXMLLoader();
            messagesLoader.setLocation(getClass().getResource("/views/MessagesPageView.fxml".replace("%20", " ")));
            mainPane.setCenter(messagesLoader.load());

            MessagesPageController messagesPageController = messagesLoader.getController();
            messagesPageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleSendMessage(ActionEvent actionEvent) {

        try {

            if(selectedContactsListModel.size() == 0) {
                AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No addressee", "You must select at least an addressee!\n");
            } else if(selectedContactsListModel.size() == 1) {
                Long toID = selectedContactsListModel.get(0).getId();
                String text = textAreaMessageText.getText();
                userPage.getSuperService().getMessageService().sendMessage(userPage.getUserID(), text, toID);
            } else {
                List<Long> addresseesIDsList = new ArrayList<>();
                selectedContactsListModel.forEach(x -> {
                    addresseesIDsList.add(x.getId());
                });
                String text = textAreaMessageText.getText();
                userPage.getSuperService().getMessageService().sendToMany(userPage.getUserID(), text, addresseesIDsList);
            }

        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }

    }

    public void handleSearchContacts(KeyEvent event) {

        String text = textFieldSearchContacts.getText();
        String[] parts = text.split(" ");
        if(parts.length > 1) {
            this.firstString = parts[0];
            this.secondString = parts[1];
            changeTableViewContacts(0);
        } else if(parts.length == 1) {
            this.firstString = parts[0];
            this.secondString = "";
            changeTableViewContacts(0);
        }
    }

    public void handleAddContact(ActionEvent actionEvent) {
        User selectedContact = tableViewContacts.getSelectionModel().getSelectedItem();
        if(selectedContact == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "You must select a contact before pressing the button!\n");
        } else {
            if(!selectedContactsListModel.contains(selectedContact))
                selectedContactsListModel.add(selectedContact);
            else {
                AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Already added", "You've already added to addressees list the selected contact!\n");
            }
        }
    }

    public void handleDeleteContact(ActionEvent actionEvent) {
        User selectedContact = listViewSelectedContacts.getSelectionModel().getSelectedItem();
        if(selectedContact == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "You must select a contact before pressing the button!\n");
        } else {
            selectedContactsListModel.remove(selectedContact);
        }
    }
}
