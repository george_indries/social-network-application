package socialnetwork.controller;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.Notification;
import socialnetwork.domain.User;
import socialnetwork.service.UserPage;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.TimerTask;

public class NotifierGUI extends TimerTask {

    private UserPage userPage;

    public void setUserPage(UserPage userPage) {
        this.userPage = userPage;
    }

    @Override
    public void run() {

        int currentPage = 0;
        int totalNumberOfPages = (int) (Math.ceil(userPage.getSuperService().getNotificationService().getTotalNumberOfNotifications() * 1.0 / 5));

        while(currentPage < totalNumberOfPages) {

            List<Notification> notificationsOnCurrentPage = userPage.getSuperService().getNotificationService().getNotificationsOnPage(currentPage);
            for(Notification notification: notificationsOnCurrentPage) {
                Long attendeeID = notification.getAttendeeID();
                User attendee = userPage.getSuperService().getNotificationService().getUserByID(attendeeID);
                if(attendee.getId().equals(userPage.getUserID()) && attendee.isNotifiable()) {
                    LocalDateTime lastDate = notification.getLastDate();
                    LocalDateTime nowDate = LocalDateTime.now();
                    LocalDateTime tempDate = LocalDateTime.from(lastDate);
                    long minutes = tempDate.until(nowDate, ChronoUnit.MINUTES);
                    if(notification.isToShow()) {

                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    FXMLLoader loader = new FXMLLoader();
                                    loader.setLocation(getClass().getResource("/views/NotificationDetailsPageView.fxml".replace("%20", " ")));
                                    AnchorPane root = loader.load();
                                    Stage stage = new Stage();
                                    stage.setTitle("Reminder");
                                    stage.getIcons().add(new Image("/images/notification.png"));
                                    stage.initModality(Modality.WINDOW_MODAL);
                                    Scene scene = new Scene(root);
                                    stage.setScene(scene);

                                    NotificationDetailsPageController notificationDetailsPageController = loader.getController();
                                    notificationDetailsPageController.setUserPage(notification);

                                    stage.show();
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                        notification.setToShow(false);
                        userPage.getSuperService().getNotificationService().updateNotification(notification);
                    }

                }

            }

            currentPage++;
        }

    }
}
