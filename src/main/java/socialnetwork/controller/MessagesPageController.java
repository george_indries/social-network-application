package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import socialnetwork.domain.MessageDTO;
import socialnetwork.service.UserPage;

import java.io.IOException;

public class MessagesPageController {

    private UserPage userPage;
    private ObservableList<MessageDTO> messagesListModel = FXCollections.observableArrayList();
    private BorderPane mainPane;
    private boolean inbox = true;
    private static final int ROWS_PER_PAGE = 5;

    @FXML
    private TableView<MessageDTO> tableViewMessages;
    @FXML
    private TableColumn<MessageDTO, String> tableColumnSubject;
    @FXML
    private TableColumn<MessageDTO, String> tableColumnFromOrTo;
    @FXML
    private TableColumn<MessageDTO, String> tableColumnDate;
    @FXML
    private Pagination paginationTableViewMessages;
    @FXML
    private AnchorPane anchorPaneMessages;

    public void setUserPage(UserPage userPage, BorderPane mainPane) {
        this.userPage = userPage;
        this.mainPane = mainPane;
        initModel();
    }

    @FXML
    public void initialize() {

        tableColumnSubject.setCellValueFactory(new PropertyValueFactory<>("subject"));
        tableColumnFromOrTo.setCellValueFactory(new PropertyValueFactory<>("fromUserName"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        tableViewMessages.setItems(messagesListModel);
    }

    public void initModel() {
        tableViewMessages.setRowFactory(tV -> {
            TableRow<MessageDTO> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if(event.getClickCount() == 2 && !row.isEmpty()) {
                    handleReadMessage(row.getItem());
                }
            });
            return row;
        });
        initModelInboxMessages();
        anchorPaneMessages.getChildren().forEach(x -> {
            x.hoverProperty().addListener((obs, oldItem, newItem) -> {
                if(newItem) {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #AAC9FF;-fx-text-fill: #1a8cff");
                } else {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #1a8cff;-fx-text-fill: #ffffff");
                }
            });
        });
    }

    public void initModelInboxMessages() {
        inbox = true;
        tableColumnFromOrTo.setCellValueFactory(new PropertyValueFactory<>("fromUserName"));
        int totalPagesFriendshipRequests = (int) (Math.ceil(userPage.getNumberOfReceivedMessages() * 1.0 / ROWS_PER_PAGE));
        paginationTableViewMessages.setPageCount(totalPagesFriendshipRequests);
        paginationTableViewMessages.setCurrentPageIndex(0);
        paginationTableViewMessages.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewMessagesInbox(newValue.intValue())
        );
        changeTableViewMessagesInbox(0);
    }

    public void initModelSentMessages() {
        inbox = false;
        tableColumnFromOrTo.setCellValueFactory(new PropertyValueFactory<>("toUsersNames"));
        int totalPagesMessages = (int) (Math.ceil(userPage.getNumberOfSentMessages() * 1.0 / ROWS_PER_PAGE));
        paginationTableViewMessages.setPageCount(totalPagesMessages);
        paginationTableViewMessages.setCurrentPageIndex(0);
        paginationTableViewMessages.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewMessagesSent(newValue.intValue())
        );
        changeTableViewMessagesSent(0);
    }

    public void changeTableViewMessagesSent(int pageNumber) {
        messagesListModel.setAll(userPage.getUserSentMessages(pageNumber));
    }

    public void changeTableViewMessagesInbox(int pageNumber) {
        messagesListModel.setAll(userPage.getUserReceivedMessages(pageNumber));
    }


    public void handleComposeMessage(ActionEvent actionEvent) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/ComposeMessagePageView.fxml".replace("%20", " ")));
            mainPane.setCenter(loader.load());

            ComposeMessagePageController composeMessagePageController = loader.getController();
            composeMessagePageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleReadMessage(MessageDTO messageDTO) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/ReadMessagePageView.fxml".replace("%20", " ")));
            mainPane.setCenter(loader.load());

            ReadMessagePageController readMessagePageController = loader.getController();
            readMessagePageController.setUserPage(userPage, mainPane, messageDTO);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleShowInboxMessages(ActionEvent actionEvent) {
        initModelInboxMessages();
    }

    public void handleShowSentMessages(ActionEvent actionEvent) {
        initModelSentMessages();
    }
}
