package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import socialnetwork.domain.MessageDTO;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.UserPage;

import java.io.IOException;

public class ReadMessagePageController {

    private UserPage userPage;
    private BorderPane mainPane;
    private MessageDTO messageDTO;
    private ObservableList<User> addresseesListModel = FXCollections.observableArrayList();

    @FXML
    private ListView<User> listViewAddressees;
    @FXML
    private TextArea textAreaMessage;
    @FXML
    private Label labelFromName;
    @FXML
    private Label labelDate;
    @FXML
    private AnchorPane anchorPaneReadMessage;

    public void setUserPage(UserPage userPage, BorderPane mainPane, MessageDTO messageDTO) {
        this.userPage = userPage;
        this.mainPane = mainPane;
        this.messageDTO = messageDTO;
        initModel();
    }

    public void initialize() {
        listViewAddressees.setItems(addresseesListModel);
    }

    public void initModel() {
        addresseesListModel.setAll(messageDTO.getToUsersList());
        textAreaMessage.setWrapText(true);
        textAreaMessage.setText(messageDTO.getText());
        //textAreaMessage.setDisable(true);
        textAreaMessage.setEditable(false);
        labelFromName.setText(messageDTO.getFromUserName());
        labelDate.setText(messageDTO.getDate());
        anchorPaneReadMessage.getChildren().forEach(x -> {
            x.hoverProperty().addListener((obs, oldItem, newItem) -> {
                if(newItem) {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #AAC9FF;-fx-text-fill: #1a8cff");
                } else {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #1a8cff;-fx-text-fill: #ffffff");
                }
            });
        });
    }

    public void handleBackToMessagesPage(ActionEvent actionEvent) {

        try {
            FXMLLoader messagesLoader = new FXMLLoader();
            messagesLoader.setLocation(getClass().getResource("/views/MessagesPageView.fxml".replace("%20", " ")));
            mainPane.setCenter(messagesLoader.load());

            MessagesPageController messagesPageController = messagesLoader.getController();
            messagesPageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleReplyMessage(ActionEvent actionEvent) {

        try {
            String replyText = textAreaMessage.getText();
            userPage.getSuperService().getMessageService().replyToMessage(userPage.getUserID(), replyText, messageDTO.getFromUser().getId(), messageDTO.getMessage().getId());

        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }

    }

    public void handlePrepareForReply(ActionEvent actionEvent) {
        //textAreaMessage.setDisable(false);
        textAreaMessage.setEditable(true);
        String sep = "On " + messageDTO.getDate() + " " + messageDTO.getFromUserName() + " wrote:\n";
        textAreaMessage.setText("\n" + sep + messageDTO.getText() + "\n");
    }

}
