package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.SuperService;

import java.io.IOException;

public class RegisterController {

    private SuperService superService;
    private Stage primaryStage;

    @FXML
    private TextField textFieldFirstName;
    @FXML
    private TextField textFieldLastName;
    @FXML
    private TextField textFieldUserName;
    @FXML
    private PasswordField passwordFieldUserPassword;
    @FXML
    private PasswordField passwordFieldUserPasswordConfirm;
    @FXML
    private AnchorPane anchorPaneRegister;


    public void setService(SuperService superService, Stage primaryStage) {
        this.superService = superService;
        this.primaryStage = primaryStage;
    }


    @FXML
    public void handleBackToLogin(ActionEvent action) {

        try {

            FXMLLoader loginPageLoader = new FXMLLoader();
            loginPageLoader.setLocation(getClass().getResource("/views/LoginPageView.fxml".replace("%20", " ")));
            Parent root = loginPageLoader.load();
            Scene loginScene = new Scene(root);
            primaryStage.setScene(loginScene);
            primaryStage.setTitle("Login");

            LoginPageController loginPageController = loginPageLoader.getController();
            loginPageController.setService(superService, primaryStage, loginScene);


        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    @FXML
    public void handleRegister(ActionEvent actionEvent) {

        try {

            superService.getUserService().saveUser(textFieldFirstName.getText(), textFieldLastName.getText(),
                    textFieldUserName.getText(), passwordFieldUserPassword.getText(), passwordFieldUserPasswordConfirm.getText());

            AlertMessage.showMessage(null, Alert.AlertType.INFORMATION, "Successfully registered!", "Welcome to Snapp! Your account was successfully created! Enjoy the app!");

            anchorPaneRegister.getChildren()
                    .forEach(x -> {
                        if(x instanceof TextField)
                            ((TextField) x).setText("");
                    });

        } catch (ValidationException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }
}
