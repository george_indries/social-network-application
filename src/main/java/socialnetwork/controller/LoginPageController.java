package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.SuperService;
import socialnetwork.service.UserPage;

import java.io.IOException;

public class LoginPageController {

    private SuperService superService;
    private Stage primaryStage;
    private Scene loginScene;

    @FXML
    private TextField textFieldUserName;
    @FXML
    private PasswordField passwordFieldUserPassword;
    @FXML
    private AnchorPane paneLogin;
    @FXML
    private BorderPane borderPane;
    @FXML
    private Button buttonLogin;

    public void setService(SuperService service, Stage primaryStage, Scene loginScene) {
        this.superService = service;
        this.primaryStage = primaryStage;
        this.loginScene = loginScene;
        init();
    }

    public void init() {
        buttonLogin.hoverProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue) {
                buttonLogin.setStyle("-fx-background-color: #cce6ff;");
            } else {
                buttonLogin.setStyle("-fx-background-color: #AAC9FF;");
            }
        });
    }

    @FXML
    public void handleCreateAccount(ActionEvent event) {

        try {

            FXMLLoader registerPageLoader = new FXMLLoader();
            registerPageLoader.setLocation(getClass().getResource("/views/RegisterView.fxml".replace("%20", " ")));
            paneLogin = registerPageLoader.load();
            borderPane.setRight(paneLogin);
            primaryStage.setTitle("Register");

            RegisterController registerController = registerPageLoader.getController();
            registerController.setService(superService, primaryStage);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    @FXML
    public void handleLogin(ActionEvent event) {

        try {
            primaryStage.setWidth(1050);
            User user = superService.getUserService().userLogin(textFieldUserName.getText(), passwordFieldUserPassword.getText());
            UserPage userPage = new UserPage(superService, user);

            FXMLLoader userPageLoader = new FXMLLoader();
            userPageLoader.setLocation(getClass().getResource("/views/UserPageView.fxml".replace("%20", " ")));
            Parent root = userPageLoader.load();
            Scene mainScene = new Scene(root);
            primaryStage.setScene(mainScene);
            mainScene.getStylesheets().add("/stylesheets/styles.css".replace("%20", " "));
            primaryStage.setTitle("Home");

            UserPageController userPageController = userPageLoader.getController();
            userPageController.setService(userPage, primaryStage, loginScene);

        } catch (IOException | ValidationException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

}
