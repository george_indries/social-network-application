package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import socialnetwork.domain.Notification;
import socialnetwork.domain.User;
import socialnetwork.service.UserPage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class HomePageController {

    private UserPage userPage;
    private ObservableList<User> usersListModel = FXCollections.observableArrayList();
    private ObservableList<Notification> notificationsListModel = FXCollections.observableArrayList();
    private BorderPane mainPane;
    private static final int ROWS_PER_PAGE = 5;
    private File chosenPhoto;

    @FXML
    private Label labelProfileName;
    @FXML
    private ListView<User> usersListView;
    @FXML
    private TextField textFieldFindPeople;
    @FXML
    private TableView<Notification> tableViewNotifications;
    @FXML
    private TableColumn<Notification, String> tableColumnNotification;
    @FXML
    private Pagination paginationTableViewNotifications;
    @FXML
    private ImageView imageViewProfilePhoto;
    @FXML
    private AnchorPane anchorPaneHomePage;


    public void setUserPage(UserPage userPage, BorderPane mainPane) {
        this.userPage = userPage;
        this.mainPane = mainPane;
        init();
    }


    @FXML
    public void initialize() {
        Callback<ListView<User>, ListCell<User>> listCellFactory = new Callback<ListView<User>, ListCell<User>>() {
            @Override
            public ListCell<User> call(ListView<User> param) {
                return new ListCell<>() {
                    final Button buttonViewProfile = new Button();
                    {
                        buttonViewProfile.setOnAction(event -> {
                            User selectedItem = getListView().getSelectionModel().getSelectedItem();
                            handleViewUserProfile(selectedItem);
                        });
                        buttonViewProfile.setPrefWidth(25);
                        buttonViewProfile.setPrefHeight(25);
                        buttonViewProfile.setStyle("-fx-background-color: #33ffbb;-fx-text-alignment: right; -fx-text-color: #ffffff;" +
                                "-fx-border-radius: 10 10 10 10;-fx-background-radius: 10 10 10 10; -fx-margin: 0 0 0 120");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/eye.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(20);
                        imageView.setFitWidth(20);
                        buttonViewProfile.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(User item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty) {
                            setGraphic(null);
                        } else {
                            Label labelName = new Label(item.getName());
                            Region space = new Region();
                            HBox.setHgrow(space, Priority.ALWAYS);
                            HBox hBoxForListCell = new HBox(labelName, space, buttonViewProfile);
                            setGraphic(hBoxForListCell);
                        }
                    }
                };

            }
        };
        usersListView.setCellFactory(listCellFactory);
        usersListView.setItems(usersListModel);
        tableColumnNotification.setCellValueFactory(new PropertyValueFactory<>("description"));
        tableViewNotifications.setItems(notificationsListModel);
        tableViewNotifications.setRowFactory(tV -> {
            TableRow<Notification> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if(event.getClickCount() == 2 && !row.isEmpty()) {
                    handleViewEventDetails(row.getItem());
                }
            });
            return row;
        });
    }


    private void init() {
        labelProfileName.setText(userPage.getUserFirstName() + " " + userPage.getUserLastName());
        usersListModel.setAll(userPage.getSuperService().getUserService().getAllUsersList());
        labelProfileName.setText(userPage.getUserFirstName() + " " + userPage.getUserLastName());
        usersListView.setVisible(false);
        initModel();
        anchorPaneHomePage.getChildren().forEach(x -> {
            x.hoverProperty().addListener((obs, oldItem, newItem) -> {
                if(newItem) {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #AAC9FF;-fx-text-fill: #1a8cff");
                } else {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #1a8cff;-fx-text-fill: #ffffff");
                }
            });
        });
        Image image;
        if(userPage.getUserFirstName().charAt(userPage.getUserFirstName().length() - 1) == 'a') {
            image = new Image("/images/lady-profile-1.jpeg");
        } else {
            image = new Image("/images/manprofile1.jpeg");
        }
        imageViewProfilePhoto.setImage(image);
    }

    public void initModel() {
        int totalPagesNotifications = (int) (Math.ceil(userPage.getTotalNumberOfNotifications() * 1.0 / ROWS_PER_PAGE));
        paginationTableViewNotifications.setPageCount(totalPagesNotifications);
        paginationTableViewNotifications.setCurrentPageIndex(0);
        paginationTableViewNotifications.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> changeTableViewNotifications(newValue.intValue())
        );
        changeTableViewNotifications(0);
    }

    public void changeTableViewNotifications(int pageNumber) {
        notificationsListModel.setAll(userPage.getAllNotifications(pageNumber));
    }


    public void handleSearchPeople(KeyEvent event) {

        usersListView.setVisible(true);

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<List<User>> future = executorService.submit(() -> {
            String text = textFieldFindPeople.getText();
            String[] parts = text.split(" ");
            List<User> result;
            if(parts.length > 1) {
                result = userPage.getSuperService().getUserService().searchUsers(parts[0], parts[1]);
            } else if(parts.length == 1) {
                result = userPage.getSuperService().getUserService().searchUsers(parts[0], "");
            }
            else {
                result = new ArrayList<>();
            }
            return result;
        });
        executorService.shutdown();

        try {
            List<User> result = future.get();
            usersListView.setPrefHeight(28 * result.size());
            usersListModel.setAll(result);
        } catch (InterruptedException | ExecutionException ex) {
            ex.printStackTrace();
        }

        if(textFieldFindPeople.getText().isEmpty()) {
            usersListView.setVisible(false);
        }

    }

    public void handleViewUserProfile(User selectedUser) {
        if (selectedUser == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "No selection", "Please select the user whose profile you want to view!\n");
        } else {
            try{
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/ProfilePageView.fxml".replace("%20", " ")));
                AnchorPane profilePage = loader.load();
                profilePage.getStylesheets().add("/stylesheets/styles.css".replace("%20", " "));
                mainPane.setCenter(profilePage);

                ProfilePageController profilePageController = loader.getController();
                profilePageController.setUserPage(userPage, selectedUser, mainPane);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void handleViewEventDetails(Notification selectedNotification) {

        try {
            FXMLLoader messagesLoader = new FXMLLoader();
            messagesLoader.setLocation(getClass().getResource("/views/EventDetailsPageView.fxml".replace("%20", " ")));
            mainPane.setCenter(messagesLoader.load());

            EventDetailsController eventDetailsController = messagesLoader.getController();
            eventDetailsController.setUserPage(userPage, mainPane, selectedNotification.getEvent());

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleUploadPhoto(ActionEvent actionEvent) throws MalformedURLException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("C:/Users/George Indres/Desktop/MAP/photosMAP"));
        fileChooser.setTitle("Choose a photo");
        chosenPhoto = fileChooser.showOpenDialog(null);
        if(chosenPhoto == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", "You didn't select any photo to upload!\n");
        } else {
            Image photo = new Image(chosenPhoto.toURI().toURL().toString());
            System.out.println(chosenPhoto.toURI().toString());
            imageViewProfilePhoto.setImage(photo);
            userPage.getSuperService().getUserService().updateUser(userPage.getLoggedInUser().getId(), userPage.getUserFirstName(), userPage.getUserLastName(),
                    userPage.getUserName(), userPage.getLoggedInUser().getUserPassword(), userPage.getLoggedInUser().isNotifiable(), photo);
        }

    }

    public void handleUpdateProfile(ActionEvent actionEvent) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/UpdateProfilePageView.fxml".replace("%20", " ")));
            mainPane.setCenter(loader.load());

            UpdateProfilePageController updateProfilePageController = loader.getController();
            updateProfilePageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }
}
