package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import socialnetwork.domain.Event;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.UserPage;
import socialnetwork.utils.TimeSpinner;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class CreateEventPageController {

    private UserPage userPage;
    private BorderPane mainPane;
    private TimeSpinner startTimeSpinner;
    private TimeSpinner endTimeSpinner;
    private ObservableList<String> categoriesListModel = FXCollections.observableArrayList(
            "Art",
            "Comedy",
            "Competition",
            "Culture",
            "Food",
            "Film",
            "Health",
            "Religion",
            "Sport",
            "Other"
    );

    @FXML
    private TextField textFieldName;
    @FXML
    private DatePicker startDatePicker;
    @FXML
    private DatePicker endDatePicker;
    @FXML
    private TextArea textAreaDescription;
    @FXML
    private VBox startTimeVBox;
    @FXML
    private VBox endTimeVBox;
    @FXML
    private Button buttonBackToEvents;
    @FXML
    private Button buttonUploadPhoto;
    @FXML
    private Button buttonCreate;
    @FXML
    private AnchorPane anchorPaneCreateEvent;
    @FXML
    private ComboBox<String> comboBoxCategory;
    @FXML
    private TextField textFieldLocation;

    public void setUserPage(UserPage userPage, BorderPane mainPane) {
        this.userPage = userPage;
        this.mainPane = mainPane;
        init();
    }

    public void init() {
        startTimeSpinner = new TimeSpinner();
        startTimeVBox.getChildren().add(startTimeSpinner);
        endTimeSpinner = new TimeSpinner();
        endTimeVBox.getChildren().add(endTimeSpinner);
        anchorPaneCreateEvent.getChildren().forEach(x -> {
            x.hoverProperty().addListener((obs, oldItem, newItem) -> {
                if(newItem) {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #AAC9FF;-fx-text-fill: #1a8cff");
                } else {
                    if(x instanceof Button)
                        x.setStyle("-fx-background-color: #1a8cff;-fx-text-fill: #ffffff");
                }
            });
        });
        comboBoxCategory.setItems(categoriesListModel);
    }

    public void handleBackToEvents(ActionEvent actionEvent) {

        try {
            FXMLLoader messagesLoader = new FXMLLoader();
            messagesLoader.setLocation(getClass().getResource("/views/EventsPageView.fxml".replace("%20", " ")));
            mainPane.setCenter(messagesLoader.load());

            EventsPageController eventsPageController = messagesLoader.getController();
            eventsPageController.setUserPage(userPage, mainPane);
        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleUploadPhoto(ActionEvent actionEvent) {

    }

    public void handleCreate(ActionEvent actionEvent) {

        LocalDate eventStartDate = startDatePicker.getValue();
        LocalTime eventStartTime = startTimeSpinner.getValue();
        LocalDate eventEndDate = startDatePicker.getValue();
        LocalTime eventEndTime = endTimeSpinner.getValue();

        if(eventStartDate == null || eventEndDate == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", "Date not specified!\n");
            return;
        }
        if(eventStartTime == null || eventEndTime == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", "Time not specified!\n");
            return;
        }
        String eventCategory = comboBoxCategory.getSelectionModel().getSelectedItem();
        if(eventCategory == null) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", "You must choose a category!\n");
            return;
        }

        try {
            String eventName = textFieldName.getText();
            LocalDateTime eventStartDateTime = LocalDateTime.of(eventStartDate, eventStartTime);
            LocalDateTime eventEndDateTime = LocalDateTime.of(eventEndDate, eventEndTime);
            String eventDescription = textAreaDescription.getText();
            String eventLocation = textFieldLocation.getText();
            userPage.getSuperService().getEventService()
                    .addEvent(eventName, userPage.getLoggedInUser(), eventStartDateTime, eventEndDateTime, eventDescription, "", eventLocation, eventCategory);

        } catch (ValidationException ex) {
            AlertMessage.showMessage(null, Alert.AlertType.WARNING, "Warning", ex.getMessage());
        }

    }
}
