package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import socialnetwork.domain.Notification;
import socialnetwork.service.UserPage;

public class NotificationDetailsPageController {

    private Notification notification;

    @FXML
    private TextArea textAreaDescription;

    public void setUserPage(Notification notification) {
        this.notification = notification;
        init();
    }

    public void init() {
        textAreaDescription.setEditable(false);
        textAreaDescription.setWrapText(true);
        textAreaDescription.setText(notification.getDescription());
    }

}
