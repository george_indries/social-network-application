package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import socialnetwork.service.UserPage;

import java.io.IOException;
import java.util.Timer;


public class UserPageController {

    @FXML
    private BorderPane mainPane;
    @FXML
    private Button homeButton;
    @FXML
    private Button friendsButton;
    @FXML
    private Button chatButton;
    @FXML
    private Button reportsButton;
    @FXML
    private Button logoutButton;
    @FXML
    private AnchorPane centerAnchorPane;
    @FXML
    private VBox vBox;

    private Pane view;
    private UserPage userPage;
    private Stage primaryStage;
    private Scene loginScene;

    public void setService(UserPage userPage, Stage primaryStage, Scene loginScene) {
        this.userPage = userPage;
        this.primaryStage = primaryStage;
        this.loginScene = loginScene;
        init();
    }


    @FXML
    public void handleLogout() {
        primaryStage.setScene(loginScene);
        primaryStage.setTitle("Login");
        primaryStage.setWidth(950);
    }

    public void init() {
        vBox.getChildren().forEach(x -> {
            if(x.getId().contains("Button")) {
                x.hoverProperty().addListener((observable, oldItem, newItem) -> {
                    if(newItem) {
                        x.setStyle("-fx-background-color: #cce6ff;");
                    } else {
                        x.setStyle("-fx-background-color: #AAC9FF;");
                    }
                });
            }
        });
        //notifyUser();
    }

    public void notifyUser() {
        Timer timer = new Timer();
        NotifierGUI notifierGUI = new NotifierGUI();
        notifierGUI.setUserPage(userPage);
        timer.schedule(notifierGUI, 60000, 100000);
    }

    @FXML
    public void handleHomePage() {

        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/HomePageView.fxml".replace("%20", " ")));
            mainPane.setCenter(loader.load());
            primaryStage.setTitle("Home");

            HomePageController homePageController = loader.getController();
            homePageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    @FXML
    public void handleFriendsPage() {

        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/FriendsPageView.fxml".replace("%20", " ")));
            AnchorPane friendsPage = loader.load();
            friendsPage.getStylesheets().add("/stylesheets/styles.css".replace("%20", " "));
            mainPane.setCenter(friendsPage);
            primaryStage.setTitle("Friends");

            FriendsPageController friendsPageController = loader.getController();
            friendsPageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    @FXML
    public void handleChatPage() {

        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/MessagesPageView.fxml".replace("%20", " ")));
            mainPane.setCenter(loader.load());
            primaryStage.setTitle("Messages");

            MessagesPageController messagesPageController = loader.getController();
            messagesPageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    @FXML
    public void handleReportsPage() {

        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/ReportActivitiesTab.fxml".replace("%20", " ")));
            mainPane.setCenter(loader.load());
            primaryStage.setTitle("Reports");

            ReportsController reportsController = loader.getController();
            reportsController.setService(userPage.getSuperService(), userPage.getUserID());


        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }

    public void handleEventsPage(ActionEvent actionEvent) {

        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/EventsPageView.fxml".replace("%20", " ")));
            mainPane.setCenter(loader.load());
            primaryStage.setTitle("Events");

            EventsPageController eventsPageController = loader.getController();
            eventsPageController.setUserPage(userPage, mainPane);

        } catch (IOException ex) {
            AlertMessage.showErrorMessage(null, ex.getMessage());
        }

    }
}
