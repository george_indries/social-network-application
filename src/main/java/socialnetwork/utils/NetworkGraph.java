package socialnetwork.utils;

import socialnetwork.domain.User;

import java.util.*;

/**
 * Implements necessary operation for - finding the number of connected components from graph network
 *                                    - finding the largest connected component
 */
public class NetworkGraph {

    private List<User> users = new ArrayList<>();
    private Map<Long, Boolean> visited = new HashMap<>();

    /**
     * Graph Constructor
     * @param users - List of Users
     */
    public NetworkGraph(List<User> users) {
        users.forEach(x -> this.users.add(x));
        users.forEach(x -> this.visited.putIfAbsent(x.getId(), false));
    }

    /**
     *
     * @param ID - Long
     * @return the element having the given ID
     */
    private User getByID(Long ID) {

        return users.stream()
                .filter(x -> x.getId() == ID)
                .findFirst()
                .get();

    }

    /**
     * Mark all graph vertices as visited
     * @param users - List of Users
     */
    private void initializeVisited(List<User> users) {
        users.forEach(x -> this.visited.putIfAbsent(x.getId(), false));
    }

    /**
     * Depth first search algorithm
     * @param user - User
     * @param visited - Boolean
     */
    private void DFS(User user, Map<Long, Boolean> visited) {

        visited.put(user.getId(), true);
        List<User> neighbours = user.getFriends();

        for(User neighbour: neighbours) {
            if(!visited.get(neighbour.getId()))
                DFS(neighbour, visited);
        }

    }

    /**
     *
     * @return the number of connected components from graph
     */
    public Integer countConnectedComponents() {

        initializeVisited(this.users);

        Integer counter = 0;

        for(Map.Entry<Long, Boolean> pair: visited.entrySet()) {
            User user = getByID(pair.getKey());
            if(!pair.getValue()) {
                counter++;
                DFS(user, visited);
            }
        }

        return counter;

    }

    /**
     * Depth first search algorithm used for finding the largest connected component
     * @param user - User
     * @param visited - Boolean
     * @param usersFromComponent - List of Users
     */
    private void DFS(User user, Map<Long, Boolean> visited, List<User> usersFromComponent) {

        visited.put(user.getId(), true);
        usersFromComponent.add(user);
        List<User> neighbours = user.getFriends();

        for(User neighbour: neighbours) {
            if(!visited.get(neighbour.getId()))
                DFS(neighbour, visited, usersFromComponent);
        }

    }

    /**
     *
     * @return the list of all users forming the largest connected component
     */
    public List<User> getLargestComponent() {

        initializeVisited(this.users);

        List<User> users = new ArrayList<>();
        Integer maxPath = -1;

        for(Map.Entry<Long, Boolean> pair: visited.entrySet()) {
            User user = getByID(pair.getKey());
            if(!pair.getValue()) {
                List<User> auxUsers = new ArrayList<>();
                DFS(user, visited, auxUsers);
                if(maxPath < auxUsers.size()) {
                    maxPath = auxUsers.size();
                    users = copyList(auxUsers);
                }
            }
        }

        return users;

    }

    /**
     *
     * @param users -
     * @return a copy of the list given as parameter
     */
    private List<User> copyList(List<User> users) {

        List<User> copy = new ArrayList<>();
        users.forEach(x -> copy.add(x));
        return copy;

    }

}
