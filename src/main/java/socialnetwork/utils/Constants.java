package socialnetwork.utils;

import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

public class Constants {

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yy-MM-dd hh:mm:ss.SSS");
    public static final DateTimeFormatter DATE_TIME_FORMATTER_SIMPLE = DateTimeFormatter.ofPattern("yy-MM-dd hh:mm");
    public static final Pattern VALID_ADDRESS_REGEX = Pattern.compile("[^&*()^%$£'!`|/?;:{}#+-].{3,12}?@snapp\\.com$");
    //"\<[^@*\!?]\+@[a-z0-9_-]\+\(\.[a-z0-9_-]\+\)\+\>"
    //"\"[^\\s\\t&*()^%$£'!`|/?;:{}#+-]+@snapp.com$\""
}
