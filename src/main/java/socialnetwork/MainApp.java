package socialnetwork;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controller.LoginPageController;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.database.*;
import socialnetwork.repository.paging.*;
import socialnetwork.service.*;

import java.io.IOException;


public class MainApp extends Application {

    UserService userService1;
    FriendshipService friendshipService1;
    FriendshipRequestService friendshipRequestService1;
    MessageService messageService;
    EventService eventService;
    NotificationService notificationService;
    SuperService superService;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        final String password = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");

        UserPagingRepository userDBRepository =
                new UserDBRepository(url, username, password, new UserValidator());
        FriendshipPagingRepository friendshipDBRepository =
                new FriendshipDBRepository(url, username, password, new FriendshipValidator());
        FriendshipRequestPagingRepository friendshipRequestDBRepository =
                new FriendshipRequestDBRepository(url, username, password, new FriendshipRequestValidator());
        MessagePagingRepository messageDBRepository =
                new MessageDBRepository(url, username, password, new MessageValidator());
        EventPagingRepository eventDBRepository =
                new EventDBRepository(url, username, password, new EventValidator(), userDBRepository);
        NotificationPagingRepository notificationDBRepository =
                new NotificationDBRepository(url, username, password, new NotificationValidator(), eventDBRepository);

        userService1 =
                new UserService(userDBRepository, friendshipDBRepository, friendshipRequestDBRepository);
        friendshipService1 =
                new FriendshipService(friendshipDBRepository, userDBRepository, friendshipRequestDBRepository);
        friendshipRequestService1 =
                new FriendshipRequestService(friendshipDBRepository, userDBRepository, friendshipRequestDBRepository, friendshipService1);
        messageService =
                new MessageService(messageDBRepository, userDBRepository);
        eventService =
                new EventService(eventDBRepository, userDBRepository);
        notificationService =
                new NotificationService(notificationDBRepository, eventDBRepository, userDBRepository);


        superService =
                new SuperService(userService1, friendshipService1, friendshipRequestService1, messageService, eventService, notificationService);

        initView(primaryStage);
        primaryStage.setWidth(950);
        primaryStage.setHeight(750);
        primaryStage.show();
    }

    private void initView(Stage primaryStage) throws IOException {

        FXMLLoader loginPageLoader = new FXMLLoader();
        loginPageLoader.setLocation(getClass().getResource("/views/LoginPageView.fxml".replace("%20", " ")));
        Parent root = loginPageLoader.load();
        Scene loginScene = new Scene(root);
        loginScene.getStylesheets().add("/stylesheets/styles.css".replace("%20", " "));
        primaryStage.setScene(loginScene);
        primaryStage.getIcons().add(new Image("/images/community-manager.png"));
        primaryStage.setTitle("Login");

        LoginPageController loginPageController = loginPageLoader.getController();
        loginPageController.setService(superService, primaryStage, loginScene);

        /*Timer timer = new Timer();
        Notifier notifier = new Notifier();
        notifier.setSuperService(superService);
        timer.schedule(notifier, 30000, 360000);*/

    }

}
