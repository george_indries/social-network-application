package socialnetwork.ui;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;


public class UserInterface {

    SuperService superService;

    BufferedReader reader = null;

    public UserInterface(SuperService superService) {
        this.superService = superService;
    }

    public void printAllUI() {
        superService.getUserService().getAll().forEach(System.out::println);
    }

    public void addUserUI() {

        try {
            System.out.println("User first name:");
            String firstName = reader.readLine();
            System.out.println("User last name:");
            String lastName = reader.readLine();
            superService.getUserService().addUser(firstName, lastName);
        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void deleteUserUI() {

        try{
            System.out.println("User ID:");
            Long ID = Long.parseLong(reader.readLine());
            superService.getUserService().deleteUser(ID);
        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void addFriendshipUI() {

        try {
            System.out.println("Enter the first ID:");
            Long leftID = Long.parseLong(reader.readLine());
            System.out.println("Enter the second ID:");
            Long rightID = Long.parseLong(reader.readLine());
            superService.getFriendshipService().addFriendship(leftID, rightID);
        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void deleteFriendshipUI() {

        try {
            System.out.println("Enter the first ID:");
            Long leftID = Long.parseLong(reader.readLine());
            System.out.println("Enter the second ID:");
            Long rightID = Long.parseLong(reader.readLine());
            superService.getFriendshipService().deleteFriendship(leftID, rightID);
        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void printUserFriends() {

        try {
            System.out.println("Enter ID:");
            Long ID = Long.parseLong(reader.readLine());
            superService.getFriendshipService().getUserFriends(ID).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void printAllFriendsForAllUsers() {

        try {

            superService.getFriendshipService().loadAllFriendsForAllUsers()
                    .forEach((User user) -> {
                        System.out.println(user.getFirstName() + " " + user.getLastName() + " : ");
                        if(!user.getFriends().isEmpty())
                            user.getFriends().forEach(System.out::println);
                        else
                            System.out.println("No friend");
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void printNumberOfCommunities() {

        try {

            System.out.println(superService.getFriendshipService().getNumberOfCommunities());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

    public void printMostSociableCommunity() {

        try {

            superService.getFriendshipService().getMostSociableCommunity().forEach(System.out::println);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void printAllFriendshipsUser() {

        try {

            System.out.println("Enter ID:");
            Long ID = Long.parseLong(reader.readLine());
            superService.getFriendshipService().getAllFriendshipsUser(ID)
                    .forEach(x -> System.out.println(x.getLastName() + "|" + x.getFirstName() +
                            "|" + x.getDate().toString()));

        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void printAllFriendshipsUserByMonth() {

        try {

            System.out.println("Enter Month:");
            Integer monthInteger = Integer.parseInt(reader.readLine());
            Month month = Month.of(monthInteger);
            System.out.println("Enter ID:");
            Long ID = Long.parseLong(reader.readLine());

            superService.getFriendshipService().getAllFriendshipsUserByMonth(ID, month)
                    .forEach(x -> System.out.println(x.getLastName() + "|" + x.getFirstName() +
                            "|" + x.getDate().toString()));

        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void printAllFriendships() {
        superService.getFriendshipService().getAll().forEach(System.out::println);
    }

    public void printAllFriendshipRequests() {

        superService.getFriendshipRequestService().getAll().forEach(System.out::println);

    }

    public void addFriendshipRequestUI() {

        try {

            System.out.println("Enter the ID of the user who requests the friendship:");
            Long firstID = Long.parseLong(reader.readLine());
            System.out.println("Enter the ID of the user who is asked to accept the friendship:");
            Long secondID = Long.parseLong(reader.readLine());
            superService.getFriendshipRequestService().addFriendshipRequest(firstID, secondID);

        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void manageFriendshipRequest() {

        try {

            System.out.println("Enter the ID of the user you want to manage friendship requests:");
            Long firstID = Long.parseLong(reader.readLine());
            System.out.println("Enter the ID of the user you want to approve/reject the friendship request:");
            Long secondID = Long.parseLong(reader.readLine());
            System.out.println("Enter on of the following approve/reject:");
            String acceptString = reader.readLine();
            Boolean accept = false;
            if(acceptString.equals("approve")) {
                accept = true;
            }
            superService.getFriendshipRequestService().acceptFriendship(firstID, secondID, accept);

        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void printAllMessages() {

        try {

            superService.getMessageService().getAll().forEach(System.out::println);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void sendMessageUI() {

        try {

            System.out.println("Enter the sender's id:");
            Long fromID = Long.parseLong(reader.readLine());
            System.out.println("Enter the addressee's id:");
            Long toID = Long.parseLong(reader.readLine());
            System.out.println("Enter the text you want to send:");
            String text = reader.readLine();
            superService.getMessageService().sendMessage(fromID, text, toID);

        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void replyToMessageUI() {

        try {

            System.out.println("Enter the sender's id:");
            Long fromID = Long.parseLong(reader.readLine());
            System.out.println("Enter the addressee's id:");
            Long toID = Long.parseLong(reader.readLine());
            System.out.println("Enter the id of the message you want to reply:");
            Long replyMessageID = Long.parseLong(reader.readLine());
            System.out.println("Enter the text you want to send:");
            String text = reader.readLine();
            superService.getMessageService().replyToMessage(fromID, text, toID, replyMessageID);

        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void sendMessageToManyUI() {

        try {

            System.out.println("Enter the sender's id:");
            Long fromID = Long.parseLong(reader.readLine());
            System.out.println("Enter the addressees' ids separated by spaces:");
            String toIDsForSplit = reader.readLine();
            System.out.println("Enter the text you want to send:");
            String text = reader.readLine();

            String[] toIDsString = toIDsForSplit.split(" ");
            List<Long> toIDs = new ArrayList<>();
            for(String str: toIDsString) {
                toIDs.add(Long.parseLong(str));
            }

            superService.getMessageService().sendToMany(fromID, text, toIDs);

        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void sentToAllUI() {

        try {

            System.out.println("Enter the sender's id:");
            Long fromID = Long.parseLong(reader.readLine());
            System.out.println("Enter the text you want to send:");
            String text = reader.readLine();

            superService.getMessageService().sendToMany(fromID, text, superService.getMessageService().getAllUsersIDs());

        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void printMessagesTwoUsersUI() {

        try {

            System.out.println("Enter the id of the first user:");
            Long firstID = Long.parseLong(reader.readLine());
            System.out.println("Enter the id of the second user:");
            Long secondID = Long.parseLong(reader.readLine());

            superService.getMessageService().getUsersMessagesSortedByDate(firstID, secondID)
                    .forEach(x -> System.out.println("From: " + x.getLastName() + "|" +
                            x.getFirstName() + "|" + x.getDate() + "|" +
                            x.getTextMessage() + "\n"));


        } catch (ValidationException | IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    public void printNewMenu() {

        System.out.println("1. Print all users - print-all");
        System.out.println("2. Add new user - add-user");
        System.out.println("3. Delete user - delete-user");
        System.out.println("4. Add new friendship - add-friendship");
        System.out.println("5. Print all friendships - print-friendships");
        System.out.println("6. Delete friendship - delete-friendship");
        System.out.println("7. Print the number of communities - print-no-communities");
        System.out.println("8. Print the most sociable community - print-most-sociable");
        System.out.println("9. Print all friendships for a given user - print-all-friendships-user");
        System.out.println("10. Print all friendships for a given user, by month - print-all-friendships-user-by-month");
        System.out.println("11. Print all friendship requests - print-all-friendship-requests");
        System.out.println("12. Add new friendship request - add-friendship-request");
        System.out.println("13. Manage the friendship request of a user - manage-friendship-request");
        System.out.println("14. Print all messages - print-all-messages");
        System.out.println("15. Send message - send-message");
        System.out.println("16. Reply to message - reply-to-message");
        System.out.println("17. Send message to more users - send-to-many");
        System.out.println("18. Send message to all users - send-to-all");
        System.out.println("19. Print all messages of two users - print-messages-two-users");

    }

    public void runUI() {

        reader = new BufferedReader(new InputStreamReader(System.in));

        boolean running = true;

        printNewMenu();

        while(running) {

            try {
                System.out.println("Give command:");
                String command = reader.readLine();
                switch (command) {
                    case "print-all":
                        printAllUI();
                        break;
                    case "add-user":
                        addUserUI();
                        break;
                    case "delete-user":
                        deleteUserUI();
                        break;
                    case "add-friendship":
                        addFriendshipUI();
                        break;
                    case "print-friendships":
                        printAllFriendships();
                        break;
                    case "print-friends":
                        printUserFriends();
                        break;
                    case "delete-friendship":
                        deleteFriendshipUI();
                        break;
                    case "print-all-friends":
                        printAllFriendsForAllUsers();
                        break;
                    case "print-no-communities":
                        printNumberOfCommunities();
                        break;
                    case "print-most-sociable":
                        printMostSociableCommunity();
                        break;
                    case "print-all-friendships-user":
                        printAllFriendshipsUser();
                        break;
                    case "print-all-friendships-user-by-month":
                        printAllFriendshipsUserByMonth();
                        break;
                    case "print-all-friendship-requests":
                        printAllFriendshipRequests();
                        break;
                    case "add-friendship-request":
                        addFriendshipRequestUI();
                        break;
                    case "manage-friendship-request":
                        manageFriendshipRequest();
                        break;
                    case "print-all-messages":
                        printAllMessages();
                        break;
                    case "send-message":
                        sendMessageUI();
                        break;
                    case "reply-to-message":
                        replyToMessageUI();
                        break;
                    case "send-to-many":
                        sendMessageToManyUI();
                        break;
                    case "send-to-all":
                        sentToAllUI();
                        break;
                    case "print-messages-two-users":
                        printMessagesTwoUsersUI();
                        break;
                    case "exit":
                        running = false;
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
                running = false;
            }

        }

        if(reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
