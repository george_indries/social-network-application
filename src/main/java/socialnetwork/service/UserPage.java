package socialnetwork.service;

import socialnetwork.domain.*;

import java.util.List;


public class UserPage {

    private SuperService superService;
    private Long userID;
    private String userName;
    private String userFirstName;
    private String userLastName;
    private User loggedInUser;

    public UserPage(SuperService superService, User user) {
        this.superService = superService;
        this.userID = user.getId();
        this.userName = user.getUserName();
        this.userFirstName = user.getFirstName();
        this.userLastName = user.getLastName();
        this.loggedInUser = user;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public List<UserAndFriendshipRequestDTO> getNextPageFriendshipRequestsSent(int page) {
        return this.superService.getFriendshipRequestService().getNextUserFriendshipRequestsSentBy(userID, page);
    }

    public List<UserAndFriendshipRequestDTO> getNextPageFriendshipRequestsReceived(int page) {
        return this.superService.getFriendshipRequestService().getNextUserFriendshipRequestsReceivedBy(userID, page);
    }

    public List<MessageDTO> getUserSentMessages(int page) {
        return this.superService.getMessageService().getNextMessagesSentBy(userID, page);
    }

    public List<MessageDTO> getUserReceivedMessages(int page) {
        return this.superService.getMessageService().getNextMessagesReceivedBy(userID, page);
    }

    public List<Event> getAllEvents(int page) {
        return this.superService.getEventService().getEventsOnPage(page);
    }

    public List<Event> getAllJoinedEvents(int page) {
        return this.superService.getEventService().getAttendeeEventsOnPage(page, userID);
    }

    public List<Event> getAllHostedEvents(int page) {
        return this.superService.getEventService().getHostEventsOnPage(page, userID);
    }

    public List<User> searchUsers(String firstString, String lastString, int page) {
        return this.superService.getUserService().searchUsersByName(firstString, lastString, page);
    }

    public List<Notification> getAllNotifications(int page) {
        return this.superService.getNotificationService().getUserNotificationsOnPage(page, userID);
    }

    public List<FriendAndDateDTO> getNextPageFriends(int page) {
        return this.superService.getFriendshipService().getNextFriendAndDateDTOsUser(userID, page);
    }

    public String getUserName() {
        return userName;
    }

    public SuperService getSuperService() {
        return superService;
    }

    public Long getUserID() {
        return userID;
    }

    public Integer getNumberOfFriends() {
        return this.superService.getFriendshipService().getNumberOfFriendsUser(userID);
    }

    public Integer getNumberOfReceivedMessages() {
        return this.superService.getMessageService().getNumberOfReceivedMessagesUser(userID);
    }

    public Integer getNumberOfSentMessages() {
        return this.superService.getMessageService().getNumberOfSentMessagesUser(userID);
    }

    public Integer getNumberOfFriendshipsReceived() {
        return this.superService.getFriendshipRequestService().getNumberOfReceivedFriendshipRequestsUser(userID);
    }

    public Integer getNumberOfFriendshipsSent() {
        return this.superService.getFriendshipRequestService().getNumberOfSentFriendshipRequestsUser(userID);
    }

    public Integer getNumberOfUsersSearch(String firstString, String secondString) {
        return this.superService.getUserService().getNumberOfUsersForSearch(firstString, secondString);
    }

    public Integer getTotalNumberOfEvents() {
        return this.superService.getEventService().getNumberOfAllEvents();
    }

    public Integer getTotalNumberOfHostedEvents() {
        return this.superService.getEventService().getNumberOfAllEventsHost(userID);
    }

    public Integer getTotalNumberOfJoinedEvents() {
        return this.superService.getEventService().getNumberOfAllEventsAttendee(userID);
    }

    public Integer getTotalNumberOfNotifications() {
        return this.superService.getNotificationService().getNumberOfNotificationsUser(userID);
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }
}
