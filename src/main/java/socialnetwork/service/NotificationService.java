package socialnetwork.service;

import socialnetwork.domain.Event;
import socialnetwork.domain.Notification;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.paging.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class NotificationService {

    private final NotificationPagingRepository notificationRepo;
    private final EventPagingRepository eventRepo;
    private final UserPagingRepository userRepo;
    private final int size = 5;

    public NotificationService(NotificationPagingRepository notificationRepo, EventPagingRepository eventRepo, UserPagingRepository userRepo) {
        this.notificationRepo = notificationRepo;
        this.eventRepo = eventRepo;
        this.userRepo = userRepo;
    }

    /**
     *
     * @param event - Event
     * @param userID - Long
     * @param lastDate - LocalDateTime
     * @param description - String
     * Saves a notification, that is specific to the user having the given ID, into application
     */
    public void addNotification(Event event, Long userID, LocalDateTime lastDate, String description) {

        if(eventRepo.findOne(event.getId()).isEmpty())
            throw new ValidationException("The event doesn't exist!\n");

        if(userRepo.findOne(userID).isEmpty())
            throw new ValidationException("The user doesn't exist!\n");

        Notification notification = new Notification();
        notification.setEvent(event);
        notification.setAttendeeID(userID);
        notification.setLastDate(lastDate);
        notification.setDescription(description);
        notification.setToShow(false);

        notificationRepo.save(notification);
    }

    /**
     *
     * @param pageNumber - int
     * @param id - Long
     * @return a {@code List<Notification>} containing the notifications of the user having the given id, from a given page
     */
    public List<Notification> getUserNotificationsOnPage(int pageNumber, Long id) {
        Pageable pageable = new PageableImplementation(pageNumber, this.size);
        PageV2<Notification> notificationPage = notificationRepo.findAllUserNotifications(pageable, id);
        return notificationPage.getContent().collect(Collectors.toList());
    }

    /**
     *
     * @param id - Long
     * @return the number of all notifications that belong to a user
     */
    public Integer getNumberOfNotificationsUser(Long id) {
        return notificationRepo.getNumberOfNotificationsUser(id);
    }

    public List<Notification> getNotificationsOnPage(int pageNumber) {
        Pageable pageable = new PageableImplementation(pageNumber, this.size);
        PageV2<Notification> notificationPage = notificationRepo.findAll(pageable);
        return notificationPage.getContent().collect(Collectors.toList());
    }

    /**
     *
     * @return the number of all notifications stored into application
     */
    public Integer getTotalNumberOfNotifications() {
        return notificationRepo.getTotalNumberOfNotifications();
    }

    /**
     *
     * @param id - Long
     * @return the user having the given id - if the user exists
     *                                      - @throws ValidationException otherwise
     */
    public User getUserByID(Long id) {
        if(userRepo.findOne(id).isEmpty())
            throw new ValidationException("The user does not exist!\n");
        return userRepo.findOne(id).get();
    }

    public void updateNotification(Notification notification) {
        notificationRepo.update(notification);
    }


}
