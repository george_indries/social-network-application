package socialnetwork.service;

import javafx.scene.image.Image;
import org.mindrot.jbcrypt.BCrypt;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class UserService {

    private final UserPagingRepository userRepo;
    private final FriendshipPagingRepository friendshipRepo;
    private final FriendshipRequestPagingRepository friendshipRequestRepo;
    private final int size = 5;

    /**
     * UserService Constructor
     * @param userRepo - reference to the Users' Repository
     * @param friendshipRepo - reference to the Friendships' Repository
     */
    public UserService(UserPagingRepository userRepo, FriendshipPagingRepository friendshipRepo, FriendshipRequestPagingRepository friendshipRequestRepo) {
        this.userRepo = userRepo;
        this.friendshipRepo = friendshipRepo;
        this.friendshipRequestRepo = friendshipRequestRepo;
    }


    /**
     * Adds a user into application
     * @param firstName - String
     * @param lastName - String
     */
    public void addUser(String firstName, String lastName) {

        User user = new User(firstName, lastName);
        userRepo.save(user);

    }


    public void saveUser(String firstName, String lastName, String userName, String userPassword, String userPasswordConfirm) {

        if(!userPassword.equals(userPasswordConfirm)) {
            throw new ValidationException("The password confirmation is not correct!\n");
        }

        User user = new User(firstName, lastName);
        user.setUserName(userName);
        user.setUserPassword(BCrypt.hashpw(userPassword, BCrypt.gensalt()));
        user.setNotifiable(true);

        if(findUserByUserName(userName).isPresent()) {
            throw new ValidationException("This username is already taken!\nPlease choose another username!\n");
        }

        userRepo.save(user);
    }

    public void updateUser(Long userID, String firstName, String lastName, String username, String password, boolean isNotifiable, Image photo) {

        User user = new User(firstName, lastName);
        user.setId(userID);
        user.setUserName(username);
        user.setUserPassword(password);
        user.setNotifiable(isNotifiable);
        if(photo != null)
            user.setImage(photo);

        userRepo.update(user);
    }

    private void validateLoginData(String userName, String userPassword) {

        List<String> errors = new ArrayList<>();

        Matcher matcher = Constants.VALID_ADDRESS_REGEX.matcher(userName);
        if(!matcher.find()) {
            errors.add("Invalid username!\n");
        }

        if(userPassword.trim().equals("")) {
            errors.add("Empty password field!\n");
        }

        if(userPassword.length() < 6) {
            errors.add("The password must contain at least 6 characters!\n");
        }

        String errorMessage = errors.stream()
                .reduce("", String::concat);

        if(!errors.isEmpty()) {
            throw new ValidationException(errorMessage);
        }

    }


    public User userLogin(String userName, String userPassword) {

        validateLoginData(userName, userPassword);

        if(findUserByUserName(userName).isEmpty()) {
            throw new ValidationException("This username doesn't exists!\n");
        }

        /*if(!findUserByUserName(userName).get().getUserPassword().equals(userPassword)) {
            throw new ValidationException("Wrong password!\n");
        }*/

        if(!BCrypt.checkpw(userPassword, findUserByUserName(userName).get().getUserPassword())) {
            throw new ValidationException("Wrong password!\n");
        }

        return findUserByUserName(userName).get();

    }


    /**
     * Deletes all friends of a user by a given id
     * @param ID - Long
     */
    private void deleteAllFriends(Long ID) {

        List<Friendship> friendshipsWithID =
                StreamSupport.stream(friendshipRepo.findAll().spliterator(), false)
                        .filter(x -> x.getId().getLeft().equals(ID) || x.getId().getRight().equals(ID))
                        .collect(Collectors.toList());

        friendshipsWithID.forEach(x -> friendshipRepo.delete(x.getId()));

    }


    /**
     * Deletes all friendship requests of a user by a given id
     * @param userID - Long
     */
    private void deleteFriendshipRequests(Long userID) {

        List<FriendshipRequest> friendshipRequestsWithID =
                StreamSupport.stream(friendshipRequestRepo.findAll().spliterator(), false)
                        .filter(x -> x.getId().getLeft().equals(userID) || x.getId().getRight().equals(userID))
                        .collect(Collectors.toList());

        friendshipRequestsWithID.forEach(x -> friendshipRequestRepo.delete(x.getId()));

    }


    /**
     * Delete a user from application by a given id
     * @param ID - Long
     */
    public void deleteUser(Long ID) {

        if (userRepo.delete(ID).isEmpty()) {
            throw new ValidationException("Non-existent ID!\n");
        }

        deleteAllFriends(ID);
        deleteFriendshipRequests(ID);

    }


    public List<User> searchUsersByName(String strLastName, String strFirstName) {

        Predicate<User> byLastName = x -> x.getLastName().contains(strLastName);
        Predicate<User> byFirstName = x -> x.getFirstName().contains(strFirstName);
        Predicate<User> composedPredicate = byLastName.and(byFirstName);

        return getAllUsersList()
                .stream()
                .filter(composedPredicate)
                .collect(Collectors.toList());
    }


    public List<User> searchUsers(String firstString, String secondString) {

        List<User> result = new ArrayList<>();

        searchUsersByName(secondString, firstString).forEach(x -> result.add(x));
        searchUsersByName(firstString, secondString).forEach(x -> {
            if(!result.contains(x))
                result.add(x);
        });

        return result;
    }

    /**
     *
     * @param userID - Long
     * @return an {@code Optional<User>} - contains the user having the given id if they exist in DB
     *                                   - empty if there is no user having the given id
     */
    public Optional<User> findUser(Long userID) {
        return userRepo.findOne(userID);
    }


    /**
     *
     * @param userName - String
     * @return an {@code Optional<User>} - contains the user having the given username if the userName exists in DB
     *                                   - empty if there is no user having the given username
     */
    public Optional<User> findUserByUserName(String userName) {

        List<User> users =  getAllUsersList()
                .stream()
                .filter(x -> x.getUserName().equals(userName))
                .collect(Collectors.toList());

        if(users.toArray().length != 0)
            return Optional.ofNullable(users.get(0));
        return Optional.ofNullable(null);
    }

    /**
     *
     * @return {@code List<User>} containing all users saved into application
     */
    public List<User> getAllUsersList() {

        return StreamSupport
                .stream(userRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());

    }


    public List<User> searchUsersByName(String firstString, String secondString, int pageNumber) {
        return getUsersOnPage(firstString, secondString, pageNumber);
    }


    public List<User> getUsersOnPage(String firstString, String secondString, int pageNumber) {
        Pageable pageable = new PageableImplementation(pageNumber, this.size);
        PageV2<User> pageUser = userRepo.findAllUsersByName(pageable, firstString, secondString);
        return pageUser.getContent().collect(Collectors.toList());
    }


    /**
     *
     * @return all users from application
     */
    public Iterable<User> getAll() {
        return userRepo.findAll();
    }

    public Integer getNumberOfUsersForSearch(String firstString, String secondString) {
        return userRepo.getNumberOfUsers(firstString, secondString);
    }

}
