package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.paging.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessageService {

    private final MessagePagingRepository messageRepo;
    private final UserPagingRepository userRepo;
    private final int size = 5;

    public MessageService(MessagePagingRepository messageRepo, UserPagingRepository userRepo) {
        this.messageRepo = messageRepo;
        this.userRepo = userRepo;
    }

    /**
     *
     * @param IDs - ID List
     * @throws ValidationException if at least one ID from the given list doesn't exist
     */
    void checkIfUsersExist(List<Long> IDs) {

        List<String> errors = new ArrayList<>();

        IDs.forEach(x -> {
            if(userRepo.findOne(x).isEmpty())
                errors.add("The user with ID " + x + " doesn't exist!\n");
        });

        String errorMessage = errors.stream()
                .reduce("", String::concat);

        if(!errorMessage.isEmpty()) {
            throw new ValidationException(errorMessage);
        }

    }

    /**
     *
     * @param fromID - Long
     * @param text - String
     * @param toID - Long
     * Send a message to a user being given the parameters above
     */
    public void sendMessage(Long fromID, String text, Long toID) {

        List<Long> IDs = new ArrayList<>();
        IDs.add(fromID);
        IDs.add(toID);
        checkIfUsersExist(IDs);

        Message message = new Message();
        message.setFromID(fromID);
        message.setMessage(text);
        message.addToListID(toID);
        message.setDate(LocalDateTime.now());

        messageRepo.save(message);
    }

    /**
     *
     * @param fromID - Long
     * @param text - String
     * @param toIDs - ID List
     * Send a message to at least one user from application being given the parameters above
     */
    public void sendToMany(Long fromID, String text, List<Long> toIDs) {

        toIDs.remove(fromID);

        List<Long> IDs = new ArrayList<>();
        IDs.add(fromID);
        IDs.addAll(toIDs);
        checkIfUsersExist(IDs);

        Message message = new Message();
        message.setFromID(fromID);
        message.setMessage(text);
        for(Long ID: toIDs)
            message.addToListID(ID);
        message.setDate(LocalDateTime.now());

        messageRepo.save(message);
    }

    /**
     *
     * @param fromID - Long
     * @param text - String
     * @param toID - Long
     * @param replyID - Long
     * Reply with a message to a user being given the parameters above
     */
    public void replyToMessage(Long fromID, String text, Long toID, Long replyID) {

        List<Long> listOfIDs = new ArrayList<>();
        listOfIDs.add(fromID);
        listOfIDs.add(toID);
        checkIfUsersExist(listOfIDs);

        if(messageRepo.findOne(replyID).isEmpty()) {
            throw new ValidationException("The message you want to reply doesn't exist!\n");
        } else {
            if(!messageRepo.findOne(replyID).get().getToListID().contains(fromID))
                throw new ValidationException("You cannot reply to this message!\n");
        }

        Message replyMessage = new Message();
        replyMessage.setFromID(fromID);
        replyMessage.setMessage(text);
        replyMessage.addToListID(toID);
        replyMessage.setDate(LocalDateTime.now());
        replyMessage.setReplyMessageID(replyID);

        messageRepo.save(replyMessage);
    }

    /**
     *
     * @param fromID - Long
     * @param message - Message
     * @return a UserAndMessageDTO representing all information about a message and the user who sent it
     */
    private UserAndMessageDTO getFromUserAndMessage(Long fromID, Message message) {

        User fromUser = null;

        if(userRepo.findOne(fromID).isPresent())
            fromUser = userRepo.findOne(fromID).get();

        return new UserAndMessageDTO(fromUser, message);

    }

    /**
     *
     * @param firstID - Long
     * @param secondID - Long
     * @return {@code List<UserAndMessageDTO>} representing all messages between two users, sorted by date
     */
    public List<UserAndMessageDTO> getUsersMessagesSortedByDate(Long firstID, Long secondID) {

        return getAllMessagesList()
                .stream()
                .filter(x -> (x.getFromID().equals(firstID) && x.getToListID().contains(secondID)) ||
                            (x.getFromID().equals(secondID) && x.getToListID().contains(firstID)))
                .sorted((x,y) -> x.getDate().compareTo(y.getDate()))
                .map(x -> getFromUserAndMessage(x.getFromID(), x))
                .collect(Collectors.toList());

    }

    /**
     *
     * @param message - Message
     * @param userID - Long
     * @return a MessageDTO representing all needed information about a Message:
     *                      - the User who sent the Message
     *                      - the Users List who received the Message
     *                      - the Message
     */
    private MessageDTO getMessageDTO(Message message, Long userID) {

        User fromUser = null;
        List<User> toUsersList = new ArrayList<>();

        if(userRepo.findOne(userID).isPresent())
            fromUser = userRepo.findOne(userID).get();

        for(Long id: message.getToListID()) {
            if(userRepo.findOne(id).isPresent())
                toUsersList.add(userRepo.findOne(id).get());
        }

        MessageDTO messageDTO = new MessageDTO(fromUser, toUsersList, message);

        if(message.getReplyMessageID() != null && messageRepo.findOne(message.getReplyMessageID()).isPresent())
            messageDTO.setReplyMessage(messageRepo.findOne(message.getReplyMessageID()).get());

        return messageDTO;
    }

    /**
     *
     * @param userID - Long
     * @return a {@code List<MessageDTO>} representing all messages (sorted by date) of the user having the given id as parameter
     */
    public List<MessageDTO> getAllMessagesSortedByDate(Long userID) {

        return getAllMessagesList()
                .stream()
                .filter(x -> (x.getFromID().equals(userID) || x.getToListID().contains(userID)))
                .sorted((x,y) -> x.getDate().compareTo(y.getDate()))
                .map(x -> getMessageDTO(x, x.getFromID()))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @return a {@code List<MessageDTO>} representing all received messages (sorted by date) by the user having the given id as parameter
     */
    public List<MessageDTO> getAllReceivedMessagesSortedByDate(Long userID) {

        return getAllMessagesSortedByDate(userID)
                .stream()
                .filter(x -> x.getMessage().getToListID().contains(userID))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @return a {@code List<MessageDTO>} representing all sent messages (sorted by date) by the user having the given id as parameter
     */
    public List<MessageDTO> getAllSentMessagesSortedByDate(Long userID) {

        return getAllMessagesSortedByDate(userID)
                .stream()
                .filter(x -> x.getMessage().getFromID().equals(userID))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @param initialDate - LocalDateTime
     * @param finalDate - LocalDateTime
     * @return a {@code List<MessageDTO>} containing all received messages from a given period of time by the user having the id userID
     */
    public List<MessageDTO> getAllReceivedMessagesDuringAPeriodOfTime(Long userID, LocalDate initialDate, LocalDate finalDate) {

        return getAllReceivedMessagesSortedByDate(userID)
                .stream()
                .filter(x -> x.getLocalDate().compareTo(initialDate) >= 0 && x.getLocalDate().compareTo(finalDate) <= 0)
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @param friendID - Long
     * @param initialDate - LocalDateTime
     * @param finalDate - LocalDateTime
     * @return a {@code List<MessageDTO>} containing all received messages of the user having userID from a friend with friendID, from a given period of time
     */
    public List<MessageDTO> getAllReceivedMessagesFromAFriendDuringAPeriodOfTime(Long userID, Long friendID, LocalDate initialDate, LocalDate finalDate) {

        return getAllReceivedMessagesSortedByDate(userID)
                .stream()
                .filter(x -> x.getFromUser().getId().equals(friendID))
                .filter(x -> x.getLocalDate().compareTo(initialDate) >= 0 && x.getLocalDate().compareTo(finalDate) <= 0)
                .collect(Collectors.toList());
    }

    /**
     *
     * @return a {@code List<Long>} containing every IDs for all users from application
     */
    public List<Long> getAllUsersIDs() {

        return StreamSupport.stream(userRepo.findAll().spliterator(), false)
                .map(x -> x.getId())
                .collect(Collectors.toList());
    }

    /**
     *
     * @return Message List containing all messages saved into application
     */
    public List<Message> getAllMessagesList() {

        return StreamSupport
                .stream(messageRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    /**
     *
     * @return al messages from application
     */
    public Iterable<Message> getAll() {
        return messageRepo.findAll();
    }

    public List<MessageDTO> getNextMessagesSentBy(Long userID, int pageNumber) {
        return getMessagesOnPageSent(pageNumber, userID)
                .stream()
                .map(x -> getMessageDTO(x, x.getFromID()))
                .collect(Collectors.toList());
    }

    public List<Message> getMessagesOnPageSent(int page, Long userID) {
        Pageable pageable = new PageableImplementation(page, this.size);
        PageV2<Message> messagePage = messageRepo.findAllSentMessagesByUserIdOrderedByDate(pageable, userID);
        return messagePage.getContent().collect(Collectors.toList());
    }

    public List<MessageDTO> getNextMessagesReceivedBy(Long userID, int pageNumber) {
        return getMessagesOnPageReceived(pageNumber, userID)
                .stream()
                .map(x -> getMessageDTO(x, x.getFromID()))
                .collect(Collectors.toList());
    }

    public List<Message> getMessagesOnPageReceived(int page, Long userID) {
        Pageable pageable = new PageableImplementation(page, this.size);
        PageV2<Message> messagePage = messageRepo.findAllReceivedMessagesByUserIdOrderedByDate(pageable, userID);
        return messagePage.getContent().collect(Collectors.toList());
    }

    public Integer getNumberOfSentMessagesUser(Long userID) {
        return messageRepo.getNumberOfSentMessages(userID);
    }

    public Integer getNumberOfReceivedMessagesUser(Long userID) {
        return messageRepo.getNumberOfReceivedMessages(userID);
    }

}
