package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.NetworkGraph;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendshipService implements Observable<FriendshipChangeEvent> {

    private final FriendshipPagingRepository friendshipRepo;
    private final UserPagingRepository userRepo;
    private final FriendshipRequestPagingRepository friendshipRequestRepo;
    private final int size = 5;
    private List<Observer<FriendshipChangeEvent>> observers = new ArrayList<>();

    /**
     * FriendshipService Constructor
     * @param friendshipRepo - reference to the Friendships' Repository
     * @param userRepo - reference to the Users' Repository
     */
    public FriendshipService(FriendshipPagingRepository friendshipRepo, UserPagingRepository userRepo, FriendshipRequestPagingRepository friendshipRequestRepo) {
        this.userRepo = userRepo;
        this.friendshipRepo = friendshipRepo;
        this.friendshipRequestRepo = friendshipRequestRepo;
    }

    /**
     * Checks the validity of a friendship ID
     * @param leftID - Long
     * @param rightID - Long
     */
    public void checkIDValidity(Long leftID, Long rightID) {

        String message = "";

        if(userRepo.findOne(leftID).isEmpty()) {
            message = message.concat("The first user doesn't exist!\n");
        }

        if(userRepo.findOne(rightID).isEmpty()) {
            message = message.concat("The second user doesn't exist!\n");
        }

        if(!message.equals(""))
            throw new ValidationException(message);
    }

    /**
     * Adds a friendship in application
     * @param leftID - Long
     * @param rightID - Long
     */
    public void addFriendship(Long leftID, Long rightID) {

        Friendship friendship = new Friendship();
        friendship.setId(new Tuple<>(leftID, rightID));
        friendship.setDate(LocalDateTime.now());

        Friendship possibleFriendship = new Friendship();
        possibleFriendship.setId(new Tuple<>(rightID, leftID));
        possibleFriendship.setDate(LocalDateTime.now());

        checkIDValidity(leftID, rightID);

        if (friendshipRepo.findOne(friendship.getId()).isPresent() || friendshipRepo.findOne(possibleFriendship.getId()).isPresent())
            throw new ValidationException("You are already friends!\n");

        friendshipRepo.save(friendship);
    }

    /**
     * Deletes a friendship from application
     * @param leftID - Long
     * @param rightID - Long
     */
    public void deleteFriendship(Long leftID, Long rightID) {

        Tuple<Long, Long> friendshipID = new Tuple<>(leftID, rightID);
        Tuple<Long, Long> possibleFriendshipID = new Tuple<>(rightID, leftID);

        checkIDValidity(leftID, rightID);

        Friendship deletedFriendship = null;

        if (friendshipRepo.findOne(friendshipID).isEmpty() && friendshipRepo.findOne(possibleFriendshipID).isEmpty())
            throw new ValidationException("You are not friend with this user!\n");
        else if (friendshipRepo.findOne(friendshipID).isPresent()) {
            deletedFriendship = friendshipRepo.findOne(friendshipID).get();
            friendshipRepo.delete(friendshipID);
        }
        else if(friendshipRepo.findOne(possibleFriendshipID).isPresent()) {
            deletedFriendship = friendshipRepo.findOne(possibleFriendshipID).get();
            friendshipRepo.delete(possibleFriendshipID);
        }

        if(friendshipRequestRepo.findOne(new Tuple<>(leftID, rightID)).isPresent()) {
            friendshipRequestRepo.delete(new Tuple<>(leftID, rightID));
            notifyObserver(new FriendshipChangeEvent(ChangeEventType.DELETE, deletedFriendship));
        } else if(friendshipRequestRepo.findOne(new Tuple<>(rightID, leftID)).isPresent()) {
            friendshipRequestRepo.delete(new Tuple<>(rightID, leftID));
            notifyObserver(new FriendshipChangeEvent(ChangeEventType.DELETE, deletedFriendship));
        }

    }

    /**
     *
     * @param friendshipID - Long
     * @return a Friendship - if it exists in application, having the given id as parameter
     *                      - null, otherwise - there is no friendship with the given ID
     */
    public Friendship findFriendship(Tuple<Long, Long> friendshipID) {

        if(friendshipRepo.findOne(friendshipID).isPresent()) {
            return friendshipRepo.findOne(friendshipID).get();
        }

        return null;
    }


    public Friendship getFriendshipTwoUsers(Long firstID, Long secondID) {

        if(friendshipRepo.findOne(new Tuple<>(firstID, secondID)).isPresent()) {
            return friendshipRepo.findOne(new Tuple<>(firstID, secondID)).get();
        } else if(friendshipRepo.findOne(new Tuple<>(secondID, firstID)).isPresent()) {
            return friendshipRepo.findOne(new Tuple<>(secondID, firstID)).get();
        }

        return null;
    }

    /**
     *
     * @param ID - Long
     * @return the list with all friends of a user by a given ID as parameter
     */
    public List<User> getUserFriends(Long ID) {

        List<Friendship> friendships = StreamSupport
                .stream(friendshipRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());

        Map<Long, User> friendsMap = new HashMap<>();

        for(Friendship friendship: friendships) {
            if (friendship.getId().getLeft().equals(ID)) {
                Optional<User> found = userRepo.findOne(friendship.getId().getRight());
                if(found.isPresent()) {
                    friendsMap.putIfAbsent(found.get().getId(), found.get());
                }

            } else if (friendship.getId().getRight().equals(ID)) {
                Optional<User> found = userRepo.findOne(friendship.getId().getLeft());
                if(found.isPresent()) {
                    friendsMap.putIfAbsent(found.get().getId(), found.get());
                }
            }
        }

        return new ArrayList<>(friendsMap.values());
    }

    /**
     * Loads in memory all friends for every user
     * @return the list with all users from application, every user having loaded his/her friends
     */
    public List<User> loadAllFriendsForAllUsers() {

        List<User> users = StreamSupport
                .stream(userRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());

        for(User user: users) {

            List<User> userFriends = getUserFriends(user.getId());
            for(User friend: userFriends) {
                user.add_friend(friend);
            }

        }

        return users;
    }


    /**
     *
     * @return the number of all existent communities from application
     */
    public Integer getNumberOfCommunities() {

        NetworkGraph networkGraph = new NetworkGraph(loadAllFriendsForAllUsers());
        return networkGraph.countConnectedComponents();
    }


    /**
     *
     * @return a list of users representing all users from the most sociable community
     */
    public List<User> getMostSociableCommunity() {

        NetworkGraph networkGraph = new NetworkGraph(loadAllFriendsForAllUsers());
        return new ArrayList<>(networkGraph.getLargestComponent());
    }

    /**
     *
     * @param friendship - Friendship
     * @param ID - Long
     * @return a FriendAndDateDTO formed by User and Friendship
     *           - User - the friend of the user having the given ID
     *           - Friendship - their friendship
     */
    private FriendAndDateDTO getFriendAndDate(Friendship friendship, Long ID) {

        User possibleFriend1 = null, possibleFriend2 = null;

        if(userRepo.findOne(friendship.getId().getLeft()).isPresent())
            possibleFriend1 = userRepo.findOne(friendship.getId().getLeft()).get();
        if(userRepo.findOne(friendship.getId().getRight()).isPresent())
            possibleFriend2 = userRepo.findOne(friendship.getId().getRight()).get();

        FriendAndDateDTO friendAndDateDTO = null;

        if(possibleFriend1 != null && !possibleFriend1.getId().equals(ID))
            friendAndDateDTO = new FriendAndDateDTO(possibleFriend1, friendship);
        else if(possibleFriend2 != null && !possibleFriend2.getId().equals(ID))
            friendAndDateDTO = new FriendAndDateDTO(possibleFriend2, friendship);

        return  friendAndDateDTO;

    }


    /**
     *
     * @param ID - Long
     * @return - {@code List<Tuple<User, Friendship>>}
     *         - a list containing all friends of a user and the date they became friends
     */
    public List<FriendAndDateDTO> getAllFriendshipsUser(Long ID) {

        if(userRepo.findOne(ID).isEmpty())
            throw new ValidationException("This user doesn't exist!\n");

        List<Friendship> allFriendships = getAllFriendshipsList();

        return allFriendships
                .stream()
                .filter(x -> x.getId().getLeft().equals(ID) || x.getId().getRight().equals(ID))
                .map(x -> getFriendAndDate(x, ID))
                .collect(Collectors.toList());

    }


    public List<FriendAndDateDTO> searchFriendsByName(Long userID, String strLastName, String strFirstName) {

        Predicate<FriendAndDateDTO> byLastName = x -> x.getFriend().getLastName().contains(strLastName);
        Predicate<FriendAndDateDTO> byFirstName = x -> x.getFriend().getFirstName().contains(strFirstName);
        Predicate<FriendAndDateDTO> composedPredicate = byLastName.and(byFirstName);

        return getAllFriendshipsUser(userID)
                .stream()
                .filter(composedPredicate)
                .collect(Collectors.toList());
    }

    public List<FriendAndDateDTO> searchFriends(Long userID, String firstString, String secondString) {

        if(searchFriendsByName(userID, firstString, secondString).isEmpty())
            return searchFriendsByName(userID, secondString, firstString);

        return searchFriendsByName(userID, firstString, secondString);
    }

    public List<User> searchFriendsByName2(Long userID, String strLastName, String strFirstName) {

        Predicate<FriendAndDateDTO> byLastName = x -> x.getFriend().getLastName().contains(strLastName);
        Predicate<FriendAndDateDTO> byFirstName = x -> x.getFriend().getFirstName().contains(strFirstName);
        Predicate<FriendAndDateDTO> composedPredicate = byLastName.and(byFirstName);

        return getAllFriendshipsUser(userID)
                .stream()
                .filter(composedPredicate)
                .map(x -> x.getFriend())
                .collect(Collectors.toList());
    }


    public List<User> searchFriends2(Long userID, String firstString, String secondString) {

        List<User> result = new ArrayList<>();

        searchFriendsByName2(userID, secondString, firstString).forEach(x -> result.add(x));
        searchFriendsByName2(userID, firstString, secondString).forEach(
                x -> {
                    if(!result.contains(x))
                        result.add(x);
                }
        );

        return result;
    }

    /**
     *
     * @param ID - Long
     * @param initialDate - LocalDate
     * @param finalDate - LocalDate
     * @return a {@code List<FriendAndDateDTO>} containing all needed data about the friends added during a given period of time of the user having the id ID
     */
    public List<FriendAndDateDTO> getAllNewFriendshipsUserDuringAPeriodOfTime(Long ID, LocalDate initialDate, LocalDate finalDate) {

        return getAllFriendshipsUser(ID)
                .stream()
                .filter(x -> x.getDate().toLocalDate().compareTo(initialDate) >= 0 && x.getDate().toLocalDate().compareTo(finalDate) <= 0)
                .collect(Collectors.toList());
    }


    /**
     *
     * @param ID - Long
     * @param month - Month
     * @return - {@code List<Tuple<User, Friendship>>}
     *         - a filtered by month list containing all friends of a user and the date the became friends
     */
    public List<FriendAndDateDTO> getAllFriendshipsUserByMonth(Long ID, Month month) {

        Predicate<FriendAndDateDTO> byMonth = x -> x.getDate().getMonth().equals(month);

        return getAllFriendshipsUser(ID)
                .stream()
                .filter(byMonth)
                .collect(Collectors.toList());

    }


    /**
     *
     * @return a User List containing all users saved into application
     */
    public List<Friendship> getAllFriendshipsList() {

        return StreamSupport
                .stream(friendshipRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());

    }

    /**
     *
     * @return all friendships from application
     */
    public Iterable<Friendship> getAll() {
        return friendshipRepo.findAll();
    }

    /**
     *
     * @param userID - Long
     * @return the number of friends for the user having the given id
     */
    public int getNumberOfFriendsUser(Long userID) {
        return friendshipRepo.getNumberOfFriends(userID);
    }


    /**
     *
     * @param userID - Long
     * @param pageNumber - int
     * @return all friends of a given user from a given page
     */
    public List<User> getNextFriendsUser(Long userID, int pageNumber) {
        return getNextFriendships(userID, pageNumber)
                .stream()
                .map(x -> {
                    if(x.getId().getLeft().equals(userID)) {
                        if(userRepo.findOne(x.getId().getRight()).isPresent())
                            return userRepo.findOne(x.getId().getRight()).get();
                    } else {
                        if(userRepo.findOne(x.getId().getLeft()).isPresent())
                            return userRepo.findOne(x.getId().getLeft()).get();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @param pageNumber - int
     * @return a {@code List<FriendAndDateDTO>} containing all friends of the user having the given userID
     */
    public List<FriendAndDateDTO> getNextFriendAndDateDTOsUser(Long userID, int pageNumber) {
        return getNextFriendships(userID, pageNumber)
                .stream()
                .map(x -> {
                    if(x.getId().getLeft().equals(userID)) {
                        return getFriendAndDate(x, x.getId().getLeft());
                    } else {
                        return getFriendAndDate(x, x.getId().getRight());
                    }
                })
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @param pageNumber - int
     * @param firstString - String
     * @param secondString - String
     * @return a {@code List<FriendAndDateDTO>} containing all friends of the user having the id userID, filtered by name
     */
    public List<FriendAndDateDTO> getNextFriendAndDateDTOsUserSearchByName(Long userID, int pageNumber, String firstString, String secondString) {
        Pageable pageable = new PageableImplementation(pageNumber, this.size);
        PageV2<FriendAndDateDTO> page = friendshipRepo.findFriendsByName(pageable, userID, firstString, secondString);
        return page.getContent().collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @param pageNumber - int
     * @return a {@code List<Friendship>} containing all friendships from the given page
     */
    public List<Friendship> getNextFriendships(Long userID, int pageNumber) {
        return getFriendshipsOnPage(pageNumber, userID);
    }

    /**
     *
     * @param page - int
     * @param userID - Long
     * @return a {@code List<Friendship>} containing all friendships from the given page
     */
    public List<Friendship> getFriendshipsOnPage(int page, Long userID) {
        Pageable pageable = new PageableImplementation(page, this.size);
        PageV2<Friendship> friendshipPage = friendshipRepo.findAllFriendshipsByUserIdOrderedByDate(pageable, userID);
        return friendshipPage.getContent().collect(Collectors.toList());
    }

    @Override
    public void addObserver(Observer<FriendshipChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendshipChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObserver(FriendshipChangeEvent friendshipChangeEvent) {
        observers.stream().forEach(x -> x.update(friendshipChangeEvent));
    }
}
