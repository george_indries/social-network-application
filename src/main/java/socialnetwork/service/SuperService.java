package socialnetwork.service;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import socialnetwork.domain.FriendAndDateDTO;
import socialnetwork.domain.MessageDTO;
import socialnetwork.domain.User;
import socialnetwork.utils.Constants;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class SuperService {

    private final UserService userService;
    private final FriendshipService friendshipService;
    private final FriendshipRequestService friendshipRequestService;
    private final MessageService messageService;
    private final EventService eventService;
    private final NotificationService notificationService;


    public SuperService(UserService userService, FriendshipService friendshipService, FriendshipRequestService friendshipRequestService, MessageService messageService, EventService eventService, NotificationService notificationService) {
        this.userService = userService;
        this.friendshipService = friendshipService;
        this.friendshipRequestService = friendshipRequestService;
        this.messageService = messageService;
        this.eventService = eventService;
        this.notificationService = notificationService;
    }

    /**
     *
     * @param path - String
     * @param userID - Long
     * @param initialDate - LocalDate
     * @param finalDate - LocalDate
     * @throws FileNotFoundException - if the file does not exist and it cannot be created
     * @throws DocumentException
     * Creates a new PDF Document - for writing an activities report of the user having userID given as a parameter
     *                            - if the document already exists it is overwritten
     *                            - the generated report consists of a all activities of a user during a given
     *                              period of time (starting date - initialDate, ending date - finalDate)
     *                            - the activities represents all new created friends and all received messages during that period of time
     */
    public void writeReportActivitiesToPDF(String path, Long userID, LocalDate initialDate, LocalDate finalDate) throws FileNotFoundException, DocumentException {

        Document document = new Document();
        User user;
        if(getUserService().findUser(userID).isPresent()) {
            user = getUserService().findUser(userID).get();
            PdfWriter.getInstance(document, new FileOutputStream(path + "/ActivitiesReport" + user.getFirstName() + user.getLastName() + ".pdf"));
        }
        document.open();
        document.addTitle("Activities Report");

        Paragraph paragraph = new Paragraph();
        paragraph.setFont(new Font(Font.FontFamily.COURIER, 18));
        paragraph.add("New friends created between " + initialDate.toString() + " and " + finalDate.toString() + "\n\n");
        PdfPTable friendsTable = new PdfPTable(3);
        Stream.of("Firstname", "Lastname", "Date").forEach(friendsTable::addCell);

        List<FriendAndDateDTO> friendsList = getFriendshipService().getAllNewFriendshipsUserDuringAPeriodOfTime(userID, initialDate, finalDate);
        friendsList.forEach(x -> {
            friendsTable.addCell(x.getFirstName());
            friendsTable.addCell(x.getLastName());
            friendsTable.addCell(x.getDate().toLocalDate().toString());
        });
        paragraph.add(friendsTable);
        paragraph.add("\n\n");

        Paragraph paragraph2 = new Paragraph();
        paragraph2.setFont(new Font(Font.FontFamily.COURIER, 18));
        paragraph2.add("Received messages between " + initialDate.toString() + " and " + finalDate.toString() + "\n\n");
        List<MessageDTO> messagesList = getMessageService().getAllReceivedMessagesDuringAPeriodOfTime(userID, initialDate, finalDate);

        paragraph2.setFont(new Font(Font.FontFamily.COURIER, 14));
        messagesList.forEach(x -> {
            paragraph2.add("Received from " + x.getFromUser().getFirstName() + " " + x.getFromUser().getLastName() +
                    " on " + x.getLocalDate().toString() + ":\n");
            paragraph2.add(x.getMessage().getMessage() + "\n");
        });


        document.add(paragraph);
        document.add(paragraph2);
        document.add(new Paragraph("\nOn " + LocalDateTime.now().format(Constants.DATE_TIME_FORMATTER_SIMPLE)));
        document.close();
    }

    /**
     *
     * @param userID - Long
     * @param initialDate - LocalDateTime
     * @param finalDate - LocalDateTime
     * @return a {@code List<String>} consisting of more rows that compose the pdf preview for ReportActivities
     */
    public List<String> getReportActivitiesPreview(Long userID, LocalDate initialDate, LocalDate finalDate) {

        List<String> stringList = new ArrayList<>();

        stringList.add("New friends created between " + initialDate.toString() + " and " + finalDate.toString() + "\n\n");

        List<FriendAndDateDTO> friendsList = getFriendshipService().getAllNewFriendshipsUserDuringAPeriodOfTime(userID, initialDate, finalDate);
        stringList.add("First Name  Last Name  Date\n");
        friendsList.forEach(x -> {
            stringList.add(x.getFirstName() + "     " + x.getLastName() + "     " + x.getDate().toLocalDate().toString() + "\n");
        });
        stringList.add("\n\n");

        stringList.add("Received messages between " + initialDate.toString() + " and " + finalDate.toString() + "\n\n");
        List<MessageDTO> messagesList = getMessageService().getAllReceivedMessagesDuringAPeriodOfTime(userID, initialDate, finalDate);

        messagesList.forEach(x -> {
            stringList.add("Received from " + x.getFromUser().getFirstName() + " " + x.getFromUser().getLastName() +
                    " on " + x.getLocalDate().toString() + ":\n");
            stringList.add(x.getMessage().getMessage() + "\n");
        });

        stringList.add("\nOn " + LocalDateTime.now().format(Constants.DATE_TIME_FORMATTER_SIMPLE));

        return stringList;
    }

    /**
     *
     * @param path - String
     * @param userID - Long
     * @param friendID - Long
     * @param initialDate - LocalDate
     * @param finalDate - LocalDate
     * @throws FileNotFoundException - if the file does not exist and it cannot be created
     * @throws DocumentException
     * Creates a new PDF Document - for writing an messages report of the user having userID given as a parameter
     *                            - if the document already exists it is overwritten
     *                            - the generated report consists of a all messages received by the user having userID from his/her friend having friendID, during a given
     *                              period of time (starting date - initialDate, ending date - finalDate)
     */
    public void writeReportMessagesToPDF(String path, Long userID, Long friendID, LocalDate initialDate, LocalDate finalDate) throws FileNotFoundException, DocumentException {

        Document document = new Document();
        User user, friend;
        if(getUserService().findUser(userID).isPresent()) {
            user = getUserService().findUser(userID).get();
            PdfWriter.getInstance(document, new FileOutputStream(path + "/MessagesReport" + user.getFirstName() + user.getLastName() + ".pdf"));
        }
        document.open();
        document.addTitle("Messages Report");

        Paragraph paragraph = new Paragraph();
        paragraph.setFont(new Font(Font.FontFamily.COURIER, 18));
        List<MessageDTO> messagesList = getMessageService().getAllReceivedMessagesFromAFriendDuringAPeriodOfTime(userID, friendID, initialDate, finalDate);

        if(getUserService().findUser(friendID).isPresent()) {
            friend = getUserService().findUser(friendID).get();
            paragraph.add("Received messages from " + friend.getLastName() + " " + friend.getFirstName() + " between " + initialDate.toString() + " and " + finalDate.toString() + "\n\n");
        }

        paragraph.setFont(new Font(Font.FontFamily.COURIER, 14));
        messagesList.forEach(x -> {
            paragraph.add("On " + x.getLocalDate().toString() + ":\n");
            paragraph.add(x.getMessage().getMessage() + "\n");
        });


        document.add(paragraph);
        document.add(new Paragraph("\nOn " + LocalDateTime.now().format(Constants.DATE_TIME_FORMATTER_SIMPLE)));
        document.close();
    }

    /**
     *
     * @param userID - Long
     * @param friendID - Long
     * @param initialDate - LocalDateTime
     * @param finalDate - LocalDateTime
     * @return a {@code List<String>} consisting of more rows that compose the pdf preview for ReportMessages
     */
    public List<String> getReportMessagesPreview(Long userID, Long friendID, LocalDate initialDate, LocalDate finalDate) {

        User friend;
        List<String> stringList = new ArrayList<>();
        List<MessageDTO> messagesList = getMessageService().getAllReceivedMessagesFromAFriendDuringAPeriodOfTime(userID, friendID, initialDate, finalDate);

        if(getUserService().findUser(friendID).isPresent()) {
            friend = getUserService().findUser(friendID).get();
            stringList.add("Received messages from " + friend.getLastName() + " " + friend.getFirstName() + " between " + initialDate.toString() + " and " + finalDate.toString() + "\n\n");
        }

        messagesList.forEach(x -> {
            stringList.add("On " + x.getLocalDate().toString() + ":\n");
            stringList.add(x.getMessage().getMessage() + "\n");
        });

        stringList.add("\nOn " + LocalDateTime.now().format(Constants.DATE_TIME_FORMATTER_SIMPLE));

        return stringList;
    }

    /**
     *
     * @return a reference to UserService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     *
     * @return a reference to FriendshipService
     */
    public FriendshipService getFriendshipService() {
        return friendshipService;
    }

    /**
     *
     * @return a reference to FriendshipRequestService
     */
    public FriendshipRequestService getFriendshipRequestService() {
        return friendshipRequestService;
    }

    /**
     *
     * @return a reference to MessageService
     */
    public MessageService getMessageService() {
        return messageService;
    }

    /**
     *
     * @return a reference to EventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     *
     * @return a reference to NotificationService
     */
    public NotificationService getNotificationService() {
        return notificationService;
    }
}
