package socialnetwork.service;

import socialnetwork.domain.Attendee;
import socialnetwork.domain.Event;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.paging.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class EventService {

    private final EventPagingRepository eventRepo;
    private final UserPagingRepository userRepo;
    private final int size = 5;

    public EventService(EventPagingRepository eventRepo, UserPagingRepository userRepo) {
        this.eventRepo = eventRepo;
        this.userRepo = userRepo;
    }

    /**
     *
     * @param name - String
     * @param host - User
     * @param startDate - LocalDateTime
     * @param endDate - LocalDateTime
     * @param description - String
     * @param pathToImage - String
     * @param location - String
     * @param category - String
     * Stores an event created by a user(host) into application being given the parameters above
     */
    public void addEvent(String name, User host, LocalDateTime startDate, LocalDateTime endDate, String description, String pathToImage, String location, String category) {

        if(userRepo.findOne(host.getId()).isEmpty()) {
            throw new ValidationException("Non-existent user!\n");
        }

        Event event = new Event();
        event.setName(name);
        event.setDescription(description);
        event.setDate(startDate);
        event.setEndDate(endDate);
        event.setHost(host);
        event.setPathToImage(pathToImage);
        event.setLocation(location);
        event.setCategory(category);

        eventRepo.save(event);
    }

    /**
     *
     * @param userID - Long
     * @param eventID - Long
     * Register a user for an event, being given the id of the user id and the event id
     */
    public void addAttendeeToEvent(Long userID, Long eventID) {

        if(userRepo.findOne(userID).isEmpty()) {
            throw new ValidationException("Non-existent user!\n");
        }

        if(eventRepo.findOne(eventID).isEmpty()) {
            throw new ValidationException("Non-existent event!\n");
        }

        Event event = eventRepo.findOne(eventID).get();
        if(event.getHost().getId().equals(userID)) {
            throw new ValidationException("You are already the host of this event!\n");
        }

        User user = userRepo.findOne(userID).get();
        Attendee attendee = new Attendee(user, true);
        attendee.setId(userID);
        attendee.setEventID(eventID);

        List<Long> attendeesIDs = event.getAttendees().stream().map(x -> x.getId()).collect(Collectors.toList());
        if(attendeesIDs.contains(userID)) {
            throw new ValidationException("You have already joined to this event!\n");
        }

        eventRepo.save(attendee);
    }

    /**
     *
     * @param pageNumber - int
     * @return a {@code List<Event>} containing the events from a given page
     */
    public List<Event> getEventsOnPage(int pageNumber) {
        Pageable pageable = new PageableImplementation(pageNumber, this.size);
        PageV2<Event> eventPage = eventRepo.findAll(pageable);
        return eventPage.getContent().collect(Collectors.toList());
    }

    /**
     *
     * @param pageNumber - int
     * @param hostID - Long
     * @return a {@code List<Event>} containing the events from a given page that were created by the user having the given id
     */
    public List<Event> getHostEventsOnPage(int pageNumber, Long hostID) {
        Pageable pageable = new PageableImplementation(pageNumber, this.size);
        PageV2<Event> eventPage = eventRepo.findAllEventsUser(pageable, hostID);
        return eventPage.getContent().collect(Collectors.toList());
    }

    /**
     *
     * @param pageNumber - int
     * @param attendeeID - Long
     * @return a {@code List<Event>} containing the events from a given page which a user having the given id joined
     */
    public List<Event> getAttendeeEventsOnPage(int pageNumber, Long attendeeID) {
        Pageable pageable = new PageableImplementation(pageNumber, this.size);
        PageV2<Event> eventPage = eventRepo.findAllEventsAttendee(pageable, attendeeID);
        return eventPage.getContent().collect(Collectors.toList());
    }

    /**
     *
     * @return the number of all events saved into application
     */
    public Integer getNumberOfAllEvents() {
        return eventRepo.getTotalNumberOfEvents();
    }

    /**
     *
     * @param id - Long
     * @return the number of all events which the user having the given id joined
     */
    public Integer getNumberOfAllEventsAttendee(Long id) {
        return eventRepo.getNumberOfAllEventsAttendee(id);
    }

    /**
     *
     * @param id - Long
     * @return the number of all events which were created by the user having the given id
     */
    public Integer getNumberOfAllEventsHost(Long id) {
        return eventRepo.getNumberOfAllEventsUser(id);
    }
}
