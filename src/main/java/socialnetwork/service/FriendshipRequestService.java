package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.events.FriendshipRequestChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendshipRequestService {

    private final FriendshipPagingRepository friendshipRepo;
    private final UserPagingRepository userRepo;
    private final FriendshipRequestPagingRepository friendshipRequestRepo;
    private final FriendshipService friendshipService;
    private final int size = 5;
    private List<Observer<FriendshipRequestChangeEvent>> observers = new ArrayList<>();

    /**
     * FriendshipRequestService Constructor
     * @param friendshipRepo - reference to the Friendships' Repository
     * @param userRepo - reference to the Users' Repository
     */
    public FriendshipRequestService(FriendshipPagingRepository friendshipRepo, UserPagingRepository userRepo, FriendshipRequestPagingRepository friendshipRequestRepo, FriendshipService friendshipService) {
        this.userRepo = userRepo;
        this.friendshipRepo = friendshipRepo;
        this.friendshipRequestRepo = friendshipRequestRepo;
        this.friendshipService = friendshipService;
    }

    /**
     *
     * @param firstID - Long
     * @param secondID - Long
     * Adds into application a friendship request
     */
    public void addFriendshipRequest(Long firstID, Long secondID) {

        if(firstID.equals(secondID)) {
            throw new ValidationException("You cannot be friend with yourself!");
        }

        FriendshipRequest friendshipRequest = new FriendshipRequest();
        friendshipRequest.setId(new Tuple<>(firstID, secondID));
        friendshipRequest.setFromID(firstID);
        friendshipRequest.setToID(secondID);
        friendshipRequest.setStatus("pending");
        friendshipRequest.setDate(LocalDateTime.now());

        FriendshipRequest possibleFriendshipRequest = new FriendshipRequest();
        possibleFriendshipRequest.setId(new Tuple<>(secondID, firstID));
        possibleFriendshipRequest.setFromID(firstID);
        possibleFriendshipRequest.setToID(secondID);
        possibleFriendshipRequest.setStatus("pending");
        possibleFriendshipRequest.setDate(LocalDateTime.now());

        friendshipService.checkIDValidity(firstID, secondID);

        if((friendshipRequestRepo.findOne(friendshipRequest.getId()).isPresent() &&
                friendshipRequestRepo.findOne(friendshipRequest.getId()).get().getStatus().equals("pending")) ||
                (friendshipRequestRepo.findOne(possibleFriendshipRequest.getId()).isPresent() &&
                        friendshipRequestRepo.findOne(possibleFriendshipRequest.getId()).get().getStatus().equals("pending"))) {
            throw new ValidationException("This friendship request has pending status!\n");
        }

        if((friendshipRequestRepo.findOne(friendshipRequest.getId()).isPresent() &&
                friendshipRequestRepo.findOne(friendshipRequest.getId()).get().getStatus().equals("approved")) ||
                (friendshipRequestRepo.findOne(possibleFriendshipRequest.getId()).isPresent() &&
                        friendshipRequestRepo.findOne(possibleFriendshipRequest.getId()).get().getStatus().equals("approved"))) {
            throw new ValidationException("You are already friends\n");
        }

        if(friendshipRequestRepo.findOne(friendshipRequest.getId()).isPresent() &&
                friendshipRequestRepo.findOne(friendshipRequest.getId()).get().getStatus().equals("rejected")) {
            friendshipRequestRepo.delete(friendshipRequest.getId());
        }

        if(friendshipRequestRepo.findOne(possibleFriendshipRequest.getId()).isPresent() &&
                friendshipRequestRepo.findOne(possibleFriendshipRequest.getId()).get().getStatus().equals("rejected")) {
            friendshipRequestRepo.delete(possibleFriendshipRequest.getId());
        }

        friendshipRequestRepo.save(friendshipRequest);

    }

    /**
     *
     * @param fromID - Long
     * @param toID - Long
     * Cancel a friendship request - if it's having pending status
     *                             - @throws ValidationException otherwise
     */
    public void deleteFriendshipRequest(Long fromID, Long toID) {

        if(friendshipRequestRepo.findOne(new Tuple<>(fromID, toID)).isEmpty()) {
            throw new ValidationException("You didn't send any friendship request to this user!");
        } else {
            if(friendshipRequestRepo.findOne(new Tuple<>(fromID, toID)).get().getStatus().equals("approved"))
                throw new ValidationException("This friendship request already has been approved!");
            else if(friendshipRequestRepo.findOne(new Tuple<>(fromID, toID)).get().getStatus().equals("rejected")) {
                throw new ValidationException("This friendship request already has been rejected!");
            }
        }

        FriendshipRequest deletedFriendshipRequest = friendshipRequestRepo.findOne(new Tuple<>(fromID, toID)).get();
        friendshipRequestRepo.delete(new Tuple<>(fromID, toID));
    }

    /**
     *
     * @param firstID - Long
     *                - the ID of the user who can accept/reject the friendship
     * @param secondID - Long
     *                 - the ID of the user who asked for friendship
     */
    public void acceptFriendship(Long firstID, Long secondID, Boolean accept) {

        if(friendshipRequestRepo.findOne(new Tuple<>(secondID, firstID)).isEmpty()) {
            throw new ValidationException("The friendship request doesn't exist!\n");
        } else {
            if(friendshipRequestRepo.findOne(new Tuple<>(secondID, firstID)).get().getStatus().equals("rejected"))
                throw new ValidationException("You have already rejected this friendship request!");
            else if(friendshipRequestRepo.findOne(new Tuple<>(secondID, firstID)).get().getStatus().equals("approved"))
                throw new ValidationException("You have already approved this friendship request!");
        }

        if(friendshipService.findFriendship(new Tuple<>(secondID, firstID)) != null) {
            throw new ValidationException("You are already friends!");
        }

        Friendship friendship = new Friendship();
        friendship.setId(new Tuple<>(secondID, firstID));
        friendship.setDate(LocalDateTime.now());

        FriendshipRequest friendshipRequest = new FriendshipRequest();
        friendshipRequest.setId(new Tuple<>(secondID, firstID));
        friendshipRequest.setFromID(secondID);
        friendshipRequest.setToID(firstID);

        if(accept) {

            //friendshipRepo.save(friendship);
            friendshipService.addFriendship(secondID, firstID);
            friendshipRequest.setStatus("approved");

        } else {

            friendshipRequest.setStatus("rejected");
        }

        friendshipRequestRepo.update(friendshipRequest);
    }

    /**
     *
     * @param friendshipRequest - FriendshipRequest
     * @param fromID - Long
     * @param toID - Long
     * @return a UserAndFriendshipRequestDTO consisting of two users and a FriendshipRequest
     *                - fromUser - represents a user who asked for friendship
     *                - toUser - represents a user who was asked for friendship
     *                - FriendshipRequest - the friendshipRequest between the fromUser and toUser
     */
    private UserAndFriendshipRequestDTO getUserAndFriendshipRequest(Long fromID, Long toID, FriendshipRequest friendshipRequest) {

        User fromUser = null, toUser = null;

        if(userRepo.findOne(fromID).isPresent())
            fromUser = userRepo.findOne(fromID).get();
        if(userRepo.findOne(toID).isPresent())
            toUser = userRepo.findOne(toID).get();

        return new UserAndFriendshipRequestDTO(fromUser, toUser, friendshipRequest);
    }

    /**
     *
     * @param userID - Long
     * @return {@code List<UserAndFriendshipRequestDTO>}
     *         - representing a list of objects containing all friendships of a user, by whom were sent and to whom were sent
     */
    public List<UserAndFriendshipRequestDTO> getUserFriendshipRequests(Long userID) {

        if(userRepo.findOne(userID).isEmpty()) {
            throw new ValidationException("ID doesn't exist!\n");
        }

        List<FriendshipRequest> friendshipRequests = getAllFriendshipRequestsList();

        return friendshipRequests
                .stream()
                .filter(x -> x.getId().getLeft().equals(userID) || x.getId().getRight().equals(userID))
                .map(x -> getUserAndFriendshipRequest(x.getFromID(), x.getToID(), x))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @return all friendship requests sent by the user having the ID userID
     */
    public List<UserAndFriendshipRequestDTO> getUserFriendshipRequestsSentBy(Long userID) {

        return getUserFriendshipRequests(userID)
                .stream()
                .filter(x -> x.getFriendshipRequest().getFromID().equals(userID))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @return all friendship requests received by the user having the ID userID
     */
    public List<UserAndFriendshipRequestDTO> getUserFriendshipRequestsReceivedBy(Long userID) {

        return getUserFriendshipRequests(userID)
                .stream()
                .filter(x -> x.getFriendshipRequest().getToID().equals(userID))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @param pageNumber - int
     * @return a {@code List<UserAndFriendshipRequestDTO>} containing all sent friendships by a given user, by whom was the friendship request sent and to whom,
     *                                                     from a given page
     */
    public List<UserAndFriendshipRequestDTO> getNextUserFriendshipRequestsSentBy(Long userID, int pageNumber) {
        return getNextSentFriendshipRequestsOnPage(userID, pageNumber)
                .stream()
                .map(x -> getUserAndFriendshipRequest(x.getFromID(), x.getToID(), x))
                .collect(Collectors.toList());
    }

    public List<FriendshipRequest> getNextSentFriendshipRequestsOnPage(Long userID, int pageNumber) {
        return getSentFriendshipRequestsOnPage(pageNumber, userID);
    }

    public List<FriendshipRequest> getSentFriendshipRequestsOnPage(int page, Long userID) {
        Pageable pageable = new PageableImplementation(page, this.size);
        PageV2<FriendshipRequest> friendshipRequestPageV2 = friendshipRequestRepo.findAllSentFriendshipRequestsByUserIdOrderedByDate(pageable, userID);
        return friendshipRequestPageV2.getContent().collect(Collectors.toList());
    }

    /**
     *
     * @param userID - Long
     * @param pageNumber - int
     * @return {@code List<UserAndFriendshipRequestDTO>} containing all received friendships by a given user, by whom was the friendship request sent and to whom,
     *                                                   from a given page
     */
    public List<UserAndFriendshipRequestDTO> getNextUserFriendshipRequestsReceivedBy(Long userID, int pageNumber) {
        return getNextReceivedFriendshipRequestsOnPage(userID, pageNumber)
                .stream()
                .map(x -> getUserAndFriendshipRequest(x.getFromID(), x.getToID(), x))
                .collect(Collectors.toList());
    }

    public List<FriendshipRequest> getNextReceivedFriendshipRequestsOnPage(Long userID, int pageNumber) {
        return getReceivedFriendshipRequestsOnPage(pageNumber, userID);
    }

    public List<FriendshipRequest> getReceivedFriendshipRequestsOnPage(int page, Long userID) {
        Pageable pageable = new PageableImplementation(page, this.size);
        PageV2<FriendshipRequest> friendshipRequestPageV2 = friendshipRequestRepo.findAllReceivedFriendshipRequestsByUserIdOrderedByDate(pageable, userID);
        return friendshipRequestPageV2.getContent().collect(Collectors.toList());
    }

    /**
     *
     * @return {@code List<FriendshipRequest>} containing all friendship requests stored into application
     */
    private List<FriendshipRequest> getAllFriendshipRequestsList() {

        return StreamSupport
                .stream(friendshipRequestRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    /**
     *
     * @param fromUserID - Long
     * @param toUserID - Long
     * @return {@code UserRelation} representing the relation between two users from application
     */
    public UsersRelation getRelationTwoUsers(Long fromUserID, Long toUserID) {

        if(friendshipRepo.findOne(new Tuple<>(fromUserID, toUserID)).isPresent()) {
            return UsersRelation.FRIENDS;
        } else if(friendshipRepo.findOne(new Tuple<>(toUserID, fromUserID)).isPresent()) {
            return UsersRelation.FRIENDS;
        }

        if(friendshipRequestRepo.findOne(new Tuple<>(fromUserID, toUserID)).isPresent()) {
            if(friendshipRequestRepo.findOne(new Tuple<>(fromUserID, toUserID)).get().getStatus().equals("pending"))
                return UsersRelation.PENDING_REQUEST_FROM_TO;
        } else if(friendshipRequestRepo.findOne(new Tuple<>(toUserID, fromUserID)).isPresent()) {
            if(friendshipRequestRepo.findOne(new Tuple<>(toUserID, fromUserID)).get().getStatus().equals("pending"))
                return UsersRelation.PENDING_REQUEST_TO_FROM;
        }

        return UsersRelation.NOT_FRIENDS;
    }

    public Integer getNumberOfSentFriendshipRequestsUser(Long userID) {
        return friendshipRequestRepo.getNumberOfSentFriendshipRequests(userID);
    }

    public Integer getNumberOfReceivedFriendshipRequestsUser(Long userID) {
        return friendshipRequestRepo.getNumberOfReceivedFriendshipRequests(userID);
    }

    /**
     *
     * @return all friendshipRequests from application
     */
    public Iterable<FriendshipRequest> getAll() {
        return friendshipRequestRepo.findAll();
    }

}
