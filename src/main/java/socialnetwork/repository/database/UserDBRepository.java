package socialnetwork.repository.database;

import javafx.scene.image.Image;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.stream.StreamSupport;


public class UserDBRepository implements UserPagingRepository {

    private final String url;
    private final String username;
    private final String password;
    private final Validator<User> validator;


    public UserDBRepository(String url, String username, String password, Validator<User> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }


    @Override
    public Optional<User> findOne(Long ID) {

        if(ID == null)
            throw new IllegalArgumentException("ID must be not null");

        String query = "SELECT * FROM users WHERE id = " + ID.toString();
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
        ) {

            if(resultSet.next()) {

                Long id = resultSet.getLong("id");
                String lastName = resultSet.getString("last_name");
                String firstName = resultSet.getString("first_name");
                String userName = resultSet.getString("username");
                String userPassword = resultSet.getString("password");
                boolean notifiable = resultSet.getBoolean("notifiable");
                InputStream inputStream = resultSet.getBinaryStream("photo");

                User user = new User(firstName, lastName);
                user.setUserName(userName);
                user.setUserPassword(userPassword);
                user.setId(id);
                user.setNotifiable(notifiable);
                if(inputStream != null) {
                    Image image = new Image(inputStream);
                    user.setImage(image);
                }

                return Optional.of(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }


    @Override
    public Iterable<User> findAll() {

        Set<User> users = new HashSet<>();

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users");
            ResultSet resultSet = statement.executeQuery()
        ) {

            while (resultSet.next()) {

                Long id = resultSet.getLong("id");
                String lastName = resultSet.getString("last_name");
                String firstName = resultSet.getString("first_name");
                String userPassword = resultSet.getString("password");
                String userName = resultSet.getString("username");
                boolean notifiable = resultSet.getBoolean("notifiable");
                InputStream inputStream = resultSet.getBinaryStream("photo");

                User user = new User(firstName, lastName);
                user.setUserPassword(userPassword);
                user.setUserName(userName);
                user.setId(id);
                user.setNotifiable(notifiable);
                if(inputStream != null) {
                    Image image = new Image(inputStream);
                    user.setImage(image);
                }

                users.add(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }


    @Override
    public Optional<User> save(User entity) {

        if(entity == null)
            throw new IllegalArgumentException("Entity must be not null");
        validator.validate(entity);

        String forIDQuery = "SELECT MAX(id) from users";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO users(id, last_name, first_name, username, password, notifiable, photo) "+
                    "VALUES (?,?,?,?,?,?,?)");
            PreparedStatement statementForID = connection.prepareStatement(forIDQuery);
        ) {

            ResultSet resultSet = statementForID.executeQuery();
            Long ID = 1L;
            if(resultSet.next())
                ID = resultSet.getLong(1) + 1;
            statement.setLong(1, ID);
            statement.setString(2, entity.getLastName());
            statement.setString(3, entity.getFirstName());
            statement.setString(4, entity.getUserName());
            statement.setString(5, entity.getUserPassword());
            statement.setBoolean(6, entity.isNotifiable());
            statement.setNull(7, Types.BINARY);

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }


    @Override
    public Optional<User> delete(Long id) {

        if(id == null) {
            throw new IllegalArgumentException("ID must be not null");
        }

        Optional<User> optionalUser = findOne(id);

        if(optionalUser.isEmpty()) {
            return optionalUser;
        }

        String query = "DELETE FROM users WHERE id = " + id.toString();
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
        ) {

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return optionalUser;
    }


    @Override
    public Optional<User> update(User entity) {

        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null");
        validator.validate(entity);

        if(findOne(entity.getId()).isEmpty()) {
            return Optional.empty();
        }

        String query = "UPDATE users SET last_name = ?, first_name = ?, notifiable = ?, photo = ? WHERE id = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
        ) {

            statement.setString(1, entity.getLastName());
            statement.setString(2, entity.getFirstName());
            statement.setBoolean(3, entity.isNotifiable());
            if(entity.getImage() != null) {
                try (FileInputStream fileInputStream = new FileInputStream(entity.getImage().getUrl().substring(entity.getImage().getUrl().lastIndexOf("\\")))){
                    File file = new File("manprofile1.jpeg");
                    statement.setBinaryStream(4, fileInputStream, (int) file.length());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                statement.setNull(4, Types.BINARY);
            }
            statement.setLong(5, entity.getId());

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.of(entity);
    }

    /*@Override
    public Optional<User> update(User entity) {

        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null");
        validator.validate(entity);

        if(findOne(entity.getId()).isEmpty()) {
            return Optional.empty();
        }

        String query = "UPDATE users SET password = ? WHERE id = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
        ) {

            statement.setString(1, entity.getUserPassword());
            statement.setLong(2, entity.getId());
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.of(entity);
    }*/

    @Override
    public PageV2<User> findAll(Pageable pageable) {

        List<User> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String query = "SELECT * FROM users limit ? offset ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, offset);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {

                String lastName = resultSet.getString("last_name");
                String firstName = resultSet.getString("first_name");
                Long id = resultSet.getLong("id");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                boolean isNotifiable = resultSet.getBoolean("notifiable");
                InputStream inputStream = resultSet.getBinaryStream("photo");

                User user = new User(firstName, lastName);
                user.setId(id);
                user.setUserName(username);
                user.setUserPassword(password);
                user.setNotifiable(isNotifiable);
                if(inputStream != null) {
                    Image image = new Image(inputStream);
                    user.setImage(image);
                }

                result.add(user);

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCount());
    }

    private int getTotalCount() {

        int total = 0;
        String query = "SELECT COUNT(*) FROM users";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            total = resultSet.getInt(1);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return total;
    }

    @Override
    public PageV2<User> findAllUsersByName(Pageable pageable, String firstString, String secondString) {
        List<User> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();// '"%DT%"' "
        String query = "SELECT * FROM users where first_name like '%" + firstString + "%'" +
                " and last_name like '%" + secondString + "%'" +
                " limit ? offset ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, offset);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {

                String lastName = resultSet.getString("last_name");
                String firstName = resultSet.getString("first_name");
                Long id = resultSet.getLong("id");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                boolean isNotifiable = resultSet.getBoolean("notifiable");
                InputStream inputStream = resultSet.getBinaryStream("photo");

                User user = new User(firstName, lastName);
                user.setId(id);
                user.setUserName(username);
                user.setUserPassword(password);
                user.setNotifiable(isNotifiable);
                if(inputStream != null) {
                    Image image = new Image(inputStream);
                    user.setImage(image);
                }

                result.add(user);

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        PageV2<User> page = new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCount(firstString, secondString));
        return page;
    }

    private int getTotalCount(String firstString, String secondString) {

        String query = "SELECT * FROM users where first_name like '%" + firstString + "%'" +
                " and last_name like '%" + secondString + "%'";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                return resultSet.getInt(1);
            }


        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    @Override
    public Integer getNumberOfUsers(String firstString, String secondString) {
        return getTotalCount();
    }

}
