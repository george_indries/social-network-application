package socialnetwork.repository.database;

import socialnetwork.domain.Message;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.MessagePagingRepository;
import socialnetwork.repository.paging.PageV2;
import socialnetwork.repository.paging.Pageable;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.StreamSupport;

public class MessageDBRepository implements MessagePagingRepository {

    private final String url;
    private final String username;
    private final String password;
    private final Validator<Message> validator;

    public MessageDBRepository(String url, String username, String password, Validator<Message> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }


    @Override
    public Optional<Message> findOne(Long ID) {

        if(ID == null)
            throw new IllegalArgumentException("ID must be not null");

        String firstQuery = "SELECT * FROM messages WHERE id = ?";
        String secondQuery = "SELECT * FROM tousers WHERE message_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement secondStatement = connection.prepareStatement(secondQuery)
        ) {

            firstStatement.setLong(1, ID);
            secondStatement.setLong(1, ID);

            ResultSet firstResultSet = firstStatement.executeQuery();
            if(firstResultSet.next()) {

                Long id = firstResultSet.getLong("id");
                Long fromID = firstResultSet.getLong("from_id");
                String text = firstResultSet.getString("message");
                LocalDateTime date = firstResultSet.getTimestamp("date").toLocalDateTime();
                Long replyID = firstResultSet.getLong("reply_id");

                Message message = new Message();
                message.setId(id);
                message.setFromID(fromID);
                message.setMessage(text);
                message.setDate(date);
                if(replyID != null) {
                    message.setReplyMessageID(replyID);
                }

                ResultSet secondResultSet = secondStatement.executeQuery();
                while (secondResultSet.next()) {

                    Long toID = secondResultSet.getLong("to_id");
                    message.addToListID(toID);

                }

                return Optional.of(message);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }


    @Override
    public Iterable<Message> findAll() {

        Set<Message> messages = new HashSet<>();

        String firstQuery = "SELECT * FROM messages";
        String secondQuery = "SELECT * FROM tousers WHERE message_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement secondStatement = connection.prepareStatement(secondQuery)
        ) {

            ResultSet firstResultSet = firstStatement.executeQuery();
            while(firstResultSet.next()) {

                Long id = firstResultSet.getLong("id");
                Long fromID = firstResultSet.getLong("from_id");
                LocalDateTime date = firstResultSet.getTimestamp("date").toLocalDateTime();
                String text = firstResultSet.getString("message");
                Long replyID = firstResultSet.getLong("reply_id");

                Message message = new Message();
                message.setId(id);
                message.setFromID(fromID);
                message.setDate(date);
                message.setMessage(text);
                if(replyID != null) {
                    message.setReplyMessageID(replyID);
                }

                secondStatement.setLong(1, id);
                ResultSet secondResultSet = secondStatement.executeQuery();
                while(secondResultSet.next()) {

                    Long toID = secondResultSet.getLong("to_id");
                    message.addToListID(toID);

                }

                messages.add(message);

            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return messages;
    }


    @Override
    public Optional<Message> save(Message entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must be not null");
        validator.validate(entity);

        String firstQuery = "INSERT INTO messages(id, from_id, message, date, reply_id)" +
                " VALUES (?,?,?,?,?)";
        String secondQuery = "INSERT INTO tousers(message_id, to_id)" +
                " VALUES (?,?)";
        String forIDQuery = "SELECT MAX(id) from messages";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement secondStatement = connection.prepareStatement(secondQuery);
            PreparedStatement forIDStatement = connection.prepareStatement(forIDQuery)
        ) {
            Long ID = 1L;
            ResultSet resultSet = forIDStatement.executeQuery();
            if(resultSet.next())
                ID = resultSet.getLong(1) + 1;
            firstStatement.setLong(1, ID);
            firstStatement.setLong(2, entity.getFromID());
            firstStatement.setString(3, entity.getMessage());
            firstStatement.setTimestamp(4, Timestamp.valueOf(entity.getDate()));
            if(entity.getReplyMessageID() != null)
                firstStatement.setLong(5, entity.getReplyMessageID());
            else
                firstStatement.setNull(5, Types.BIGINT);

            firstStatement.execute();

            List<Long> toIDs = entity.getToListID();
            Integer length = toIDs.size();
            Integer counter = 0;

            while(counter < length) {

                secondStatement.setLong(1, ID);
                secondStatement.setLong(2, toIDs.get(counter));
                secondStatement.execute();
                counter++;

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }


    @Override
    public Optional<Message> delete(Long ID) {

        if(ID == null) {
            throw new IllegalArgumentException("ID must be not null");
        }

        Optional<Message> optionalMessage = findOne(ID);

        if(optionalMessage.isEmpty()) {
            return optionalMessage;
        }

        String firstQuery = "DELETE FROM messages WHERE id = ?";
        String secondQuery = "DELETE FROM tousers WHERE message_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement secondStatement = connection.prepareStatement(secondQuery)
        ) {

            firstStatement.setLong(1, ID);
            secondStatement.setLong(2, ID);

            firstStatement.execute();
            secondStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }


    @Override
    public Optional<Message> update(Message entity) {
        return Optional.empty();
    }

    private int getTotalCountSent(Long id) {

        int total = 0;
        String query = "SELECT COUNT(*) FROM messages where from_id = " + id;

        try(Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement()
        ) {

            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            total = resultSet.getInt(1);

        } catch (SQLException e){
            e.printStackTrace();
        }

        return total;

    }

    private int getTotalCountReceived(Long id) {

        int total = 0;
        String query = "SELECT COUNT(*) FROM tousers WHERE to_id = " + id;

        try(Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement()
        ) {

            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            total = resultSet.getInt(1);

        } catch (SQLException e){
            e.printStackTrace();
        }

        return total;

    }

    @Override
    public PageV2<Message> findAllSentMessagesByUserIdOrderedByDate(Pageable pageable, Long id) {

        List<Message> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();

        String firstQuery = "SELECT * FROM messages where from_id = ? order by date desc limit ? offset ?";
        String secondQuery = "SELECT * FROM tousers where message_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement secondStatement = connection.prepareStatement(secondQuery)
        ) {

            firstStatement.setLong(1, id);
            firstStatement.setInt(2, pageable.getPageSize());
            firstStatement.setInt(3, offset);
            ResultSet firstResultSet = firstStatement.executeQuery();
            while(firstResultSet.next()) {

                Long fromID = firstResultSet.getLong("from_id");
                Long messageID = firstResultSet.getLong("id");
                String text = firstResultSet.getString("message");
                LocalDateTime date = firstResultSet.getTimestamp("date").toLocalDateTime();
                Long replyID = firstResultSet.getLong("reply_id");

                Message message = new Message();
                message.setId(messageID);
                message.setFromID(fromID);
                message.setDate(date);
                message.setMessage(text);
                if(replyID != null) {
                    message.setReplyMessageID(replyID);
                }

                secondStatement.setLong(1, messageID);
                ResultSet secondResultSet = secondStatement.executeQuery();
                while(secondResultSet.next()) {

                    Long toID = secondResultSet.getLong("to_id");
                    message.addToListID(toID);

                }

                result.add(message);

            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCountSent(id));
    }

    @Override
    public PageV2<Message> findAllReceivedMessagesByUserIdOrderedByDate(Pageable pageable, Long id) {

        List<Message> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();

        String firstQuery = "SELECT * FROM tousers where to_id = ? limit ? offset ?";
        String secondQuery = "SELECT * FROM messages where id = ? order by date desc";
        String thirdQuery = "SELECT * FROM tousers where message_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement secondStatement = connection.prepareStatement(secondQuery);
            PreparedStatement thirdStatement = connection.prepareStatement(thirdQuery)
        ) {

            firstStatement.setLong(1, id);
            firstStatement.setInt(2, pageable.getPageSize());
            firstStatement.setInt(3, offset);
            ResultSet firstResultSet = firstStatement.executeQuery();
            while(firstResultSet.next()) {

                Long messageID = firstResultSet.getLong("message_id");
                Message message = new Message();
                message.setId(messageID);

                thirdStatement.setLong(1, messageID);
                ResultSet thirdResultSet = thirdStatement.executeQuery();
                while(thirdResultSet.next()) {
                    Long toID = thirdResultSet.getLong("to_id");
                    message.addToListID(toID);
                }

                secondStatement.setLong(1, messageID);
                ResultSet secondResultSet = secondStatement.executeQuery();
                if(secondResultSet.next()) {

                    Long fromID = secondResultSet.getLong("from_id");
                    String text = secondResultSet.getString("message");
                    LocalDateTime date = secondResultSet.getTimestamp("date").toLocalDateTime();
                    Long replyID = secondResultSet.getLong("reply_id");

                    message.setFromID(fromID);
                    message.setDate(date);
                    message.setMessage(text);
                    if (replyID != null) {
                        message.setReplyMessageID(replyID);
                    }
                }

                result.add(message);

            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCountReceived(id));
    }

    @Override
    public Integer getNumberOfSentMessages(Long id) {
        return getTotalCountSent(id);
    }

    @Override
    public Integer getNumberOfReceivedMessages(Long id) {
        return getTotalCountReceived(id);
    }

    @Override
    public PageV2<Message> findAll(Pageable pageable) {
        throw new UnsupportedOperationException();
    }

}
