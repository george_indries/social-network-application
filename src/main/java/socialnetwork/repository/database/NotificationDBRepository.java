package socialnetwork.repository.database;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.NotificationPagingRepository;
import socialnetwork.repository.paging.PageV2;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

public class NotificationDBRepository implements NotificationPagingRepository {

    private final String url;
    private final String username;
    private final String password;
    private final Validator<Notification> validator;
    private final PagingRepository<Long, Event> eventRepo;

    public NotificationDBRepository(String url, String username, String password, Validator<Notification> validator, PagingRepository<Long, Event> eventRepo) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        this.eventRepo = eventRepo;
    }


    @Override
    public Optional<Notification> findOne(Long ID) {
        if(ID == null)
            throw new IllegalArgumentException("ID must be not null");

        String query = "SELECT * FROM notifications WHERE id = " + ID.toString();
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery()
        ) {

            if(resultSet.next()) {

                Long eventID = resultSet.getLong("event_id");
                if(eventRepo.findOne(eventID).isEmpty())
                    throw new ValidationException("The event doesn't exist!\n");
                Event event = eventRepo.findOne(eventID).get();
                Long attendeeID = resultSet.getLong("user_id");
                LocalDateTime lastDate = resultSet.getTimestamp("last_date").toLocalDateTime();
                String description = resultSet.getString("description");
                boolean toShow = resultSet.getBoolean("to_show");

                Notification notification = new Notification();
                notification.setId(ID);
                notification.setEvent(event);
                notification.setAttendeeID(attendeeID);
                notification.setLastDate(lastDate);
                notification.setDescription(description);
                notification.setToShow(toShow);

                return Optional.of(notification);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Iterable<Notification> findAll() {
        throw new UnsupportedOperationException();
    }


    @Override
    public Optional<Notification> save(Notification notification) {
        if(notification == null)
            throw new IllegalArgumentException("Entity must be not null");
        validator.validate(notification);

        String forIDQuery = "SELECT MAX(id) from notifications";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO notifications(id, event_id, user_id, last_date, description, to_show) "+
                    "VALUES (?,?,?,?,?,?)");
            PreparedStatement statementForID = connection.prepareStatement(forIDQuery)
        ) {

            ResultSet resultSet = statementForID.executeQuery();
            Long ID = 1L;
            if(resultSet.next())
                ID = resultSet.getLong(1) + 1;
            statement.setLong(1, ID);
            statement.setLong(2, notification.getEvent().getId());
            statement.setLong(3, notification.getAttendeeID());
            statement.setTimestamp(4, Timestamp.valueOf(notification.getLastDate()));
            statement.setString(5, notification.getDescription());
            statement.setBoolean(6, notification.isToShow());

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Optional<Notification> delete(Long aLong) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<Notification> update(Notification notification) {
        if(notification == null)
            throw new IllegalArgumentException("Entity must not be null");
        validator.validate(notification);

        if(findOne(notification.getId()).isEmpty()) {
            return Optional.empty();
        }

        String query = "UPDATE notifications SET last_date = ?, description = ?, to_show = ? WHERE id = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setTimestamp(1, Timestamp.valueOf(notification.getLastDate()));
            statement.setString(2, notification.getDescription());
            statement.setBoolean(3, notification.isToShow());
            statement.setLong(4, notification.getId());
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.of(notification);
    }

    @Override
    public PageV2<Notification> findAllUserNotifications(Pageable pageable, Long id) {
        List<Notification> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String query = "SELECT * FROM notifications where user_id = ? limit ? offset ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setLong(1, id);
            statement.setInt(2, pageable.getPageSize());
            statement.setInt(3, offset);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {

                Long notificationID = resultSet.getLong("id");
                Long eventID = resultSet.getLong("event_id");
                if(eventRepo.findOne(eventID).isEmpty())
                    throw new ValidationException("The event doesn't exists!\n");
                Event event = eventRepo.findOne(eventID).get();
                LocalDateTime lastDate = resultSet.getTimestamp("last_date").toLocalDateTime();
                String description = resultSet.getString("description");
                boolean toShow = resultSet.getBoolean("to_show");

                Notification notification = new Notification();
                notification.setId(notificationID);
                notification.setEvent(event);
                notification.setAttendeeID(id);
                notification.setLastDate(lastDate);
                notification.setDescription(description);
                notification.setToShow(toShow);

                result.add(notification);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        result.sort((x,y) -> y.getLastDate().compareTo(x.getLastDate()));
        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getNumberOfNotificationsUser(id));
    }

    @Override
    public Integer getNumberOfNotificationsUser(Long id) {

        String query = "SELECT COUNT(*) FROM notifications where user_id = " + id;

        try(Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement()
        ) {

            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            return resultSet.getInt(1);

        } catch (SQLException e){
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public Integer getTotalNumberOfNotifications() {
        String query = "SELECT COUNT(*) FROM notifications";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement()
        ) {

            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            return resultSet.getInt(1);

        } catch (SQLException e){
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public PageV2<Notification> findAll(Pageable pageable) {
        List<Notification> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String query = "SELECT * FROM notifications limit ? offset ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, offset);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {

                Long notificationID = resultSet.getLong("id");
                Long eventID = resultSet.getLong("event_id");
                if(eventRepo.findOne(eventID).isEmpty())
                    throw new ValidationException("The event doesn't exists!\n");
                Event event = eventRepo.findOne(eventID).get();
                Long userID = resultSet.getLong("user_id");
                LocalDateTime lastDate = resultSet.getTimestamp("last_date").toLocalDateTime();
                String description = resultSet.getString("description");
                boolean toShow = resultSet.getBoolean("to_show");

                Notification notification = new Notification();
                notification.setId(notificationID);
                notification.setEvent(event);
                notification.setAttendeeID(userID);
                notification.setLastDate(lastDate);
                notification.setDescription(description);
                notification.setToShow(toShow);

                result.add(notification);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalNumberOfNotifications());
    }

}
