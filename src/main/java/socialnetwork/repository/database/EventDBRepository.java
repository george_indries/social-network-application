package socialnetwork.repository.database;

import socialnetwork.domain.Attendee;
import socialnetwork.domain.Event;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.EventPagingRepository;
import socialnetwork.repository.paging.PageV2;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

public class EventDBRepository implements EventPagingRepository {

    private final String url;
    private final String username;
    private final String password;
    private final Validator<Event> validator;
    private final PagingRepository<Long, User> userRepo;

    public EventDBRepository(String url, String username, String password, Validator<Event> validator, PagingRepository<Long, User> userRepo) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        this.userRepo = userRepo;
    }


    @Override
    public Optional<Event> findOne(Long ID) {

        if(ID == null)
            throw new IllegalArgumentException("ID must be not null");

        String firstQuery = "SELECT * FROM events WHERE id = ?";
        String secondQuery = "SELECT * FROM attendees WHERE event_id = ?";
        String thirdQuery = "SELECT * FROM users WHERE id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement secondStatement = connection.prepareStatement(secondQuery);
            PreparedStatement thirdStatement = connection.prepareStatement(thirdQuery)
        ) {

            firstStatement.setLong(1, ID);
            secondStatement.setLong(1, ID);

            ResultSet firstResultSet = firstStatement.executeQuery();
            if(firstResultSet.next()) {

                Long id = firstResultSet.getLong("id");
                String name = firstResultSet.getString("name");
                Long hostID = firstResultSet.getLong("host_id");
                LocalDateTime date = firstResultSet.getTimestamp("date").toLocalDateTime();
                String description = firstResultSet.getString("description");
                String pathToImage = firstResultSet.getString("path_to_image");
                LocalDateTime endDate = firstResultSet.getTimestamp("end_date").toLocalDateTime();
                String location = firstResultSet.getString("location");
                String category = firstResultSet.getString("category");

                Event event = new Event();
                event.setId(id);
                event.setName(name);

                thirdStatement.setLong(1, hostID);
                ResultSet resultSet = thirdStatement.executeQuery();
                resultSet.next();
                String lastName = resultSet.getString("last_name");
                String firstName = resultSet.getString("first_name");
                String username = resultSet.getString("username");
                boolean isNotifiable = resultSet.getBoolean("notifiable");
                User host = new User(firstName, lastName);
                host.setId(hostID);
                host.setUserName(username);
                host.setNotifiable(isNotifiable);

                event.setHost(host);
                event.setDate(date);
                event.setDescription(description);
                if(pathToImage != null)
                    event.setPathToImage(pathToImage);
                event.setEndDate(endDate);
                event.setLocation(location);
                event.setCategory(category);

                ResultSet secondResultSet = secondStatement.executeQuery();
                while (secondResultSet.next()) {

                    Long userID = secondResultSet.getLong("user_id");
                    boolean notifiable = secondResultSet.getBoolean("notifiable");

                    thirdStatement.setLong(1, userID);
                    ResultSet thirdResultSet = thirdStatement.executeQuery();
                    thirdResultSet.next();
                    String lastNameAttendee = thirdResultSet.getString("last_name");
                    String firstNameAttendee = thirdResultSet.getString("first_name");
                    String usernameAttendee = thirdResultSet.getString("username");
                    boolean isN = thirdResultSet.getBoolean("notifiable");
                    User attendeeUser = new User(firstNameAttendee, lastNameAttendee);
                    attendeeUser.setId(userID);
                    attendeeUser.setUserName(usernameAttendee);
                    attendeeUser.setNotifiable(isN);

                    Attendee attendee = new Attendee(attendeeUser, notifiable);
                    attendee.setId(userID);
                    attendee.setEventID(ID);
                    event.addToListAttendees(attendee);
                }

                return Optional.of(event);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();

    }

    @Override
    public Optional<Event> save(Event event) {
        if(event == null)
            throw new IllegalArgumentException("Entity must be not null");
        validator.validate(event);

        String firstQuery = "INSERT INTO events(id, name, host_id, date, description, path_to_image, end_date, location, category)" +
                " VALUES (?,?,?,?,?,?,?,?,?)";
        String forIDQuery = "SELECT MAX(id) from events";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement forIDStatement = connection.prepareStatement(forIDQuery)
        ) {
            Long ID = 1L;
            ResultSet resultSet = forIDStatement.executeQuery();
            if(resultSet.next())
                ID = resultSet.getLong(1) + 1;
            firstStatement.setLong(1, ID);
            firstStatement.setString(2, event.getName());
            firstStatement.setLong(3, event.getHost().getId());
            firstStatement.setTimestamp(4, Timestamp.valueOf(event.getDate()));
            firstStatement.setString(5, event.getDescription());
            if(!event.getPathToImage().equals(""))
                firstStatement.setString(6, event.getPathToImage());
            else
                firstStatement.setNull(6, Types.VARCHAR);
            firstStatement.setTimestamp(7, Timestamp.valueOf(event.getEndDate()));
            firstStatement.setString(8, event.getLocation());
            firstStatement.setString(9, event.getCategory());

            firstStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    @Override
    public Optional<Event> delete(Long ID) {

        if(ID == null) {
            throw new IllegalArgumentException("ID must be not null");
        }

        Optional<Event> optionalEvent = findOne(ID);

        if(optionalEvent.isEmpty()) {
            return optionalEvent;
        }

        String firstQuery = "DELETE FROM attendees WHERE event_id = ?";
        String secondQuery = "DELETE FROM events WHERE id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement secondStatement = connection.prepareStatement(secondQuery)
        ) {

            firstStatement.setLong(1, ID);
            secondStatement.setLong(2, ID);

            firstStatement.execute();
            secondStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return optionalEvent;
    }

    @Override
    public Optional<Attendee> save(Attendee attendee) {

        if(attendee == null)
            throw new IllegalArgumentException("Entity must be not null");

        String firstQuery = "INSERT INTO attendees(event_id, user_id, notifiable)" +
                " VALUES (?,?,?)";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery)
        ) {

            firstStatement.setLong(1, attendee.getEventID());
            firstStatement.setLong(2, attendee.getId());
            firstStatement.setBoolean(3, attendee.isNotifiable());
            firstStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    public Optional<Attendee> findOneAttendee(Long ID) {

        if(ID == null)
            throw new IllegalArgumentException("ID must be not null");

        String firstQuery = "SELECT * FROM attendees WHERE user_id = ?";
        String secondQuery = "SELECT * FROM users WHERE id = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
            PreparedStatement secondStatement = connection.prepareStatement(secondQuery)
        ) {

            firstStatement.setLong(1, ID);
            ResultSet resultSet = firstStatement.executeQuery();

            if(resultSet.next()) {

                Long eventID = resultSet.getLong("event_id");
                boolean notifiable = resultSet.getBoolean("notifiable");

                secondStatement.setLong(1, ID);
                ResultSet result = secondStatement.executeQuery();
                if(result.next()) {

                    String lastName = result.getString("last_name");
                    String firstName = result.getString("first_name");
                    String userName = result.getString("username");
                    boolean isNotifiable = result.getBoolean("notifiable");

                    User user = new User(firstName, lastName);
                    user.setUserName(userName);
                    user.setId(ID);
                    user.setNotifiable(isNotifiable);

                    Attendee attendee = new Attendee(user, notifiable);
                    attendee.setId(ID);
                    attendee.setEventID(eventID);
                    return Optional.of(attendee);
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();

    }

    @Override
    public Optional<Attendee> deleteAttendee(Long id) {

        if(id == null) {
            throw new IllegalArgumentException("ID must be not null");
        }

        Optional<Attendee> optionalAttendee = findOneAttendee(id);

        if(optionalAttendee.isEmpty()) {
            return optionalAttendee;
        }

        String query = "DELETE FROM attendees WHERE user_id = " + id;
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return optionalAttendee;
    }

    @Override
    public PageV2<Event> findAllEventsUser(Pageable pageable, Long id) {
        List<Event> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String firstQuery = "SELECT * FROM events WHERE host_id = ? limit ? offset ?";
        String secondQuery = "SELECT * FROM users WHERE id = ?";
        String thirdQuery = "SELECT * FROM attendees WHERE event_id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
             PreparedStatement secondStatement = connection.prepareStatement(secondQuery);
             PreparedStatement thirdStatement = connection.prepareStatement(thirdQuery)
        ) {

            firstStatement.setLong(1, id);
            firstStatement.setInt(2, pageable.getPageSize());
            firstStatement.setInt(3, offset);
            ResultSet resultSet1 = firstStatement.executeQuery();
            while(resultSet1.next()) {
                Long eventID = resultSet1.getLong("id");
                String name = resultSet1.getString("name");
                LocalDateTime date = resultSet1.getTimestamp("date").toLocalDateTime();
                String description = resultSet1.getString("description");
                String pathToImage = resultSet1.getString("path_to_image");
                LocalDateTime endDate = resultSet1.getTimestamp("end_date").toLocalDateTime();
                String location = resultSet1.getString("location");
                String category = resultSet1.getString("category");

                Event event = new Event();
                event.setId(eventID);
                event.setName(name);
                event.setDate(date);
                event.setDescription(description);
                if(pathToImage != null) {
                    event.setPathToImage(pathToImage);
                }
                event.setEndDate(endDate);
                event.setLocation(location);
                event.setCategory(category);

                secondStatement.setLong(1, id);
                ResultSet resultSet2 = secondStatement.executeQuery();
                if(resultSet2.next()) {
                    String firstName = resultSet2.getString("first_name");
                    String lastName = resultSet2.getString("last_name");
                    String username = resultSet2.getString("username");
                    boolean isNotifiable = resultSet2.getBoolean("notifiable");
                    User host = new User(firstName, lastName);
                    host.setUserName(username);
                    host.setId(id);
                    host.setNotifiable(isNotifiable);
                    event.setHost(host);

                    thirdStatement.setLong(1, eventID);
                    ResultSet resultSet3 = thirdStatement.executeQuery();
                    while(resultSet3.next()) {
                        Long userID = resultSet3.getLong("user_id");
                        boolean notifiable = resultSet3.getBoolean("notifiable");

                        secondStatement.setLong(1, userID);
                        ResultSet resultSet4 = secondStatement.executeQuery();
                        if(resultSet4.next()) {
                            String firstNameUser = resultSet4.getString("first_name");
                            String lastNameUser = resultSet4.getString("last_name");
                            String usernameUser = resultSet4.getString("username");
                            User user = new User(firstNameUser, lastNameUser);
                            user.setUserName(usernameUser);
                            user.setId(userID);
                            Attendee attendee = new Attendee(user, notifiable);
                            event.addToListAttendees(attendee);
                        }
                    }
                }

                result.add(event);

            }


        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getNumberOfAllEventsUser(id));
    }

    @Override
    public PageV2<Event> findAllEventsAttendee(Pageable pageable, Long id) {
        List<Event> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String firstQuery = "SELECT * FROM attendees WHERE user_id = ? limit ? offset ?";
        String secondQuery = "SELECT * FROM users WHERE id = ?";
        String thirdQuery = "SELECT * FROM events WHERE id = ?";
        String fourthQuery = "SELECT * FROM attendees WHERE event_id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
             PreparedStatement secondStatement = connection.prepareStatement(secondQuery);
             PreparedStatement thirdStatement = connection.prepareStatement(thirdQuery);
             PreparedStatement fourthStatement = connection.prepareStatement(fourthQuery)
        ) {

            firstStatement.setLong(1, id);
            firstStatement.setInt(2, pageable.getPageSize());
            firstStatement.setInt(3, offset);
            ResultSet resultSet1 = firstStatement.executeQuery();
            while(resultSet1.next()) {

                Long eventID = resultSet1.getLong("event_id");
                thirdStatement.setLong(1, eventID);
                ResultSet resultSet3 = thirdStatement.executeQuery();
                if(resultSet3.next()) {

                    String name = resultSet3.getString("name");
                    Long hostID = resultSet3.getLong("host_id");
                    LocalDateTime date = resultSet3.getTimestamp("date").toLocalDateTime();
                    String description = resultSet3.getString("description");
                    String pathToImage = resultSet3.getString("path_to_image");
                    LocalDateTime endDate = resultSet3.getTimestamp("end_date").toLocalDateTime();
                    String location = resultSet3.getString("location");
                    String category = resultSet3.getString("category");

                    Event event = new Event();
                    event.setId(eventID);
                    event.setName(name);
                    event.setDate(date);
                    event.setDescription(description);
                    if(pathToImage != null) {
                        event.setPathToImage(pathToImage);
                    }
                    event.setEndDate(endDate);
                    event.setLocation(location);
                    event.setCategory(category);

                    secondStatement.setLong(1, hostID);
                    ResultSet resultSet2 = secondStatement.executeQuery();
                    if(resultSet2.next()) {
                        String firstName = resultSet2.getString("first_name");
                        String lastName = resultSet2.getString("last_name");
                        String username = resultSet2.getString("username");
                        boolean isN = resultSet2.getBoolean("notifiable");
                        User host = new User(firstName, lastName);
                        host.setUserName(username);
                        host.setId(hostID);
                        host.setNotifiable(isN);
                        event.setHost(host);

                        fourthStatement.setLong(1, eventID);
                        ResultSet resultSet4 = fourthStatement.executeQuery();
                        while(resultSet4.next()) {
                            Long userID = resultSet4.getLong("user_id");
                            boolean notifiable = resultSet4.getBoolean("notifiable");

                            secondStatement.setLong(1, userID);
                            ResultSet resultSet5 = secondStatement.executeQuery();
                            if(resultSet5.next()) {
                                String firstNameUser = resultSet5.getString("first_name");
                                String lastNameUser = resultSet5.getString("last_name");
                                String usernameUser = resultSet5.getString("username");
                                boolean isNotifiable = resultSet5.getBoolean("notifiable");
                                User user = new User(firstNameUser, lastNameUser);
                                user.setUserName(usernameUser);
                                user.setId(userID);
                                user.setNotifiable(isNotifiable);
                                Attendee attendee = new Attendee(user, notifiable);
                                attendee.setId(userID);
                                attendee.setEventID(eventID);
                                event.addToListAttendees(attendee);
                            }
                        }
                    }

                    result.add(event);
                }

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getNumberOfAllEventsAttendee(id));
    }

    @Override
    public Integer getNumberOfAllEventsUser(Long id) {

        String query = "SELECT COUNT(*) FROM events where host_id = " + id;
        try(Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            return resultSet.getInt(1);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    @Override
    public Integer getNumberOfAllEventsAttendee(Long id) {

        String query = "SELECT COUNT(*) FROM attendees where user_id = " + id;
        try(Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            return resultSet.getInt(1);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    @Override
    public Integer getTotalNumberOfEvents() {

        String query = "SELECT COUNT(*) FROM events";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            return resultSet.getInt(1);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    @Override
    public Optional<Event> update(Event event) {
        throw new UnsupportedOperationException();
    }

    @Override
    public PageV2<Event> findAll(Pageable pageable) {
        List<Event> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String firstQuery = "SELECT * FROM events limit ? offset ?";
        String secondQuery = "SELECT * FROM users WHERE id = ?";
        String thirdQuery = "SELECT * FROM attendees WHERE event_id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement firstStatement = connection.prepareStatement(firstQuery);
             PreparedStatement secondStatement = connection.prepareStatement(secondQuery);
             PreparedStatement thirdStatement = connection.prepareStatement(thirdQuery)
        ) {

            firstStatement.setInt(1, pageable.getPageSize());
            firstStatement.setInt(2, offset);
            ResultSet resultSet1 = firstStatement.executeQuery();
            while(resultSet1.next()) {
                Long eventID = resultSet1.getLong("id");
                String name = resultSet1.getString("name");
                Long hostID = resultSet1.getLong("host_id");
                LocalDateTime date = resultSet1.getTimestamp("date").toLocalDateTime();
                String description = resultSet1.getString("description");
                String pathToImage = resultSet1.getString("path_to_image");
                LocalDateTime endDate = resultSet1.getTimestamp("end_date").toLocalDateTime();
                String location = resultSet1.getString("location");
                String category = resultSet1.getString("category");

                Event event = new Event();
                event.setId(eventID);
                event.setName(name);
                event.setDate(date);
                event.setDescription(description);
                if(pathToImage != null) {
                    event.setPathToImage(pathToImage);
                }
                event.setEndDate(endDate);
                event.setLocation(location);
                event.setCategory(category);

                secondStatement.setLong(1, hostID);
                ResultSet resultSet2 = secondStatement.executeQuery();
                if(resultSet2.next()) {
                    String firstName = resultSet2.getString("first_name");
                    String lastName = resultSet2.getString("last_name");
                    String username = resultSet2.getString("username");
                    boolean isNotifiable = resultSet2.getBoolean("notifiable");
                    User host = new User(firstName, lastName);
                    host.setUserName(username);
                    host.setId(hostID);
                    host.setNotifiable(isNotifiable);
                    event.setHost(host);

                    thirdStatement.setLong(1, eventID);
                    ResultSet resultSet3 = thirdStatement.executeQuery();
                    while(resultSet3.next()) {
                        Long userID = resultSet3.getLong("user_id");
                        boolean notifiable = resultSet3.getBoolean("notifiable");

                        secondStatement.setLong(1, userID);
                        ResultSet resultSet4 = secondStatement.executeQuery();
                        if(resultSet4.next()) {
                            String firstNameUser = resultSet4.getString("first_name");
                            String lastNameUser = resultSet4.getString("last_name");
                            String usernameUser = resultSet4.getString("username");
                            User user = new User(firstNameUser, lastNameUser);
                            user.setUserName(usernameUser);
                            user.setId(userID);
                            Attendee attendee = new Attendee(user, notifiable);
                            event.addToListAttendees(attendee);
                        }
                    }
                }

                result.add(event);

            }


        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalNumberOfEvents());
    }

    @Override
    public Iterable<Event> findAll() {
        throw new UnsupportedOperationException();
    }

}
