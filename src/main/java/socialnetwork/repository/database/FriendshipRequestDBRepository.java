package socialnetwork.repository.database;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.*;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.StreamSupport;


public class FriendshipRequestDBRepository implements FriendshipRequestPagingRepository {

    private final String url;
    private final String username;
    private final String password;
    private final Validator<FriendshipRequest> validator;

    public FriendshipRequestDBRepository(String url, String username, String password, Validator<FriendshipRequest> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }


    @Override
    public Optional<FriendshipRequest> findOne(Tuple<Long, Long> ID) {

        if(ID == null)
            throw new IllegalArgumentException("ID must be not null");

        String query = "SELECT * FROM friendshiprequests WHERE from_id = ? AND to_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setLong(1, ID.getLeft());
            statement.setLong(2, ID.getRight());

            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()) {

                Long leftID = resultSet.getLong("from_id");
                Long rightID = resultSet.getLong("to_id");
                String status = resultSet.getString("status");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

                FriendshipRequest friendshipRequest = new FriendshipRequest();
                friendshipRequest.setId(new Tuple<>(leftID, rightID));
                friendshipRequest.setFromID(leftID);
                friendshipRequest.setToID(rightID);
                friendshipRequest.setStatus(status);
                friendshipRequest.setDate(date);

                return Optional.of(friendshipRequest);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();

    }


    @Override
    public Iterable<FriendshipRequest> findAll() {
        Set<FriendshipRequest> friendshipRequests = new HashSet<>();

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM friendshiprequests");
            ResultSet resultSet = statement.executeQuery()
        ) {

            while (resultSet.next()) {

                Long rightID = resultSet.getLong("to_id");
                Long leftID = resultSet.getLong("from_id");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                String status = resultSet.getString("status");

                FriendshipRequest friendshipRequest = new FriendshipRequest();
                friendshipRequest.setId(new Tuple<>(leftID, rightID));
                friendshipRequest.setStatus(status);
                friendshipRequest.setDate(date);
                friendshipRequest.setFromID(leftID);
                friendshipRequest.setToID(rightID);

                friendshipRequests.add(friendshipRequest);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return friendshipRequests;
    }


    @Override
    public Optional<FriendshipRequest> save(FriendshipRequest entity) {

        if(entity == null)
            throw new IllegalArgumentException("Entity must be not null");
        validator.validate(entity);

        if(findOne(entity.getId()).isPresent()) {
            return Optional.of(entity);
        } else {

            try(Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement statement = connection.prepareStatement("INSERT INTO friendshiprequests(from_id, to_id, status, date) "+
                        "VALUES (?,?,?,?)")
            ) {

                statement.setLong(1, entity.getId().getLeft());
                statement.setLong(2, entity.getId().getRight());
                statement.setString(3, entity.getStatus());
                statement.setTimestamp(4, Timestamp.valueOf(entity.getDate()));

                statement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return Optional.empty();

    }


    @Override
    public Optional<FriendshipRequest> delete(Tuple<Long, Long> id) {

        if(id == null) {
            throw new IllegalArgumentException("ID must be not null");
        }

        Optional<FriendshipRequest> optionalFriendshipRequest = findOne(id);

        if(optionalFriendshipRequest.isEmpty()) {
            return optionalFriendshipRequest;
        }

        String query = "DELETE FROM friendshiprequests WHERE from_id = ? AND to_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setLong(2, id.getRight());
            statement.setLong(1, id.getLeft());

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return optionalFriendshipRequest;
    }


    @Override
    public Optional<FriendshipRequest> update(FriendshipRequest entity) {

        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null");
        validator.validate(entity);

        if(findOne(entity.getId()).isEmpty()) {
            return Optional.empty();
        }

        String query = "UPDATE friendshiprequests SET status = ? WHERE from_id = ? AND to_id = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setString(1, entity.getStatus());
            statement.setLong(2, entity.getId().getLeft());
            statement.setLong(3, entity.getId().getRight());

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.of(entity);
    }

    @Override
    public PageV2<FriendshipRequest> findAll(Pageable pageable) {

        List<FriendshipRequest> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String query = "SELECT * FROM friendshiprequests limit ? offset ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, offset);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                Long fromID = resultSet.getLong("from_id");
                Long toID = resultSet.getLong("to_id");
                String status = resultSet.getString("status");

                FriendshipRequest friendshipRequest = new FriendshipRequest();
                friendshipRequest.setDate(date);
                friendshipRequest.setStatus(status);
                friendshipRequest.setId(new Tuple<>(fromID, toID));

                result.add(friendshipRequest);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCountFriendshipRequests());
    }

    private int getTotalCountFriendshipRequests() {

        int total = 0;
        String query = "SELECT COUNT(*) FROM friendshiprequests";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            total = resultSet.getInt(1);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return total;
    }

    private int getTotalCountSent(Long id) {

        int total = 0;
        String query = "SELECT COUNT(*) FROM friendshiprequests where from_id = " + id;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            total = resultSet.getInt(1);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return total;
    }

    private int getTotalCountReceived(Long id) {

        int total = 0;
        String query = "SELECT COUNT(*) FROM friendshiprequests where to_id = " + id;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            total = resultSet.getInt(1);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return total;
    }

    @Override
    public PageV2<FriendshipRequest> findAllSentFriendshipRequestsByUserIdOrderedByDate(Pageable pageable, Long id) {

        List<FriendshipRequest> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String query = "SELECT * FROM friendshiprequests where from_id = ? order by date desc limit ? offset ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setLong(1, id);
            statement.setInt(2, pageable.getPageSize());
            statement.setInt(3, offset);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                Long fromID = resultSet.getLong("from_id");
                Long toID = resultSet.getLong("to_id");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                String status = resultSet.getString("status");

                FriendshipRequest friendshipRequest = new FriendshipRequest();
                friendshipRequest.setDate(date);
                friendshipRequest.setId(new Tuple<>(fromID, toID));
                friendshipRequest.setStatus(status);
                friendshipRequest.setFromID(fromID);
                friendshipRequest.setToID(toID);

                result.add(friendshipRequest);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCountSent(id));
    }

    @Override
    public PageV2<FriendshipRequest> findAllReceivedFriendshipRequestsByUserIdOrderedByDate(Pageable pageable, Long id) {

        List<FriendshipRequest> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String query = "SELECT * FROM friendshiprequests where to_id = ? order by date desc limit ? offset ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setLong(1, id);
            statement.setInt(2, pageable.getPageSize());
            statement.setInt(3, offset);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                Long fromID = resultSet.getLong("from_id");
                Long toID = resultSet.getLong("to_id");
                String status = resultSet.getString("status");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

                FriendshipRequest friendshipRequest = new FriendshipRequest();
                friendshipRequest.setDate(date);
                friendshipRequest.setId(new Tuple<>(fromID, toID));
                friendshipRequest.setStatus(status);
                friendshipRequest.setFromID(fromID);
                friendshipRequest.setToID(toID);

                result.add(friendshipRequest);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCountReceived(id));
    }

    @Override
    public Integer getNumberOfSentFriendshipRequests(Long id) {
        return getTotalCountSent(id);
    }

    @Override
    public Integer getNumberOfReceivedFriendshipRequests(Long id) {
        return getTotalCountReceived(id);
    }

}
