package socialnetwork.repository.database;

import javafx.scene.image.Image;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.*;

import java.io.InputStream;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.StreamSupport;


public class FriendshipDBRepository implements FriendshipPagingRepository {

    private final String url;
    private final String username;
    private final String password;
    private final Validator<Friendship> validator;

    public FriendshipDBRepository(String url, String username, String password, Validator<Friendship> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }


    @Override
    public Optional<Friendship> findOne(Tuple<Long, Long> ID) {

        if(ID == null)
            throw new IllegalArgumentException("ID must be not null");

        String query = "SELECT * FROM friendships WHERE left_id = ? AND right_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setLong(1, ID.getLeft());
            statement.setLong(2, ID.getRight());

            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()) {

                Long leftID = resultSet.getLong("left_id");
                Long rightID = resultSet.getLong("right_id");
                LocalDateTime dateTime = resultSet.getTimestamp("date").toLocalDateTime();

                Friendship friendship = new Friendship();
                friendship.setId(new Tuple<>(leftID, rightID));
                friendship.setDate(dateTime);

                return Optional.of(friendship);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }


    @Override
    public Iterable<Friendship> findAll() {

        Set<Friendship> friendships = new HashSet<>();

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM friendships");
            ResultSet resultSet = statement.executeQuery()
        ) {

            while (resultSet.next()) {

                Long leftID = resultSet.getLong("left_id");
                Long rightID = resultSet.getLong("right_id");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

                Friendship friendship = new Friendship();
                friendship.setDate(date);
                friendship.setId(new Tuple<>(leftID, rightID));

                friendships.add(friendship);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return friendships;
    }


    @Override
    public Optional<Friendship> save(Friendship entity) {

        if(entity == null)
            throw new IllegalArgumentException("Entity must be not null");
        validator.validate(entity);

        if(findOne(entity.getId()).isPresent()) {
            return Optional.of(entity);
        } else {

            try(Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement statement = connection.prepareStatement("INSERT INTO friendships(left_id, right_id, date) "+
                        "VALUES (?,?,?)")
            ) {

                statement.setLong(1, entity.getId().getLeft());
                statement.setLong(2, entity.getId().getRight());
                statement.setTimestamp(3, Timestamp.valueOf(entity.getDate()));

                statement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return Optional.empty();
    }


    @Override
    public Optional<Friendship> delete(Tuple<Long, Long> id) {

        if(id == null) {
            throw new IllegalArgumentException("ID must be not null");
        }

        Optional<Friendship> optionalFriendship = findOne(id);

        if(optionalFriendship.isEmpty()) {
            return optionalFriendship;
        }

        String query = "DELETE FROM friendships WHERE left_id = ? AND right_id = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setLong(1, id.getLeft());
            statement.setLong(2, id.getRight());

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return optionalFriendship;
    }


    @Override
    public Optional<Friendship> update(Friendship entity) {

        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null");
        validator.validate(entity);

        return Optional.empty();
    }

    @Override
    public PageV2<Friendship> findAll(Pageable pageable) {

        List<Friendship> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String query = "SELECT * FROM friendships limit ? offset ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, offset);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                Long leftID = resultSet.getLong("left_id");
                Long rightID = resultSet.getLong("right_id");

                Friendship friendship = new Friendship();
                friendship.setDate(date);
                friendship.setId(new Tuple<>(leftID, rightID));

                result.add(friendship);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCountFriendships());
    }

    @Override
    public PageV2<Friendship> findAllFriendshipsByUserIdOrderedByDate(Pageable pageable, Long id) {

        List<Friendship> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String query = "SELECT * FROM friendships where left_id = ? or right_id = ? order by date desc limit ? offset ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query)
        ) {

            statement.setLong(1, id);
            statement.setLong(2, id);
            statement.setInt(3, pageable.getPageSize());
            statement.setInt(4, offset);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {

                Long rightID = resultSet.getLong("right_id");
                Long leftID = resultSet.getLong("left_id");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

                Friendship friendship = new Friendship();
                friendship.setDate(date);
                friendship.setId(new Tuple<>(leftID, rightID));

                result.add(friendship);

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCount(id));
    }

    @Override
    public PageV2<FriendAndDateDTO> findFriendsByName(Pageable pageable, Long userID, String firstString, String secondString) {

        List<FriendAndDateDTO> result = new ArrayList<>();
        int offset = pageable.getPageNumber() * pageable.getPageSize();
        String query = "SELECT * FROM users where first_name like '%" + firstString + "%'" +
                " and last_name like '%" + secondString + "%'" +
                " limit ? offset ?";
        String queryForFriendships = "SELECT * FROM friendships WHERE left_id = ? and right_id = ? or left_id = ? and right_id = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(query);
            PreparedStatement statementForFriendships = connection.prepareStatement(queryForFriendships)
        ) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, offset);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {

                String lastName = resultSet.getString("last_name");
                String firstName = resultSet.getString("first_name");
                Long id = resultSet.getLong("id");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                boolean isNotifiable = resultSet.getBoolean("notifiable");
                InputStream inputStream = resultSet.getBinaryStream("photo");

                User user = new User(firstName, lastName);
                user.setId(id);
                user.setUserName(username);
                user.setUserPassword(password);
                user.setNotifiable(isNotifiable);
                if(inputStream != null) {
                    Image image = new Image(inputStream);
                    user.setImage(image);
                }

                statementForFriendships.setLong(1, userID);
                statementForFriendships.setLong(2, id);
                statementForFriendships.setLong(3, id);
                statementForFriendships.setLong(4, userID);
                ResultSet resultSetFriendships = statementForFriendships.executeQuery();
                while(resultSetFriendships.next()) {
                    Long leftID = resultSetFriendships.getLong("left_id");
                    Long rightID = resultSetFriendships.getLong("right_id");
                    LocalDateTime date = resultSetFriendships.getTimestamp("date").toLocalDateTime();
                    Friendship friendship = new Friendship();
                    friendship.setId(new Tuple<>(leftID, rightID));
                    friendship.setDate(date);
                    FriendAndDateDTO friendAndDateDTO = new FriendAndDateDTO(user, friendship);
                    result.add(friendAndDateDTO);
                }

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return new PageV2<>(StreamSupport.stream(result.spliterator(), false), getTotalCount(userID));
    }

    private int getTotalCount(Long id) {

        int total = 0;
        String query = "SELECT COUNT(*) FROM friendships where left_id = " + id + " or right_id = " + id;
        try(Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next())
                total = resultSet.getInt(1);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return total;
    }

    private int getTotalCountFriendships() {

        int total = 0;
        String query = "SELECT COUNT(*) FROM friendships";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             Statement statement = connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            total = resultSet.getInt(1);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return total;
    }


    @Override
    public Integer getNumberOfFriends(Long id) {
        return getTotalCount(id);
    }

}
