package socialnetwork.repository.file;

import socialnetwork.domain.FriendshipRequest;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.util.List;

public class FriendshipRequestFileRepository extends AbstractFileRepository<Tuple<Long, Long>, FriendshipRequest> {


    public FriendshipRequestFileRepository(String fileName, Validator<FriendshipRequest> validator) {
        super(fileName, validator);
    }

    @Override
    public FriendshipRequest extractEntity(List<String> attributes) {

        FriendshipRequest friendshipRequest = new FriendshipRequest();
        friendshipRequest.setId(new Tuple<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1))));
        friendshipRequest.setStatus(attributes.get(2));

        return friendshipRequest;
    }

    @Override
    protected String createEntityAsString(FriendshipRequest entity) {

        return entity.getId().toString() + ";" + entity.getStatus();

    }


}
