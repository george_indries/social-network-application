package socialnetwork.repository.file;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class FriendshipFileRepository extends AbstractFileRepository<Tuple<Long, Long>, Friendship> {


    public FriendshipFileRepository(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
    }

    @Override
    public Friendship extractEntity(List<String> attributes) {

        Friendship friendship = new Friendship();
        friendship.setDate(LocalDateTime.parse(attributes.get(0), DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        friendship.setId(new Tuple(Long.parseLong(attributes.get(1)), Long.parseLong(attributes.get(2))));

        return friendship;
    }

    @Override
    protected String createEntityAsString(Friendship entity) {
        return entity.getDate().toString() + ";" + entity.getId().toString();
    }
}
