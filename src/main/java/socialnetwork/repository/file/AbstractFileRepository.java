package socialnetwork.repository.file;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.memory.InMemoryRepository;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E> {

    String fileName;

    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    private void loadData() {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while((line = br.readLine()) != null) {
                List<String> attr = Arrays.asList(line.split(";"));
                E e = extractEntity(attr);
                super.save(e);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     *  extract entity - template method design pattern
     *  creates an entity of type E having a specified list of @code attributes
     * @param attributes - list of attributes
     * @return an entity of type E
     */
    public abstract E extractEntity(List<String> attributes);

    /**
     *  transform entity into a String
     * @param entity - entity to be convert into String
     * @return a string of type String
     */
    protected abstract String createEntityAsString(E entity);

    protected void writeToFile(E entity) {
        try(BufferedWriter bW = new BufferedWriter(new FileWriter(fileName, true))) {
            bW.write(createEntityAsString(entity));
            bW.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void writeAllToFile(List<E> users) {
        try(BufferedWriter bW = new BufferedWriter(new FileWriter(fileName, false))) {
            for(E e: users) {
                bW.write(createEntityAsString(e));
                bW.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<E> save(E entity) {
        Optional<E> optionalEntity = super.save(entity);
        if(optionalEntity.isPresent()) {
            return optionalEntity;
        }
        writeToFile(entity);
        return Optional.empty();
    }

    @Override
    public Optional<E> delete(ID id) {
        Optional<E> optionalEntity = super.delete(id);
        if(optionalEntity.isPresent()) {
            writeAllToFile(StreamSupport.stream(super.findAll().spliterator(), false).collect(Collectors.toList()));
            return optionalEntity;
        }
        return Optional.empty();
    }

    @Override
    public Optional<E> update(E entity) {
        Optional<E> optionalEntity = super.update(entity);
        if(optionalEntity.isEmpty()) {
            return Optional.empty();
        }
        writeAllToFile(StreamSupport.stream(super.findAll().spliterator(), false).collect(Collectors.toList()));
        return optionalEntity;
    }

}
