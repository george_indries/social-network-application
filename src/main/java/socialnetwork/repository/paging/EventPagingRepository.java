package socialnetwork.repository.paging;

import socialnetwork.domain.Attendee;
import socialnetwork.domain.Event;

import java.util.Optional;

public interface EventPagingRepository extends PagingRepository<Long, Event> {

    Optional<Attendee> save(Attendee attendee);

    Optional<Attendee> deleteAttendee(Long id);

    PageV2<Event> findAllEventsUser(Pageable pageable, Long id);

    PageV2<Event> findAllEventsAttendee(Pageable pageable, Long id);

    Integer getNumberOfAllEventsUser(Long id);

    Integer getNumberOfAllEventsAttendee(Long id);

    Integer getTotalNumberOfEvents();

}
