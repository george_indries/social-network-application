package socialnetwork.repository.paging;

import socialnetwork.domain.FriendAndDateDTO;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;

public interface FriendshipPagingRepository extends PagingRepository<Tuple<Long, Long>, Friendship> {

    Integer getNumberOfFriends(Long id);

    PageV2<FriendAndDateDTO> findFriendsByName(Pageable pageable, Long userID, String firstString, String secondString);

    PageV2<Friendship> findAllFriendshipsByUserIdOrderedByDate(Pageable pageable, Long id);
}
