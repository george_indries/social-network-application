package socialnetwork.repository.paging;

import socialnetwork.domain.Message;

public interface MessagePagingRepository extends PagingRepository<Long, Message> {

    PageV2<Message> findAllSentMessagesByUserIdOrderedByDate(Pageable pageable, Long id);

    PageV2<Message> findAllReceivedMessagesByUserIdOrderedByDate(Pageable pageable, Long id);

    Integer getNumberOfSentMessages(Long id);

    Integer getNumberOfReceivedMessages(Long id);

}
