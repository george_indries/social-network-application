package socialnetwork.repository.paging;

import socialnetwork.domain.FriendshipRequest;
import socialnetwork.domain.Tuple;

public interface FriendshipRequestPagingRepository extends PagingRepository<Tuple<Long, Long>, FriendshipRequest> {

    Integer getNumberOfSentFriendshipRequests(Long id);

    Integer getNumberOfReceivedFriendshipRequests(Long id);

    PageV2<FriendshipRequest> findAllSentFriendshipRequestsByUserIdOrderedByDate(Pageable pageable, Long id);

    PageV2<FriendshipRequest> findAllReceivedFriendshipRequestsByUserIdOrderedByDate(Pageable pageable, Long id);

}
