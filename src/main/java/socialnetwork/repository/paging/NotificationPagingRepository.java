package socialnetwork.repository.paging;

import socialnetwork.domain.Notification;

public interface NotificationPagingRepository extends PagingRepository<Long, Notification> {

    PageV2<Notification> findAllUserNotifications(Pageable pageable, Long id);

    Integer getNumberOfNotificationsUser(Long id);

    Integer getTotalNumberOfNotifications();

}
