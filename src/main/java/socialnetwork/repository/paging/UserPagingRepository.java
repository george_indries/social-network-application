package socialnetwork.repository.paging;

import socialnetwork.domain.User;

public interface UserPagingRepository extends PagingRepository<Long, User> {

    PageV2<User> findAllUsersByName(Pageable pageable, String firstString, String secondString);

    Integer getNumberOfUsers(String firstString, String secondString);

}
