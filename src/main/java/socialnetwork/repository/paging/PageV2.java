package socialnetwork.repository.paging;

import java.util.stream.Stream;

public class PageV2<E> {

    private Stream<E> content;
    private int totalCount;

    public PageV2(Stream<E> content, int totalCount) {
        this.content = content;
        this.totalCount = totalCount;
    }

    public Stream<E> getContent() {
        return content;
    }

    public int getTotalCount() {
        return totalCount;
    }

}
