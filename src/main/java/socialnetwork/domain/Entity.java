package socialnetwork.domain;

import java.io.Serializable;

public class Entity<ID> implements Serializable {

    private static final long serialVersionUID = 7331115341259248461L;
    private ID id;


    /**
     *
     * @return the id of an Entity
     *             id - Long
     */
    public ID getId() {
        return id;
    }


    /**
     *
     * @param id - Long
     * Sets the id for an Entity
     */
    public void setId(ID id) {
        this.id = id;
    }

}
