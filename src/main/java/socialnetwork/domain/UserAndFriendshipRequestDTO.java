package socialnetwork.domain;

import socialnetwork.utils.Constants;

/**
 * Class UserAndFriendshipRequestDTO - represents a Data Transfer Object class
 *                                   - useful for storing data about the data of a User and all needed data about his Friendship
 */
public class UserAndFriendshipRequestDTO {

    private String fromName;
    private String toName;
    private String status;
    private String date;
    private User fromUser;
    private User toUser;
    private FriendshipRequest friendshipRequest;

    public UserAndFriendshipRequestDTO(User fromUser, User toUser, FriendshipRequest friendshipRequest) {
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.fromName = this.fromUser.getLastName() + " " + this.fromUser.getFirstName();
        this.toName = this.toUser.getLastName() + " " + this.toUser.getFirstName();
        this.friendshipRequest = friendshipRequest;
        this.status = this.friendshipRequest.getStatus();
        this.date = this.friendshipRequest.getDate().format(Constants.DATE_TIME_FORMATTER_SIMPLE);
    }


    public String getFromName() {
        return fromName;
    }

    public String getToName() {
        return toName;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public User getFromUser() {
        return fromUser;
    }

    public User getToUser() {
        return toUser;
    }

    public FriendshipRequest getFriendshipRequest() {
        return friendshipRequest;
    }
}
