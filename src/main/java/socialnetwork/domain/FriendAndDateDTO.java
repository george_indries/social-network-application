package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;

public class FriendAndDateDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private LocalDateTime date;
    private String dateString;
    private User friend;
    private Friendship friendship;

    public FriendAndDateDTO(User friend, Friendship friendship) {
        this.friend = friend;
        this.friendship = friendship;
        this.id = friend.getId();
        this.firstName = friend.getFirstName();
        this.lastName = friend.getLastName();
        this.date = friendship.getDate();
        this.dateString = this.date.format(Constants.DATE_TIME_FORMATTER_SIMPLE);
    }

    /**
     *
     * @return the date of the Friendship
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     *
     * @return the id of the user
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return the first name of the user
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @return the last name of the user
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @return User
     */
    public User getFriend() {
        return friend;
    }

    /**
     *
     * @return Friendship
     */
    public Friendship getFriendship() {
        return friendship;
    }

    /**
     *
     * @return a String representing the date converted to String
     */
    public String getDateString() {
        return dateString;
    }
}
