package socialnetwork.domain.validators;

import socialnetwork.domain.Message;

import java.util.ArrayList;
import java.util.List;

public class MessageValidator implements Validator<Message> {


    /**
     * Validates the attributes of a Message
     * @param message - Messages
     * @throws ValidationException - if at least on the attributes doesn't fulfill the criteria below
     *                             - The text of the Message cannot be empty or contain just white spaces
     *                             - The date cannot be empty
     *                             - The sender's ID cannot be empty
     *                             - The {code @List<Long>} containing the IDs of the receivers cannot be empty
     */
    @Override
    public void validate(Message message) throws ValidationException {

        List<String> errors = new ArrayList<>();

        if(message.getMessage().trim().equals("")) {
            errors.add("The message cannot be empty!\n");
        }

        if(message.getDate().toString().equals("")) {
            errors.add("The date cannot be empty!\n");
        }

        if(message.getFromID().toString().trim().equals("")) {
            errors.add("You must select a sender!\n");
        }

        if(message.getToListID().isEmpty()) {
            errors.add("You must select at least one addressee!\n");
        }

        String errorMessage = errors.stream()
                .reduce("", String::concat);

        if(!errorMessage.isEmpty()) {
            throw new ValidationException(errorMessage);
        }

    }
}
