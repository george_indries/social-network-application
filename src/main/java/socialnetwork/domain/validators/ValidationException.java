package socialnetwork.domain.validators;


/**
 * Class ValidationException - own Exception Class
 *                           - extends RuntimeException
 *                           - useful for throwing ValidationException exceptions
 *                           - provides various constructors which can be used based on the context
 */
public class ValidationException extends RuntimeException {

    public ValidationException(){

    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }

    public ValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
