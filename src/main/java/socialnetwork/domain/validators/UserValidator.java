package socialnetwork.domain.validators;

import socialnetwork.domain.User;
import socialnetwork.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

public class UserValidator implements Validator<User> {

    /**
     * Validates the attributes of a User given as parameter
     * @param user - User
     * @throws ValidationException - if at least on the attributes doesn't fulfill the criteria below
     *                             - lastName, firstName - {@String} - cannot be empty and cannot contain white spaces only
     */
    @Override
    public void validate(User user) throws ValidationException {

        List<String> errors = new ArrayList<>();

        if(user.getFirstName().trim().equals("")) {
            errors.add("The first name cannot be empty!\n");
        }

        if(user.getLastName().trim().equals("")) {
            errors.add("The last name cannot be empty!\n");
        }

        if(user.getUserName().trim().equals("")) {
            errors.add("The username cannot be empty!\n");
        }

        if(user.getUserPassword().trim().equals("")) {
            errors.add("The password cannot be empty!\n");
        }

        Matcher matcher = Constants.VALID_ADDRESS_REGEX.matcher(user.getUserName());
        if(!matcher.find()) {
            errors.add("Invalid username!\n");
        }

        if(user.getUserPassword().length() < 6) {
            errors.add("The password must have at least 6 characters!\n");
        }

        String errorMessage = errors.stream()
                .reduce("", String::concat);

        if(!errorMessage.isEmpty()) {
            throw new ValidationException(errorMessage);
        }

    }
}
