package socialnetwork.domain.validators;

import socialnetwork.domain.Event;

import java.util.ArrayList;
import java.util.List;

public class EventValidator implements Validator<Event> {

    @Override
    public void validate(Event event) throws ValidationException {

        List<String> errors = new ArrayList<>();

        if(event.getName().trim().equals("")) {
            errors.add("The event name cannot be empty!\n");
        }

        if(event.getDate().toString().trim().equals("")) {
            errors.add("The event start date cannot be empty!\n");
        }

        if(event.getEndDate().toString().trim().equals("")) {
            errors.add("The event end date cannot be empty!\n");
        }

        if(event.getDescription().trim().equals("")) {
            errors.add("The event must have a description!\n");
        }

        if(event.getLocation().trim().equals("")) {
            errors.add("The event must take place somewhere!\nYou must provide a location!\n");
        }

        if(event.getCategory().trim().equals("")) {
            errors.add("The event must belong to a category!\n");
        }

        String errorMessage = errors.stream().reduce("", String::concat);

        if(!errorMessage.isEmpty()) {
            throw new ValidationException(errorMessage);
        }

    }
}
