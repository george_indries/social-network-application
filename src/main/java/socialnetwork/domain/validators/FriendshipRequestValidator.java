package socialnetwork.domain.validators;

import socialnetwork.domain.FriendshipRequest;

import java.util.ArrayList;
import java.util.List;

public class FriendshipRequestValidator implements Validator<FriendshipRequest> {


    /**
     * Validates the attributes of the FriendshipRequest
     * @param friendshipRequest - FriendshipRequest
     * @throws ValidationException - if at least on of the attributes doesn't fulfill the criteria below
     *                             - Both first user ID and second user ID must be specified
     *                             - Friendship Request status must be one of the following: pending, approved, rejected
     */
    @Override
    public void validate(FriendshipRequest friendshipRequest) throws ValidationException {

        List<String> errors = new ArrayList<>();

        if(friendshipRequest.getId().getLeft().toString().trim().equals("")) {
            errors.add("The first ID cannot be empty!\n");
        }

        if(friendshipRequest.getId().getRight().toString().trim().equals("")) {
            errors.add("The second ID cannot be empty!\n");
        }

        if(!(friendshipRequest.getStatus().equals("pending") || friendshipRequest.getStatus().equals("approved") || friendshipRequest.getStatus().equals("rejected"))) {
            errors.add("The status must be on of the following: pending, approved, rejected!\n");
        }

        String errorMessage = errors.stream()
                .reduce("", String::concat);

        if(!errorMessage.isEmpty()) {
            throw new ValidationException(errorMessage);
        }

    }

}
