package socialnetwork.domain.validators;

import socialnetwork.domain.Notification;

import java.util.ArrayList;
import java.util.List;

public class NotificationValidator implements Validator<Notification> {

    @Override
    public void validate(Notification notification) throws ValidationException {

        List<String> errors = new ArrayList<>();

        if(notification.getAttendeeID() == null) {
            errors.add("The ID of the attendee cannot be null!\n");
        }

        if(notification.getEvent().getId() == null) {
            errors.add("The ID of the event cannot be null!\n");
        }

        if(notification.getDescription().trim().equals("")) {
            errors.add("The description cannot be empty!\n");
        }

        if(notification.getLastDate().toString().trim().equals("")) {
            errors.add("The date cannot be empty!\n");
        }

        String errorMessage = errors.stream().reduce("", String::concat);

        if(!errorMessage.isEmpty()) {
            throw new ValidationException(errorMessage);
        }

    }
}
