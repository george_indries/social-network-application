package socialnetwork.domain.validators;

import socialnetwork.domain.Friendship;

import java.util.ArrayList;
import java.util.List;

public class FriendshipValidator implements Validator<Friendship> {


    /**
     * Validates the attributes of the Friendship
     * @param friendship - Friendship
     * @throws ValidationException - if at least on of the attributes doesn't fulfill the criteria below
     *                             - Both first user ID and second user ID must be specified
     *                             - The date cannot be empty
     */
    @Override
    public void validate(Friendship friendship) throws ValidationException {

        List<String> errors = new ArrayList<>();

        if(friendship.getDate().toString().trim().equals("")) {
            errors.add("The date cannot be empty!");
        }

        if(friendship.getId().getLeft().toString().trim().equals("")) {
            errors.add("The first ID cannot be empty!");
        }

        if(friendship.getId().getRight().toString().trim().equals("")) {
            errors.add("The second ID cannot be empty!");
        }

        String errorMessage = errors.stream()
                .reduce("", String::concat);

        if(!errorMessage.isEmpty()) {
            throw new ValidationException(errorMessage);
        }

    }
}
