package socialnetwork.domain;

import java.time.LocalDateTime;

public class Notification extends Entity<Long> {

    private Event event;
    private Long attendeeID;
    private LocalDateTime lastDate;
    private String description;
    private boolean toShow;


    public Notification() {

    }

    public Long getAttendeeID() {
        return attendeeID;
    }

    public void setAttendeeID(Long attendeeID) {
        this.attendeeID = attendeeID;
    }

    public LocalDateTime getLastDate() {
        return lastDate;
    }

    public void setLastDate(LocalDateTime lastDate) {
        this.lastDate = lastDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public boolean isToShow() {
        return toShow;
    }

    public void setToShow(boolean toShow) {
        this.toShow = toShow;
    }
}
