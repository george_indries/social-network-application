package socialnetwork.domain;

public enum UsersRelation {
    FRIENDS,
    PENDING_REQUEST_FROM_TO,
    PENDING_REQUEST_TO_FROM,
    NOT_FRIENDS
}
