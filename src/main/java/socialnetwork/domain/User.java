package socialnetwork.domain;

import javafx.scene.image.Image;

import java.util.*;

public class User extends Entity<Long> {

    private String firstName;
    private String lastName;
    private String userName;
    private String userPassword;
    private String name;
    private boolean notifiable;
    private Image image;

    private List<User> friends = new ArrayList<>();
    private Map<Long, User> friendsMap = new HashMap<>();


    /**
     *
     * User Constructor
     * @param firstName - Long
     * @param lastName - Long
     */
    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.name = this.firstName + " " + this.lastName;
    }

    /**
     *
     * @return the first name of the user
     *           - first name - {@code String}
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     *
     * @return the last name of the user
     *            - last name - {@code String}
     */
    public String getLastName() {
        return this.lastName;
    }


    /**
     *
     * @param user
     */
    public void add_friend(User user) {

        this.friendsMap.putIfAbsent(user.getId(), user);
    }


    /**
     *
     * @return a {@code List<User>} containing all friend of the User
     */
    public List<User> getFriends() {

        List<User> newFriends = new ArrayList<>(friendsMap.values());
        return newFriends;
    }


    @Override
    public String toString() {
        return firstName + " " + lastName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User that = (User) o;
        return Objects.equals(getFirstName(), that.getFirstName()) &&
                Objects.equals(getLastName(), that.getLastName())
                && Objects.equals(getFriends(), that.getFriends());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends());
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getName() {
        return name;
    }

    public boolean isNotifiable() {
        return notifiable;
    }

    public void setNotifiable(boolean notifiable) {
        this.notifiable = notifiable;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
