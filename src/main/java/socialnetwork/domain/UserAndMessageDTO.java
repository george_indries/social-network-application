package socialnetwork.domain;

import socialnetwork.utils.Constants;


public class UserAndMessageDTO {

    private String firstName;
    private String lastName;
    private String textMessage;
    private String date;
    private User user;
    private Message message;

    public UserAndMessageDTO(User user, Message message) {
        this.user = user;
        this.message = message;
        this.firstName = this.user.getFirstName();
        this.lastName = this.user.getLastName();
        this.date = this.message.getDate().format(Constants.DATE_TIME_FORMATTER_SIMPLE);
        this.textMessage = this.message.getMessage();
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public String getDate() {
        return date;
    }

    public User getUser() {
        return user;
    }

    public Message getMessage() {
        return message;
    }
}
