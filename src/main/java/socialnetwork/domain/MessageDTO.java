package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDate;
import java.util.List;


public class MessageDTO {

    private User fromUser;
    private String fromUserFirstName;
    private String fromUserLastName;
    private String fromUserName;
    private String toUsersNames;
    private String date;
    private LocalDate localDate;
    private List<User> toUsersList;
    private Message message;
    private String text;
    private String subject;
    private Message replyMessage = null;

    public MessageDTO(User fromUser, List<User> toUsersList, Message message) {
        this.fromUser = fromUser;
        this.toUsersList = toUsersList;
        this.message = message;
        this.fromUserFirstName = this.fromUser.getFirstName();
        this.fromUserLastName = this.fromUser.getLastName();
        this.date = this.message.getDate().format(Constants.DATE_TIME_FORMATTER_SIMPLE);
        this.localDate = this.message.getDate().toLocalDate();
        this.text = message.getMessage();
        if(this.text.length() > 10)
            this.subject = this.text.substring(0, 10) + "...";
        else
            this.subject = this.text + "...";
        this.fromUserName = this.fromUserFirstName + " " + this.fromUserLastName;
        if(toUsersList.size() > 1)
            this.toUsersNames = toUsersList.stream().map(x -> x.getName()).reduce("", (x, y) -> x + ";" + y);
        else
            this.toUsersNames = toUsersList.get(0).getName();
    }


    public User getFromUser() {
        return fromUser;
    }

    public String getFromUserFirstName() {
        return fromUserFirstName;
    }

    public String getFromUserLastName() {
        return fromUserLastName;
    }

    public String getDate() {
        return date;
    }

    public List<User> getToUsersList() {
        return toUsersList;
    }

    public Message getMessage() {
        return message;
    }

    public Message getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(Message replyMessage) {
        this.replyMessage = replyMessage;
        if(this.replyMessage.getMessage().length() > 10)
            this.subject = "Re: " + this.replyMessage.getMessage().substring(0, 10) + "...";
        else
            this.subject = "Re: " + this.replyMessage.getMessage() + "...";
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public String getText() {
        return text;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public String getToUsersNames() {
        return toUsersNames;
    }

    public String getSubject() {
        return subject;
    }
}
