package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Event extends Entity<Long> {

    private String name;
    private User host;
    private String hostName;
    private LocalDateTime date;
    private LocalDateTime endDate;
    private String startDateString;
    private String endDateString;
    private String dateString;
    private String description;
    private String pathToImage;
    private String category;
    private String location;
    private List<Attendee> attendees = new ArrayList<>();

    public Event() {

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getHost() {
        return host;
    }

    public void setHost(User host) {
        this.hostName = host.getName();
        this.host = host;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.startDateString = date.format(Constants.DATE_TIME_FORMATTER_SIMPLE);
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPathToImage() {
        return pathToImage;
    }

    public void setPathToImage(String pathToImage) {
        this.pathToImage = pathToImage;
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void addToListAttendees(Attendee attendee) {
        if(!attendees.contains(attendee)) {
            attendees.add(attendee);
        }
    }

    public String getHostName() {
        return hostName;
    }

    public String getDateString() {
        dateString = startDateString + " - " + endDateString;
        return dateString;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", host=" + host +
                ", hostName='" + hostName + '\'' +
                ", date=" + date +
                ", description='" + description + '\'' +
                '}';
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDateString = endDate.format(Constants.DATE_TIME_FORMATTER_SIMPLE);
        this.endDate = endDate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
