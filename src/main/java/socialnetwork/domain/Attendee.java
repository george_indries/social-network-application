package socialnetwork.domain;

import java.util.Objects;

public class Attendee extends Entity<Long> {

    private Long eventID;
    private User attendee;
    private boolean notifiable;
    private String name;

    public Attendee(User attendee, boolean notifiable) {
        this.attendee = attendee;
        this.notifiable = notifiable;
        this.name = attendee.getName();
    }

    public User getAttendee() {
        return attendee;
    }

    public boolean isNotifiable() {
        return notifiable;
    }

    public String getName() {
        return name;
    }

    public void setEventID(Long eventID) {
        this.eventID = eventID;
    }

    public Long getEventID() {
        return eventID;
    }

    @Override
    public String toString() {
        return name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Attendee)) return false;
        Attendee attendee1 = (Attendee) o;
        return Objects.equals(getAttendee().getId(), attendee1.getAttendee().getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAttendee().getId());
    }
}
