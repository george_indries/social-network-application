package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Class Friendship - provides the friendship between two users stored into application
 */
public class Friendship extends Entity<Tuple<Long,Long>> {

    private LocalDateTime date;

    public Friendship() {

    }


    /**
     *
     * @return the date when the friendship was created
     *             data - Date
     */
    public LocalDateTime getDate() {
        return date;
    }


    /**
     *
     * sets the date for friendship
     * @param date - Date
     *             - the date to be set
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    /**
     *
     * @return a string containing information that describes a friendship
     */
    @Override
    public String toString() {
        return "Friendship{" +
                "date=" + getDate().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + ' ' +
                "id1=" + getId().getLeft().toString() + ' ' +
                "id2=" + getId().getRight().toString() + ' ' +
                '}';
    }

    public String getDateString() {
        return getDate().format(Constants.DATE_TIME_FORMATTER_SIMPLE);
    }

}
