package socialnetwork.domain;

import java.util.Objects;

/**
 * Define a Tuple of generic type entities
 * @param <E1> - tuple first entity type
 * @param <E2> - tuple second entity type
 */
public class Tuple<E1, E2> {

    private E1 e1;
    private E2 e2;

    public Tuple(E1 e1, E2 e2) {
        this.e1 = e1;
        this.e2 = e2;
    }

    /**
     *
     * @return the left value of a Tuple
     */
    public E1 getLeft() {
        return e1;
    }

    /**
     *
     * @param e1 - E1
     * Sets the left value of a Tuple
     */
    public void setLeft(E1 e1) {
        this.e1 = e1;
    }

    /**
     *
     * @return the right value of a Tuple
     */
    public E2 getRight() {
        return e2;
    }

    /**
     *
     * @param e2 - E2
     * Sets the right value of a Tuple
     */
    public void setRight(E2 e2) {
        this.e2 = e2;
    }


    @Override
    public String toString() {
        return "" + e1 + ";" + e2;
    }


    @Override
    public boolean equals(Object obj) {
        return this.getLeft().equals(((Tuple) obj).getLeft()) && this.getRight().equals(((Tuple) obj).getRight());
    }

    @Override
    public int hashCode() {
        return Objects.hash(e1, e2);
    }
}
