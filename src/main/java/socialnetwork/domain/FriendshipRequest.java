package socialnetwork.domain;

import java.time.LocalDateTime;

/**
 * Class FriendshipRequest - provides the friend request between two users stored into application
 */
public class FriendshipRequest extends Entity<Tuple<Long, Long>> {

    private String status;
    private LocalDateTime date;
    private Long fromID;
    private Long toID;

    public FriendshipRequest() {

    }


    /**
     *
     * @return the status of the FriendshipRequest
     */
    public String getStatus() {
        return status;
    }


    /**
     *
     * @param status - String
     * Sets the status of the FriendshipRequest
     */
    public void setStatus(String status) {
        this.status = status;
    }


    /**
     *
     * @return a String containing information that describes the friend request between two users
     */
    @Override
    public String toString() {
        return "FriendshipRequest{" +
                "id1=" + getId().getLeft() + ' ' +
                "id2=" + getId().getRight() + ' ' +
                "status='" + status + ' ' +
                '}';
    }

    /**
     *
     * @return the FriendshipRequest date
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     *
     * @param date - LocalDateTime
     * Sets the date for FriendshipRequest
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    /**
     *
     * @return the ID of the user who sent the Friendship Request
     */
    public Long getFromID() {
        return fromID;
    }

    /**
     *
     * @param fromID - Long
     * Sets the ID for the user who sent the Friendship Request
     */
    public void setFromID(Long fromID) {
        this.fromID = fromID;
    }

    /**
     *
     * @return the ID of the user who was asked for Friendship
     */
    public Long getToID() {
        return toID;
    }

    /**
     *
     * @param toID Long
     * Sets the ID of the user who was asked for Friendship
     */
    public void setToID(Long toID) {
        this.toID = toID;
    }
}
