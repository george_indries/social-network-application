package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Message - useful for sending messages between two users
 */
public class Message extends Entity<Long> {

    public Message() {

    }

    private Long fromID;
    private List<Long> toListID = new ArrayList<>();
    private String message;
    private LocalDateTime date;
    private Long replyID = null;


    /**
     *
     * @return the ID of the user who sent the message
     */
    public Long getFromID() {
        return fromID;
    }


    /**
     *
     * @param fromID - Long
     * Sets the ID of the user who sends the message ( sender )
     */
    public void setFromID(Long fromID) {
        this.fromID = fromID;
    }


    /**
     *
     * @return a {@code List<Long>} representing the IDs of the users who must receive the message
     */
    public List<Long> getToListID() {
        return toListID;
    }


    /**
     *
     * @param ID - Long
     * Adds an ID to the list of the users who must receive the message ( receivers )
     */
    public void addToListID(Long ID) {

        if(!toListID.contains(ID)) {
            toListID.add(ID);
        }

    }


    /**
     *
     * @return a String representing the message's text which was sent
     */
    public String getMessage() {
        return message;
    }


    /**
     *
     * @param message - String
     * Sets a text for the message that must be sent
     */
    public void setMessage(String message) {
        this.message = message;
    }


    /**
     *
     * @return the date representing the moment when the message was sent
     *             date - Date
     */
    public LocalDateTime getDate() {
        return date;
    }


    /**
     *
     * @param date - Date
     * Sets the date of the message which must be sent
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    /**
     *
     * @return - the ID of the reply message if the actual message is a reply to another message from application
     *         - null if the actual message is not a reply to another message
     */
    public Long getReplyMessageID() {
        return replyID;
    }


    /**
     *
     * @param replyID - Long
     * Sets the ID if the actual message is a reply to another message from application
     */
    public void setReplyMessageID(Long replyID) {
        this.replyID = replyID;
    }

    /**
     *
     * @return a String containing the information that describes a Message from application
     */
    @Override
    public String toString() {
        return "Message{" +
                "fromID=" + fromID +
                ", toListID=" + toListID +
                ", message='" + message + '\'' +
                ", date=" + date +
                '}';
    }
}
